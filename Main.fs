﻿namespace WebSharper.ECharts.Extension

open WebSharper
open WebSharper.JavaScript
open WebSharper.InterfaceGenerator

module Definition =
    type Helpers =
        static member NormaliseType(t: Type.Type) = t
        static member NormaliseType(t: CodeModel.Class) = t.Type

    let zrender_LinearGradient = Class "zrender.LinearGradient"
    let SeriesBar = Class "echarts.EChartOption.SeriesBar"
    let SeriesBar_DataObject = Class "echarts.EChartOption.SeriesBar.DataObject"
    let SeriesBoxplot = Class "echarts.EChartOption.SeriesBoxplot"
    let SeriesBoxplot_DataObject = Class "echarts.EChartOption.SeriesBoxplot.DataObject"
    let SeriesCandlestick = Class "echarts.EChartOption.SeriesCandlestick"
    let SeriesCandlestick_DataObject = Class "echarts.EChartOption.SeriesCandlestick.DataObject"
    let SeriesCustom = Class "echarts.EChartOption.SeriesCustom"
    let SeriesCustom_DataObject = Class "echarts.EChartOption.SeriesCustom.DataObject"
    let SeriesCustom_RenderItemParams = Class "echarts.EChartOption.SeriesCustom.RenderItemParams"
    let SeriesCustom_RenderItemApi = Class "echarts.EChartOption.SeriesCustom.RenderItemApi"
    let SeriesCustom_CoordSys = Class "echarts.EChartOption.SeriesCustom.CoordSys"
    let SeriesCustom_RangeInfo = Class "echarts.EChartOption.SeriesCustom.RangeInfo"
    let SeriesCustom_RenderItemReturnGroup = Class "echarts.EChartOption.SeriesCustom.RenderItemReturnGroup"
    let SeriesCustom_RenderItemReturnPath = Class "echarts.EChartOption.SeriesCustom.RenderItemReturnPath"
    let SeriesCustom_RenderItemReturnImage = Class "echarts.EChartOption.SeriesCustom.RenderItemReturnImage"
    let SeriesCustom_RenderItemReturnText = Class "echarts.EChartOption.SeriesCustom.RenderItemReturnText"
    let SeriesCustom_RenderItemReturnRect = Class "echarts.EChartOption.SeriesCustom.RenderItemReturnRect"
    let SeriesCustom_RenderItemReturnCircle = Class "echarts.EChartOption.SeriesCustom.RenderItemReturnCircle"
    let SeriesCustom_RenderItemReturnRing = Class "echarts.EChartOption.SeriesCustom.RenderItemReturnRing"
    let SeriesCustom_RenderItemReturnSector = Class "echarts.EChartOption.SeriesCustom.RenderItemReturnSector"
    let SeriesCustom_RenderItemReturnArc = Class "echarts.EChartOption.SeriesCustom.RenderItemReturnArc"
    let SeriesCustom_RenderItemReturnPolygon = Class "echarts.EChartOption.SeriesCustom.RenderItemReturnPolygon"
    let SeriesCustom_RenderItemReturnPolyline = Class "echarts.EChartOption.SeriesCustom.RenderItemReturnPolyline"
    let SeriesCustom_RenderItemReturnLine = Class "echarts.EChartOption.SeriesCustom.RenderItemReturnLine"
    let SeriesCustom_RenderItemReturnBezierCurve = Class "echarts.EChartOption.SeriesCustom.RenderItemReturnBezierCurve"
    let SeriesEffectScatter = Class "echarts.EChartOption.SeriesEffectScatter"
    let SeriesEffectScatter_DataObject = Class "echarts.EChartOption.SeriesEffectScatter.DataObject"
    let SeriesFunnel = Class "echarts.EChartOption.SeriesFunnel"
    let SeriesFunnel_DataObject = Class "echarts.EChartOption.SeriesFunnel.DataObject"
    let SeriesGauge = Class "echarts.EChartOption.SeriesGauge"
    let SeriesGauge_DataObject = Class "echarts.EChartOption.SeriesGauge.DataObject"
    let SeriesGraph = Class "echarts.EChartOption.SeriesGraph"
    let SeriesGraph_CategoryObject = Class "echarts.EChartOption.SeriesGraph.CategoryObject"
    let SeriesGraph_DataObject = Class "echarts.EChartOption.SeriesGraph.DataObject"
    let SeriesGraph_LinkObject = Class "echarts.EChartOption.SeriesGraph.LinkObject"
    let SeriesHeatmap = Class "echarts.EChartOption.SeriesHeatmap"
    let SeriesHeatmap_DataObject = Class "echarts.EChartOption.SeriesHeatmap.DataObject"
    let SeriesLine = Class "echarts.EChartOption.SeriesLine"
    let SeriesLine_DataObject = Class "echarts.EChartOption.SeriesLine.DataObject"
    let SeriesLines = Class "echarts.EChartOption.SeriesLines"
    let SeriesLines_DataObject = Class "echarts.EChartOption.SeriesLines.DataObject"
    let SeriesMap = Class "echarts.EChartOption.SeriesMap"
    let SeriesMap_DataObject = Class "echarts.EChartOption.SeriesMap.DataObject"
    let SeriesParallel = Class "echarts.EChartOption.SeriesParallel"
    let SeriesParallel_DataObject = Class "echarts.EChartOption.SeriesParallel.DataObject"
    let SeriesPictorialBar = Class "echarts.EChartOption.SeriesPictorialBar"
    let SeriesPictorialBar_DataObject = Class "echarts.EChartOption.SeriesPictorialBar.DataObject"
    let SeriesPie = Class "echarts.EChartOption.SeriesPie"
    let SeriesPie_DataObject = Class "echarts.EChartOption.SeriesPie.DataObject"
    let SeriesRadar = Class "echarts.EChartOption.SeriesRadar"
    let SeriesRadar_DataObject = Class "echarts.EChartOption.SeriesRadar.DataObject"
    let SeriesSankey = Class "echarts.EChartOption.SeriesSankey"
    let SeriesSankey_DataObject = Class "echarts.EChartOption.SeriesSankey.DataObject"
    let SeriesSankey_LinkObject = Class "echarts.EChartOption.SeriesSankey.LinkObject"
    let SeriesScatter = Class "echarts.EChartOption.SeriesScatter"
    let SeriesScatter_DataObject = Class "echarts.EChartOption.SeriesScatter.DataObject"
    let SeriesSunburst = Class "echarts.EChartOption.SeriesSunburst"
    let SeriesSunburst_DataObject = Class "echarts.EChartOption.SeriesSunburst.DataObject"
    let SeriesThemeRiver = Class "echarts.EChartOption.SeriesThemeRiver"
    let SeriesThemeRiver_DataObject = Class "echarts.EChartOption.SeriesThemeRiver.DataObject"
    let SeriesTree = Class "echarts.EChartOption.SeriesTree"
    let SeriesTree_DataObject = Class "echarts.EChartOption.SeriesTree.DataObject"
    let SeriesTreemap = Class "echarts.EChartOption.SeriesTreemap"
    let SeriesTreemap_DataObject = Class "echarts.EChartOption.SeriesTreemap.DataObject"
    let AxisPointer = Class "echarts.EChartOption.AxisPointer"
    let Calendar = Class "echarts.EChartOption.Calendar"
    let Calendar_Label = Class "echarts.EChartOption.Calendar.Label"
    let Calendar_DayLabel = Class "echarts.EChartOption.Calendar.DayLabel"
    let Calendar_MonthLabel = Class "echarts.EChartOption.Calendar.MonthLabel"
    let Calendar_MonthLabelFormatterParams = Class "echarts.EChartOption.Calendar.MonthLabelFormatterParams"
    let Calendar_YearLabel = Class "echarts.EChartOption.Calendar.YearLabel"
    let Calendar_YearLabelFormatterParams = Class "echarts.EChartOption.Calendar.YearLabelFormatterParams"
    let Dataset = Class "echarts.EChartOption.Dataset"
    let Dataset_DimensionObject = Class "echarts.EChartOption.Dataset.DimensionObject"
    let DataZoom_Inside = Class "echarts.EChartOption.DataZoom.Inside"
    let DataZoom_Slider = Class "echarts.EChartOption.DataZoom.Slider"
    let Grid = Class "echarts.EChartOption.Grid"
    let Legend = Class "echarts.EChartOption.Legend"
    let Legend_LegendDataObject = Class "echarts.EChartOption.Legend.LegendDataObject"
    let Legend_PageIcons = Class "echarts.EChartOption.Legend.PageIcons"
    let LineStyle = Class "echarts.EChartOption.LineStyle"
    let SingleAxis = Class "echarts.EChartOption.SingleAxis"
    let BaseTextStyle = Class "echarts.EChartOption.BaseTextStyle"
    let TextStyle = Class "echarts.EChartOption.TextStyle"
    let RichStyle = Class "echarts.EChartOption.RichStyle"
    let BaseTextStyleWithRich = Class "echarts.EChartOption.BaseTextStyleWithRich"
    let TextStyleWithRich = Class "echarts.EChartOption.TextStyleWithRich"
    let BaseTooltip = Class "echarts.EChartOption.BaseTooltip"
    let Tooltip = Class "echarts.EChartOption.Tooltip"
    let Tooltip_Position_Obj = Class "echarts.EChartOption.Tooltip.Position.Obj"
    let Tooltip_Format = Class "echarts.EChartOption.Tooltip.Format"
    let Tooltip_AxisPointer = Class "echarts.EChartOption.Tooltip.AxisPointer"
    let VisualMap_Continuous = Class "echarts.VisualMap.Continuous"
    let VisualMap_Piecewise = Class "echarts.VisualMap.Piecewise"
    let VisualMap_RangeObject = Class "echarts.VisualMap.RangeObject"
    let VisualMap_PiecesObject = Class "echarts.VisualMap.PiecesObject"
    let XAxis = Class "echarts.EChartOption.XAxis"
    let YAxis = Class "echarts.EChartOption.YAxis"
    let MapObj = Class "echarts.MapObj"
    let Graphic = Class "echarts.Graphic"
    let ECharts = Class "echarts.ECharts"
    let EChartsConvertFinder = Class "echarts.EChartsConvertFinder"
    let ERectangle = Class "echarts.ERectangle"
    let EChartOption = Class "echarts.EChartOption"
    let EChartsResponsiveOption = Class "echarts.EChartsResponsiveOption"
    let EChartsOptionConfig = Class "echarts.EChartsOptionConfig"
    let EChartsResizeOption = Class "echarts.EChartsResizeOption"
    let EChartTitleOption = Class "echarts.EChartTitleOption"
    let EChartsLoadingOption = Class "echarts.EChartsLoadingOption"
    let Line = Class "echarts.EChartOption.BasicComponents.Line"
    let ChartExtremes = Class "echarts.EChartOption.BasicComponents.ChartExtremes"
    let CartesianAxis = Class "echarts.EChartOption.BasicComponents.CartesianAxis"
    let CartesianAxis_Tick = Class "echarts.EChartOption.BasicComponents.CartesianAxis.Tick"
    let CartesianAxis_MinorTick = Class "echarts.EChartOption.BasicComponents.CartesianAxis.MinorTick"
    let CartesianAxis_Label = Class "echarts.EChartOption.BasicComponents.CartesianAxis.Label"
    let CartesianAxis_SplitLine = Class "echarts.EChartOption.BasicComponents.CartesianAxis.SplitLine"
    let CartesianAxis_MinorSplitLine = Class "echarts.EChartOption.BasicComponents.CartesianAxis.MinorSplitLine"
    let CartesianAxis_SplitArea = Class "echarts.EChartOption.BasicComponents.CartesianAxis.SplitArea"
    let CartesianAxis_DataObject = Class "echarts.EChartOption.BasicComponents.CartesianAxis.DataObject"
    let CartesianAxis_Pointer = Class "echarts.EChartOption.BasicComponents.CartesianAxis.Pointer"
    let CartesianAxis_PointerLabel = Class "echarts.EChartOption.BasicComponents.CartesianAxis.PointerLabel"

    zrender_LinearGradient
        |> WithSourceName "zrender_LinearGradient"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
        ]
        |> ignore

    SeriesBar
        |> WithSourceName "SeriesBar"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetLegendHoverLink" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.legendHoverLink = " + tr "v" + ", $this)")
            "SetCoordinateSystem" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.coordinateSystem = " + tr "v" + ", $this)")
            "SetXAxisIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.xAxisIndex = " + tr "v" + ", $this)")
            "SetYAxisIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.yAxisIndex = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetStack" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.stack = " + tr "v" + ", $this)")
            "SetCursor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.cursor = " + tr "v" + ", $this)")
            "SetBarWidth" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.barWidth = " + tr "v" + ", $this)")
            "SetBarMaxWidth" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.barMaxWidth = " + tr "v" + ", $this)")
            "SetBarMinWidth" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.barMinWidth = " + tr "v" + ", $this)")
            "SetBarMinHeight" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.barMinHeight = " + tr "v" + ", $this)")
            "SetBarGap" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.barGap = " + tr "v" + ", $this)")
            "SetBarCategoryGap" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.barCategoryGap = " + tr "v" + ", $this)")
            "SetLarge" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.large = " + tr "v" + ", $this)")
            "SetLargeThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.largeThreshold = " + tr "v" + ", $this)")
            "SetProgressive" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.progressive = " + tr "v" + ", $this)")
            "SetProgressiveThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.progressiveThreshold = " + tr "v" + ", $this)")
            "SetProgressiveChunkMode" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.progressiveChunkMode = " + tr "v" + ", $this)")
            "SetDimensions" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.dimensions = " + tr "v" + ", $this)")
            "SetEncode" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.encode = " + tr "v" + ", $this)")
            "SetSeriesLayoutBy" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.seriesLayoutBy = " + tr "v" + ", $this)")
            "SetDatasetIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.datasetIndex = " + tr "v" + ", $this)")
            "SetData" => ((!| (T<unit> + T<string> + T<float> + SeriesBar_DataObject)) + (!| (!| (T<unit> + T<string> + T<float> + SeriesBar_DataObject))))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetMarkPoint" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markPoint = " + tr "v" + ", $this)")
            "SetMarkLine" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markLine = " + tr "v" + ", $this)")
            "SetMarkArea" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markArea = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animation = " + tr "v" + ", $this)")
            "SetAnimationThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationThreshold = " + tr "v" + ", $this)")
            "SetAnimationDuration" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDuration = " + tr "v" + ", $this)")
            "SetAnimationEasing" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasing = " + tr "v" + ", $this)")
            "SetAnimationDelay" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelay = " + tr "v" + ", $this)")
            "SetAnimationDurationUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDurationUpdate = " + tr "v" + ", $this)")
            "SetAnimationEasingUpdate" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasingUpdate = " + tr "v" + ", $this)")
            "SetAnimationDelayUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelayUpdate = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "name" =@ (!? T<string>)
            "legendHoverLink" =@ (!? T<bool>)
            "coordinateSystem" =@ (!? T<string>)
            "xAxisIndex" =@ (!? T<float>)
            "yAxisIndex" =@ (!? T<float>)
            "label" =@ (!? T<obj>)
            "itemStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "stack" =@ (!? T<string>)
            "cursor" =@ (!? T<string>)
            "barWidth" =@ (!? (T<float> + T<string>))
            "barMaxWidth" =@ (!? (T<float> + T<string>))
            "barMinWidth" =@ (!? (T<float> + T<string>))
            "barMinHeight" =@ (!? T<float>)
            "barGap" =@ (!? T<string>)
            "barCategoryGap" =@ (!? T<string>)
            "large" =@ (!? T<bool>)
            "largeThreshold" =@ (!? T<float>)
            "progressive" =@ (!? T<float>)
            "progressiveThreshold" =@ (!? T<float>)
            "progressiveChunkMode" =@ (!? T<string>)
            "dimensions" =@ (!? (!| T<obj>))
            "encode" =@ (!? T<obj>)
            "seriesLayoutBy" =@ (!? T<string>)
            "datasetIndex" =@ (!? T<float>)
            "data" =@ (!? ((!| (T<unit> + T<string> + T<float> + SeriesBar_DataObject)) + (!| (!| (T<unit> + T<string> + T<float> + SeriesBar_DataObject)))))
            "markPoint" =@ (!? T<obj>)
            "markLine" =@ (!? T<obj>)
            "markArea" =@ (!? T<obj>)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "animation" =@ (!? T<bool>)
            "animationThreshold" =@ (!? T<float>)
            "animationDuration" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasing" =@ (!? T<string>)
            "animationDelay" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationDurationUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasingUpdate" =@ (!? T<string>)
            "animationDelayUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesBar_DataObject
        |> WithSourceName "SeriesBar_DataObject"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetValue" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.value = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "name" =@ (!? T<string>)
            "value" =@ (!? T<float>)
            "label" =@ (!? T<obj>)
            "itemStyle" =@ (!? T<obj>)
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesBoxplot
        |> WithSourceName "SeriesBoxplot"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetCoordinateSystem" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.coordinateSystem = " + tr "v" + ", $this)")
            "SetXAxisIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.xAxisIndex = " + tr "v" + ", $this)")
            "SetYAxisIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.yAxisIndex = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetLegendHoverLink" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.legendHoverLink = " + tr "v" + ", $this)")
            "SetHoverAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.hoverAnimation = " + tr "v" + ", $this)")
            "SetLayout" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.layout = " + tr "v" + ", $this)")
            "SetBoxWidth" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.boxWidth = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetDimensions" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.dimensions = " + tr "v" + ", $this)")
            "SetEncode" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.encode = " + tr "v" + ", $this)")
            "SetData" => (!| (!| (T<float> + SeriesBoxplot_DataObject)))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetMarkPoint" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markPoint = " + tr "v" + ", $this)")
            "SetMarkLine" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markLine = " + tr "v" + ", $this)")
            "SetMarkArea" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markArea = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetAnimationDuration" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDuration = " + tr "v" + ", $this)")
            "SetAnimationEasing" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasing = " + tr "v" + ", $this)")
            "SetAnimationDelay" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelay = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "coordinateSystem" =@ (!? T<string>)
            "xAxisIndex" =@ (!? T<float>)
            "yAxisIndex" =@ (!? T<float>)
            "name" =@ (!? T<string>)
            "legendHoverLink" =@ (!? T<bool>)
            "hoverAnimation" =@ (!? T<bool>)
            "layout" =@ (!? T<string>)
            "boxWidth" =@ (!? (!| T<obj>))
            "itemStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "dimensions" =@ (!? (!| T<obj>))
            "encode" =@ (!? T<obj>)
            "data" =@ (!? (!| (!| (T<float> + SeriesBoxplot_DataObject))))
            "markPoint" =@ (!? T<obj>)
            "markLine" =@ (!? T<obj>)
            "markArea" =@ (!? T<obj>)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "silent" =@ (!? T<bool>)
            "animationDuration" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasing" =@ (!? T<string>)
            "animationDelay" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesBoxplot_DataObject
        |> WithSourceName "SeriesBoxplot_DataObject"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetValue" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.value = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "name" =@ (!? T<string>)
            "value" =@ (!? (!| T<obj>))
            "itemStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesCandlestick
        |> WithSourceName "SeriesCandlestick"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetCoordinateSystem" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.coordinateSystem = " + tr "v" + ", $this)")
            "SetXAxisIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.xAxisIndex = " + tr "v" + ", $this)")
            "SetYAxisIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.yAxisIndex = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetLegendHoverLink" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.legendHoverLink = " + tr "v" + ", $this)")
            "SetHoverAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.hoverAnimation = " + tr "v" + ", $this)")
            "SetLayout" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.layout = " + tr "v" + ", $this)")
            "SetBarWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.barWidth = " + tr "v" + ", $this)")
            "SetBarMinWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.barMinWidth = " + tr "v" + ", $this)")
            "SetBarMaxWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.barMaxWidth = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetLarge" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.large = " + tr "v" + ", $this)")
            "SetLargeThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.largeThreshold = " + tr "v" + ", $this)")
            "SetProgressive" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.progressive = " + tr "v" + ", $this)")
            "SetProgressiveThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.progressiveThreshold = " + tr "v" + ", $this)")
            "SetProgressiveChunkMode" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.progressiveChunkMode = " + tr "v" + ", $this)")
            "SetDimensions" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.dimensions = " + tr "v" + ", $this)")
            "SetEncode" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.encode = " + tr "v" + ", $this)")
            "SetData" => (!| (!| (T<float> + SeriesCandlestick_DataObject)))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetMarkPoint" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markPoint = " + tr "v" + ", $this)")
            "SetMarkLine" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markLine = " + tr "v" + ", $this)")
            "SetMarkArea" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markArea = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetAnimationDuration" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDuration = " + tr "v" + ", $this)")
            "SetAnimationEasing" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasing = " + tr "v" + ", $this)")
            "SetAnimationDelay" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelay = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "coordinateSystem" =@ (!? T<string>)
            "xAxisIndex" =@ (!? T<float>)
            "yAxisIndex" =@ (!? T<float>)
            "name" =@ (!? T<string>)
            "legendHoverLink" =@ (!? T<bool>)
            "hoverAnimation" =@ (!? T<bool>)
            "layout" =@ (!? T<string>)
            "barWidth" =@ (!? T<float>)
            "barMinWidth" =@ (!? T<float>)
            "barMaxWidth" =@ (!? T<float>)
            "itemStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "large" =@ (!? T<bool>)
            "largeThreshold" =@ (!? T<float>)
            "progressive" =@ (!? T<float>)
            "progressiveThreshold" =@ (!? T<float>)
            "progressiveChunkMode" =@ (!? T<string>)
            "dimensions" =@ (!? (!| T<obj>))
            "encode" =@ (!? T<obj>)
            "data" =@ (!? (!| (!| (T<float> + SeriesCandlestick_DataObject))))
            "markPoint" =@ (!? T<obj>)
            "markLine" =@ (!? T<obj>)
            "markArea" =@ (!? T<obj>)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "silent" =@ (!? T<bool>)
            "animationDuration" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasing" =@ (!? T<string>)
            "animationDelay" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesCandlestick_DataObject
        |> WithSourceName "SeriesCandlestick_DataObject"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetValue" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.value = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "name" =@ (!? T<string>)
            "value" =@ (!? (!| T<obj>))
            "itemStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesCustom
        |> WithSourceName "SeriesCustom"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetLegendHoverLink" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.legendHoverLink = " + tr "v" + ", $this)")
            "SetCoordinateSystem" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.coordinateSystem = " + tr "v" + ", $this)")
            "SetXAxisIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.xAxisIndex = " + tr "v" + ", $this)")
            "SetYAxisIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.yAxisIndex = " + tr "v" + ", $this)")
            "SetPolarIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.polarIndex = " + tr "v" + ", $this)")
            "SetGeoIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.geoIndex = " + tr "v" + ", $this)")
            "SetCalendarIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.calendarIndex = " + tr "v" + ", $this)")
            "SetRenderItem" => (SeriesCustom_RenderItemParams?``params`` * SeriesCustom_RenderItemApi?api ^-> (SeriesCustom_RenderItemReturnGroup + SeriesCustom_RenderItemReturnPath + SeriesCustom_RenderItemReturnImage + SeriesCustom_RenderItemReturnText + SeriesCustom_RenderItemReturnRect + SeriesCustom_RenderItemReturnCircle + SeriesCustom_RenderItemReturnRing + SeriesCustom_RenderItemReturnSector + SeriesCustom_RenderItemReturnArc + SeriesCustom_RenderItemReturnPolygon + SeriesCustom_RenderItemReturnPolyline + SeriesCustom_RenderItemReturnLine + SeriesCustom_RenderItemReturnBezierCurve))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.renderItem = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetDimensions" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.dimensions = " + tr "v" + ", $this)")
            "SetEncode" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.encode = " + tr "v" + ", $this)")
            "SetSeriesLayoutBy" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.seriesLayoutBy = " + tr "v" + ", $this)")
            "SetDatasetIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.datasetIndex = " + tr "v" + ", $this)")
            "SetData" => ((!| (T<unit> + T<string> + T<float> + SeriesCustom_DataObject)) + (!| (!| (T<unit> + T<string> + T<float> + SeriesCustom_DataObject))))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animation = " + tr "v" + ", $this)")
            "SetAnimationThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationThreshold = " + tr "v" + ", $this)")
            "SetAnimationDuration" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDuration = " + tr "v" + ", $this)")
            "SetAnimationEasing" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasing = " + tr "v" + ", $this)")
            "SetAnimationDelay" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelay = " + tr "v" + ", $this)")
            "SetAnimationDurationUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDurationUpdate = " + tr "v" + ", $this)")
            "SetAnimationEasingUpdate" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasingUpdate = " + tr "v" + ", $this)")
            "SetAnimationDelayUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelayUpdate = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "name" =@ (!? T<string>)
            "legendHoverLink" =@ (!? T<bool>)
            "coordinateSystem" =@ (!? T<string>)
            "xAxisIndex" =@ (!? T<float>)
            "yAxisIndex" =@ (!? T<float>)
            "polarIndex" =@ (!? T<float>)
            "geoIndex" =@ (!? T<float>)
            "calendarIndex" =@ (!? T<float>)
            "renderItem" =@ (!? (SeriesCustom_RenderItemParams?``params`` * SeriesCustom_RenderItemApi?api ^-> (SeriesCustom_RenderItemReturnGroup + SeriesCustom_RenderItemReturnPath + SeriesCustom_RenderItemReturnImage + SeriesCustom_RenderItemReturnText + SeriesCustom_RenderItemReturnRect + SeriesCustom_RenderItemReturnCircle + SeriesCustom_RenderItemReturnRing + SeriesCustom_RenderItemReturnSector + SeriesCustom_RenderItemReturnArc + SeriesCustom_RenderItemReturnPolygon + SeriesCustom_RenderItemReturnPolyline + SeriesCustom_RenderItemReturnLine + SeriesCustom_RenderItemReturnBezierCurve)))
            "itemStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "dimensions" =@ (!? (!| T<obj>))
            "encode" =@ (!? T<obj>)
            "seriesLayoutBy" =@ (!? T<string>)
            "datasetIndex" =@ (!? T<float>)
            "data" =@ (!? ((!| (T<unit> + T<string> + T<float> + SeriesCustom_DataObject)) + (!| (!| (T<unit> + T<string> + T<float> + SeriesCustom_DataObject)))))
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "silent" =@ (!? T<bool>)
            "animation" =@ (!? T<bool>)
            "animationThreshold" =@ (!? T<float>)
            "animationDuration" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasing" =@ (!? T<string>)
            "animationDelay" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationDurationUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasingUpdate" =@ (!? T<string>)
            "animationDelayUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesCustom_DataObject
        |> WithSourceName "SeriesCustom_DataObject"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetValue" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.value = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "name" =@ (!? T<string>)
            "value" =@ (!? (T<float> + (!| T<float>)))
            "itemStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesCustom_RenderItemParams
        |> WithSourceName "SeriesCustom_RenderItemParams"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetContext" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.context = " + tr "v" + ", $this)")
            "SetSeriesId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.seriesId = " + tr "v" + ", $this)")
            "SetSeriesName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.seriesName = " + tr "v" + ", $this)")
            "SetSeriesIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.seriesIndex = " + tr "v" + ", $this)")
            "SetDataIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.dataIndex = " + tr "v" + ", $this)")
            "SetDataIndexInside" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.dataIndexInside = " + tr "v" + ", $this)")
            "SetDataInsideLength" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.dataInsideLength = " + tr "v" + ", $this)")
            "SetActionType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.actionType = " + tr "v" + ", $this)")
            "SetCoordSys" => SeriesCustom_CoordSys?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.coordSys = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "context" =@ (!? T<string>)
            "seriesId" =@ (!? T<string>)
            "seriesName" =@ (!? T<string>)
            "seriesIndex" =@ (!? T<float>)
            "dataIndex" =@ (!? T<float>)
            "dataIndexInside" =@ (!? T<float>)
            "dataInsideLength" =@ (!? T<float>)
            "actionType" =@ (!? T<string>)
            "coordSys" =@ (!? SeriesCustom_CoordSys)
        ]
        |> ignore

    SeriesCustom_RenderItemApi
        |> WithSourceName "SeriesCustom_RenderItemApi"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetValue" => (T<unit> ^-> T<unit>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.value = " + tr "v" + ", $this)")
            "SetCoord" => (T<unit> ^-> T<unit>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.coord = " + tr "v" + ", $this)")
            "SetSize" => (T<unit> ^-> T<unit>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.size = " + tr "v" + ", $this)")
            "SetStyle" => (T<unit> ^-> T<unit>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.style = " + tr "v" + ", $this)")
            "SetStyleEmphasis" => (T<unit> ^-> T<unit>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.styleEmphasis = " + tr "v" + ", $this)")
            "SetVisual" => (T<unit> ^-> T<unit>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.visual = " + tr "v" + ", $this)")
            "SetBarLayout" => (T<unit> ^-> T<unit>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.barLayout = " + tr "v" + ", $this)")
            "SetCurrentSeriesIndices" => (T<unit> ^-> T<unit>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.currentSeriesIndices = " + tr "v" + ", $this)")
            "SetFont" => (T<unit> ^-> T<unit>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.font = " + tr "v" + ", $this)")
            "SetGetWidth" => (T<unit> ^-> T<unit>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.getWidth = " + tr "v" + ", $this)")
            "SetGetHeight" => (T<unit> ^-> T<unit>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.getHeight = " + tr "v" + ", $this)")
            "SetGetZr" => (T<unit> ^-> T<unit>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.getZr = " + tr "v" + ", $this)")
            "SetGetDevicePixelRatio" => (T<unit> ^-> T<unit>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.getDevicePixelRatio = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "value" =@ (!? (T<unit> ^-> T<unit>))
            "coord" =@ (!? (T<unit> ^-> T<unit>))
            "size" =@ (!? (T<unit> ^-> T<unit>))
            "style" =@ (!? (T<unit> ^-> T<unit>))
            "styleEmphasis" =@ (!? (T<unit> ^-> T<unit>))
            "visual" =@ (!? (T<unit> ^-> T<unit>))
            "barLayout" =@ (!? (T<unit> ^-> T<unit>))
            "currentSeriesIndices" =@ (!? (T<unit> ^-> T<unit>))
            "font" =@ (!? (T<unit> ^-> T<unit>))
            "getWidth" =@ (!? (T<unit> ^-> T<unit>))
            "getHeight" =@ (!? (T<unit> ^-> T<unit>))
            "getZr" =@ (!? (T<unit> ^-> T<unit>))
            "getDevicePixelRatio" =@ (!? (T<unit> ^-> T<unit>))
        ]
        |> ignore

    SeriesCustom_CoordSys
        |> WithSourceName "SeriesCustom_CoordSys"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetX" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.x = " + tr "v" + ", $this)")
            "SetY" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.y = " + tr "v" + ", $this)")
            "SetWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.width = " + tr "v" + ", $this)")
            "SetHeight" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.height = " + tr "v" + ", $this)")
            "SetCellWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.cellWidth = " + tr "v" + ", $this)")
            "SetCellHeight" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.cellHeight = " + tr "v" + ", $this)")
            "SetRangeInfo" => SeriesCustom_RangeInfo?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.rangeInfo = " + tr "v" + ", $this)")
            "SetZoom" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zoom = " + tr "v" + ", $this)")
            "SetCx" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.cx = " + tr "v" + ", $this)")
            "SetCy" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.cy = " + tr "v" + ", $this)")
            "SetR" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.r = " + tr "v" + ", $this)")
            "SetR0" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.r0 = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? (T<string>))
            "x" =@ (!? T<float>)
            "y" =@ (!? T<float>)
            "width" =@ (!? T<float>)
            "height" =@ (!? T<float>)
            "cellWidth" =@ (!? T<float>)
            "cellHeight" =@ (!? T<float>)
            "rangeInfo" =@ (!? SeriesCustom_RangeInfo)
            "zoom" =@ (!? T<float>)
            "cx" =@ (!? T<float>)
            "cy" =@ (!? T<float>)
            "r" =@ (!? T<float>)
            "r0" =@ (!? T<float>)
        ]
        |> ignore

    SeriesCustom_RangeInfo
        |> WithSourceName "SeriesCustom_RangeInfo"
        |+> Static [ ObjectConstructor ((!? T<obj>)?start * (!? T<obj>)?``end`` * (!? T<float>)?weeks * (!? T<float>)?dayCount) ]
        |+> Instance [
            "start" =@ (!? T<obj>)
            "end" =@ (!? T<obj>)
            "weeks" =@ (!? T<float>)
            "dayCount" =@ (!? T<float>)
        ]
        |> ignore

    SeriesCustom_RenderItemReturnGroup
        |> WithSourceName "SeriesCustom_RenderItemReturnGroup"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetPosition" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.position = " + tr "v" + ", $this)")
            "SetRotation" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.rotation = " + tr "v" + ", $this)")
            "SetScale" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.scale = " + tr "v" + ", $this)")
            "SetOrigin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.origin = " + tr "v" + ", $this)")
            "SetZ2" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z2 = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetInfo" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.info = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetInvisible" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.invisible = " + tr "v" + ", $this)")
            "SetIgnore" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.ignore = " + tr "v" + ", $this)")
            "SetWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.width = " + tr "v" + ", $this)")
            "SetHeight" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.height = " + tr "v" + ", $this)")
            "SetDiffChildrenByName" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.diffChildrenByName = " + tr "v" + ", $this)")
            "SetChildren" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.children = " + tr "v" + ", $this)")
            "SetStyleEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.styleEmphasis = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "position" =@ (!? (!| T<obj>))
            "rotation" =@ (!? T<float>)
            "scale" =@ (!? (!| T<obj>))
            "origin" =@ (!? T<float>)
            "z2" =@ (!? T<float>)
            "name" =@ (!? T<string>)
            "info" =@ (!? T<obj>)
            "silent" =@ (!? T<bool>)
            "invisible" =@ (!? T<bool>)
            "ignore" =@ (!? T<bool>)
            "width" =@ (!? T<float>)
            "height" =@ (!? T<float>)
            "diffChildrenByName" =@ (!? T<bool>)
            "children" =@ (!? (!| T<obj>))
            "styleEmphasis" =@ (!? T<obj>)
        ]
        |> ignore

    SeriesCustom_RenderItemReturnPath
        |> WithSourceName "SeriesCustom_RenderItemReturnPath"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetPosition" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.position = " + tr "v" + ", $this)")
            "SetRotation" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.rotation = " + tr "v" + ", $this)")
            "SetScale" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.scale = " + tr "v" + ", $this)")
            "SetOrigin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.origin = " + tr "v" + ", $this)")
            "SetZ2" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z2 = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetInfo" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.info = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetInvisible" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.invisible = " + tr "v" + ", $this)")
            "SetIgnore" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.ignore = " + tr "v" + ", $this)")
            "SetShape" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shape = " + tr "v" + ", $this)")
            "SetStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.style = " + tr "v" + ", $this)")
            "SetStyleEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.styleEmphasis = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "position" =@ (!? (!| T<obj>))
            "rotation" =@ (!? T<float>)
            "scale" =@ (!? (!| T<obj>))
            "origin" =@ (!? T<float>)
            "z2" =@ (!? T<float>)
            "name" =@ (!? T<string>)
            "info" =@ (!? T<obj>)
            "silent" =@ (!? T<bool>)
            "invisible" =@ (!? T<bool>)
            "ignore" =@ (!? T<bool>)
            "shape" =@ (!? T<obj>)
            "style" =@ (!? T<obj>)
            "styleEmphasis" =@ (!? T<obj>)
        ]
        |> ignore

    SeriesCustom_RenderItemReturnImage
        |> WithSourceName "SeriesCustom_RenderItemReturnImage"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetPosition" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.position = " + tr "v" + ", $this)")
            "SetRotation" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.rotation = " + tr "v" + ", $this)")
            "SetScale" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.scale = " + tr "v" + ", $this)")
            "SetOrigin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.origin = " + tr "v" + ", $this)")
            "SetZ2" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z2 = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetInfo" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.info = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetInvisible" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.invisible = " + tr "v" + ", $this)")
            "SetIgnore" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.ignore = " + tr "v" + ", $this)")
            "SetStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.style = " + tr "v" + ", $this)")
            "SetStyleEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.styleEmphasis = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "position" =@ (!? (!| T<obj>))
            "rotation" =@ (!? T<float>)
            "scale" =@ (!? (!| T<obj>))
            "origin" =@ (!? T<float>)
            "z2" =@ (!? T<float>)
            "name" =@ (!? T<string>)
            "info" =@ (!? T<obj>)
            "silent" =@ (!? T<bool>)
            "invisible" =@ (!? T<bool>)
            "ignore" =@ (!? T<bool>)
            "style" =@ (!? T<obj>)
            "styleEmphasis" =@ (!? T<obj>)
        ]
        |> ignore

    SeriesCustom_RenderItemReturnText
        |> WithSourceName "SeriesCustom_RenderItemReturnText"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetPosition" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.position = " + tr "v" + ", $this)")
            "SetRotation" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.rotation = " + tr "v" + ", $this)")
            "SetScale" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.scale = " + tr "v" + ", $this)")
            "SetOrigin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.origin = " + tr "v" + ", $this)")
            "SetZ2" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z2 = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetInfo" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.info = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetInvisible" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.invisible = " + tr "v" + ", $this)")
            "SetIgnore" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.ignore = " + tr "v" + ", $this)")
            "SetStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.style = " + tr "v" + ", $this)")
            "SetStyleEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.styleEmphasis = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "position" =@ (!? (!| T<obj>))
            "rotation" =@ (!? T<float>)
            "scale" =@ (!? (!| T<obj>))
            "origin" =@ (!? T<float>)
            "z2" =@ (!? T<float>)
            "name" =@ (!? T<string>)
            "info" =@ (!? T<obj>)
            "silent" =@ (!? T<bool>)
            "invisible" =@ (!? T<bool>)
            "ignore" =@ (!? T<bool>)
            "style" =@ (!? T<obj>)
            "styleEmphasis" =@ (!? T<obj>)
        ]
        |> ignore

    SeriesCustom_RenderItemReturnRect
        |> WithSourceName "SeriesCustom_RenderItemReturnRect"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetPosition" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.position = " + tr "v" + ", $this)")
            "SetRotation" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.rotation = " + tr "v" + ", $this)")
            "SetScale" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.scale = " + tr "v" + ", $this)")
            "SetOrigin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.origin = " + tr "v" + ", $this)")
            "SetZ2" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z2 = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetInfo" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.info = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetInvisible" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.invisible = " + tr "v" + ", $this)")
            "SetIgnore" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.ignore = " + tr "v" + ", $this)")
            "SetShape" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shape = " + tr "v" + ", $this)")
            "SetStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.style = " + tr "v" + ", $this)")
            "SetStyleEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.styleEmphasis = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "position" =@ (!? (!| T<obj>))
            "rotation" =@ (!? T<float>)
            "scale" =@ (!? (!| T<obj>))
            "origin" =@ (!? T<float>)
            "z2" =@ (!? T<float>)
            "name" =@ (!? T<string>)
            "info" =@ (!? T<obj>)
            "silent" =@ (!? T<bool>)
            "invisible" =@ (!? T<bool>)
            "ignore" =@ (!? T<bool>)
            "shape" =@ (!? T<obj>)
            "style" =@ (!? T<obj>)
            "styleEmphasis" =@ (!? T<obj>)
        ]
        |> ignore

    SeriesCustom_RenderItemReturnCircle
        |> WithSourceName "SeriesCustom_RenderItemReturnCircle"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetPosition" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.position = " + tr "v" + ", $this)")
            "SetRotation" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.rotation = " + tr "v" + ", $this)")
            "SetScale" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.scale = " + tr "v" + ", $this)")
            "SetOrigin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.origin = " + tr "v" + ", $this)")
            "SetZ2" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z2 = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetInfo" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.info = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetInvisible" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.invisible = " + tr "v" + ", $this)")
            "SetIgnore" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.ignore = " + tr "v" + ", $this)")
            "SetShape" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shape = " + tr "v" + ", $this)")
            "SetStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.style = " + tr "v" + ", $this)")
            "SetStyleEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.styleEmphasis = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "position" =@ (!? (!| T<obj>))
            "rotation" =@ (!? T<float>)
            "scale" =@ (!? (!| T<obj>))
            "origin" =@ (!? T<float>)
            "z2" =@ (!? T<float>)
            "name" =@ (!? T<string>)
            "info" =@ (!? T<obj>)
            "silent" =@ (!? T<bool>)
            "invisible" =@ (!? T<bool>)
            "ignore" =@ (!? T<bool>)
            "shape" =@ (!? T<obj>)
            "style" =@ (!? T<obj>)
            "styleEmphasis" =@ (!? T<obj>)
        ]
        |> ignore

    SeriesCustom_RenderItemReturnRing
        |> WithSourceName "SeriesCustom_RenderItemReturnRing"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetPosition" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.position = " + tr "v" + ", $this)")
            "SetRotation" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.rotation = " + tr "v" + ", $this)")
            "SetScale" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.scale = " + tr "v" + ", $this)")
            "SetOrigin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.origin = " + tr "v" + ", $this)")
            "SetZ2" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z2 = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetInfo" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.info = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetInvisible" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.invisible = " + tr "v" + ", $this)")
            "SetIgnore" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.ignore = " + tr "v" + ", $this)")
            "SetShape" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shape = " + tr "v" + ", $this)")
            "SetStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.style = " + tr "v" + ", $this)")
            "SetStyleEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.styleEmphasis = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "position" =@ (!? (!| T<obj>))
            "rotation" =@ (!? T<float>)
            "scale" =@ (!? (!| T<obj>))
            "origin" =@ (!? T<float>)
            "z2" =@ (!? T<float>)
            "name" =@ (!? T<string>)
            "info" =@ (!? T<obj>)
            "silent" =@ (!? T<bool>)
            "invisible" =@ (!? T<bool>)
            "ignore" =@ (!? T<bool>)
            "shape" =@ (!? T<obj>)
            "style" =@ (!? T<obj>)
            "styleEmphasis" =@ (!? T<obj>)
        ]
        |> ignore

    SeriesCustom_RenderItemReturnSector
        |> WithSourceName "SeriesCustom_RenderItemReturnSector"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetPosition" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.position = " + tr "v" + ", $this)")
            "SetRotation" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.rotation = " + tr "v" + ", $this)")
            "SetScale" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.scale = " + tr "v" + ", $this)")
            "SetOrigin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.origin = " + tr "v" + ", $this)")
            "SetZ2" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z2 = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetInfo" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.info = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetInvisible" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.invisible = " + tr "v" + ", $this)")
            "SetIgnore" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.ignore = " + tr "v" + ", $this)")
            "SetShape" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shape = " + tr "v" + ", $this)")
            "SetStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.style = " + tr "v" + ", $this)")
            "SetStyleEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.styleEmphasis = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "position" =@ (!? (!| T<obj>))
            "rotation" =@ (!? T<float>)
            "scale" =@ (!? (!| T<obj>))
            "origin" =@ (!? T<float>)
            "z2" =@ (!? T<float>)
            "name" =@ (!? T<string>)
            "info" =@ (!? T<obj>)
            "silent" =@ (!? T<bool>)
            "invisible" =@ (!? T<bool>)
            "ignore" =@ (!? T<bool>)
            "shape" =@ (!? T<obj>)
            "style" =@ (!? T<obj>)
            "styleEmphasis" =@ (!? T<obj>)
        ]
        |> ignore

    SeriesCustom_RenderItemReturnArc
        |> WithSourceName "SeriesCustom_RenderItemReturnArc"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetPosition" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.position = " + tr "v" + ", $this)")
            "SetRotation" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.rotation = " + tr "v" + ", $this)")
            "SetScale" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.scale = " + tr "v" + ", $this)")
            "SetOrigin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.origin = " + tr "v" + ", $this)")
            "SetZ2" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z2 = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetInfo" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.info = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetInvisible" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.invisible = " + tr "v" + ", $this)")
            "SetIgnore" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.ignore = " + tr "v" + ", $this)")
            "SetShape" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shape = " + tr "v" + ", $this)")
            "SetStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.style = " + tr "v" + ", $this)")
            "SetStyleEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.styleEmphasis = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "position" =@ (!? (!| T<obj>))
            "rotation" =@ (!? T<float>)
            "scale" =@ (!? (!| T<obj>))
            "origin" =@ (!? T<float>)
            "z2" =@ (!? T<float>)
            "name" =@ (!? T<string>)
            "info" =@ (!? T<obj>)
            "silent" =@ (!? T<bool>)
            "invisible" =@ (!? T<bool>)
            "ignore" =@ (!? T<bool>)
            "shape" =@ (!? T<obj>)
            "style" =@ (!? T<obj>)
            "styleEmphasis" =@ (!? T<obj>)
        ]
        |> ignore

    SeriesCustom_RenderItemReturnPolygon
        |> WithSourceName "SeriesCustom_RenderItemReturnPolygon"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetPosition" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.position = " + tr "v" + ", $this)")
            "SetRotation" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.rotation = " + tr "v" + ", $this)")
            "SetScale" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.scale = " + tr "v" + ", $this)")
            "SetOrigin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.origin = " + tr "v" + ", $this)")
            "SetZ2" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z2 = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetInfo" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.info = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetInvisible" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.invisible = " + tr "v" + ", $this)")
            "SetIgnore" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.ignore = " + tr "v" + ", $this)")
            "SetShape" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shape = " + tr "v" + ", $this)")
            "SetStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.style = " + tr "v" + ", $this)")
            "SetStyleEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.styleEmphasis = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "position" =@ (!? (!| T<obj>))
            "rotation" =@ (!? T<float>)
            "scale" =@ (!? (!| T<obj>))
            "origin" =@ (!? T<float>)
            "z2" =@ (!? T<float>)
            "name" =@ (!? T<string>)
            "info" =@ (!? T<obj>)
            "silent" =@ (!? T<bool>)
            "invisible" =@ (!? T<bool>)
            "ignore" =@ (!? T<bool>)
            "shape" =@ (!? T<obj>)
            "style" =@ (!? T<obj>)
            "styleEmphasis" =@ (!? T<obj>)
        ]
        |> ignore

    SeriesCustom_RenderItemReturnPolyline
        |> WithSourceName "SeriesCustom_RenderItemReturnPolyline"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetPosition" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.position = " + tr "v" + ", $this)")
            "SetRotation" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.rotation = " + tr "v" + ", $this)")
            "SetScale" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.scale = " + tr "v" + ", $this)")
            "SetOrigin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.origin = " + tr "v" + ", $this)")
            "SetZ2" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z2 = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetInfo" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.info = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetInvisible" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.invisible = " + tr "v" + ", $this)")
            "SetIgnore" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.ignore = " + tr "v" + ", $this)")
            "SetShape" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shape = " + tr "v" + ", $this)")
            "SetStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.style = " + tr "v" + ", $this)")
            "SetStyleEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.styleEmphasis = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "position" =@ (!? (!| T<obj>))
            "rotation" =@ (!? T<float>)
            "scale" =@ (!? (!| T<obj>))
            "origin" =@ (!? T<float>)
            "z2" =@ (!? T<float>)
            "name" =@ (!? T<string>)
            "info" =@ (!? T<obj>)
            "silent" =@ (!? T<bool>)
            "invisible" =@ (!? T<bool>)
            "ignore" =@ (!? T<bool>)
            "shape" =@ (!? T<obj>)
            "style" =@ (!? T<obj>)
            "styleEmphasis" =@ (!? T<obj>)
        ]
        |> ignore

    SeriesCustom_RenderItemReturnLine
        |> WithSourceName "SeriesCustom_RenderItemReturnLine"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetPosition" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.position = " + tr "v" + ", $this)")
            "SetRotation" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.rotation = " + tr "v" + ", $this)")
            "SetScale" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.scale = " + tr "v" + ", $this)")
            "SetOrigin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.origin = " + tr "v" + ", $this)")
            "SetZ2" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z2 = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetInfo" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.info = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetInvisible" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.invisible = " + tr "v" + ", $this)")
            "SetIgnore" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.ignore = " + tr "v" + ", $this)")
            "SetShape" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shape = " + tr "v" + ", $this)")
            "SetStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.style = " + tr "v" + ", $this)")
            "SetStyleEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.styleEmphasis = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "position" =@ (!? (!| T<obj>))
            "rotation" =@ (!? T<float>)
            "scale" =@ (!? (!| T<obj>))
            "origin" =@ (!? T<float>)
            "z2" =@ (!? T<float>)
            "name" =@ (!? T<string>)
            "info" =@ (!? T<obj>)
            "silent" =@ (!? T<bool>)
            "invisible" =@ (!? T<bool>)
            "ignore" =@ (!? T<bool>)
            "shape" =@ (!? T<obj>)
            "style" =@ (!? T<obj>)
            "styleEmphasis" =@ (!? T<obj>)
        ]
        |> ignore

    SeriesCustom_RenderItemReturnBezierCurve
        |> WithSourceName "SeriesCustom_RenderItemReturnBezierCurve"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetPosition" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.position = " + tr "v" + ", $this)")
            "SetRotation" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.rotation = " + tr "v" + ", $this)")
            "SetScale" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.scale = " + tr "v" + ", $this)")
            "SetOrigin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.origin = " + tr "v" + ", $this)")
            "SetZ2" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z2 = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetInfo" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.info = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetInvisible" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.invisible = " + tr "v" + ", $this)")
            "SetIgnore" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.ignore = " + tr "v" + ", $this)")
            "SetShape" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shape = " + tr "v" + ", $this)")
            "SetStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.style = " + tr "v" + ", $this)")
            "SetStyleEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.styleEmphasis = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "position" =@ (!? (!| T<obj>))
            "rotation" =@ (!? T<float>)
            "scale" =@ (!? (!| T<obj>))
            "origin" =@ (!? T<float>)
            "z2" =@ (!? T<float>)
            "name" =@ (!? T<string>)
            "info" =@ (!? T<obj>)
            "silent" =@ (!? T<bool>)
            "invisible" =@ (!? T<bool>)
            "ignore" =@ (!? T<bool>)
            "shape" =@ (!? T<obj>)
            "style" =@ (!? T<obj>)
            "styleEmphasis" =@ (!? T<obj>)
        ]
        |> ignore

    SeriesEffectScatter
        |> WithSourceName "SeriesEffectScatter"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetLegendHoverLink" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.legendHoverLink = " + tr "v" + ", $this)")
            "SetEffectType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.effectType = " + tr "v" + ", $this)")
            "SetShowEffectOn" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.showEffectOn = " + tr "v" + ", $this)")
            "SetRippleEffect" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.rippleEffect = " + tr "v" + ", $this)")
            "SetCoordinateSystem" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.coordinateSystem = " + tr "v" + ", $this)")
            "SetXAxisIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.xAxisIndex = " + tr "v" + ", $this)")
            "SetYAxisIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.yAxisIndex = " + tr "v" + ", $this)")
            "SetPolarIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.polarIndex = " + tr "v" + ", $this)")
            "SetGeoIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.geoIndex = " + tr "v" + ", $this)")
            "SetCalendarIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.calendarIndex = " + tr "v" + ", $this)")
            "SetSymbol" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbol = " + tr "v" + ", $this)")
            "SetSymbolSize" => ((!| T<obj>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolSize = " + tr "v" + ", $this)")
            "SetSymbolRotate" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolRotate = " + tr "v" + ", $this)")
            "SetSymbolKeepAspect" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolKeepAspect = " + tr "v" + ", $this)")
            "SetSymbolOffset" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolOffset = " + tr "v" + ", $this)")
            "SetCursor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.cursor = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetSeriesLayoutBy" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.seriesLayoutBy = " + tr "v" + ", $this)")
            "SetDatasetIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.datasetIndex = " + tr "v" + ", $this)")
            "SetDimensions" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.dimensions = " + tr "v" + ", $this)")
            "SetEncode" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.encode = " + tr "v" + ", $this)")
            "SetData" => ((!| (T<unit> + T<float> + T<string> + SeriesEffectScatter_DataObject)) + (!| (!| (T<unit> + T<float> + T<string> + SeriesEffectScatter_DataObject))))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetMarkPoint" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markPoint = " + tr "v" + ", $this)")
            "SetMarkLine" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markLine = " + tr "v" + ", $this)")
            "SetMarkArea" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markArea = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animation = " + tr "v" + ", $this)")
            "SetAnimationThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationThreshold = " + tr "v" + ", $this)")
            "SetAnimationDuration" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDuration = " + tr "v" + ", $this)")
            "SetAnimationEasing" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasing = " + tr "v" + ", $this)")
            "SetAnimationDelay" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelay = " + tr "v" + ", $this)")
            "SetAnimationDurationUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDurationUpdate = " + tr "v" + ", $this)")
            "SetAnimationEasingUpdate" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasingUpdate = " + tr "v" + ", $this)")
            "SetAnimationDelayUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelayUpdate = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "name" =@ (!? T<string>)
            "legendHoverLink" =@ (!? T<bool>)
            "effectType" =@ (!? T<string>)
            "showEffectOn" =@ (!? T<string>)
            "rippleEffect" =@ (!? T<obj>)
            "coordinateSystem" =@ (!? T<string>)
            "xAxisIndex" =@ (!? T<float>)
            "yAxisIndex" =@ (!? T<float>)
            "polarIndex" =@ (!? T<float>)
            "geoIndex" =@ (!? T<float>)
            "calendarIndex" =@ (!? T<float>)
            "symbol" =@ (!? T<string>)
            "symbolSize" =@ (!? ((!| T<obj>) + T<float>))
            "symbolRotate" =@ (!? T<float>)
            "symbolKeepAspect" =@ (!? T<bool>)
            "symbolOffset" =@ (!? (!| T<obj>))
            "cursor" =@ (!? T<string>)
            "label" =@ (!? T<obj>)
            "itemStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "seriesLayoutBy" =@ (!? T<string>)
            "datasetIndex" =@ (!? T<float>)
            "dimensions" =@ (!? (!| T<obj>))
            "encode" =@ (!? T<obj>)
            "data" =@ (!? ((!| (T<unit> + T<float> + T<string> + SeriesEffectScatter_DataObject)) + (!| (!| (T<unit> + T<float> + T<string> + SeriesEffectScatter_DataObject)))))
            "markPoint" =@ (!? T<obj>)
            "markLine" =@ (!? T<obj>)
            "markArea" =@ (!? T<obj>)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "animation" =@ (!? T<bool>)
            "animationThreshold" =@ (!? T<float>)
            "animationDuration" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasing" =@ (!? T<string>)
            "animationDelay" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationDurationUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasingUpdate" =@ (!? T<string>)
            "animationDelayUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesEffectScatter_DataObject
        |> WithSourceName "SeriesEffectScatter_DataObject"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetSymbol" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbol = " + tr "v" + ", $this)")
            "SetSymbolSize" => ((!| T<obj>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolSize = " + tr "v" + ", $this)")
            "SetSymbolRotate" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolRotate = " + tr "v" + ", $this)")
            "SetSymbolKeepAspect" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolKeepAspect = " + tr "v" + ", $this)")
            "SetSymbolOffset" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolOffset = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "symbol" =@ (!? T<string>)
            "symbolSize" =@ (!? ((!| T<obj>) + T<float>))
            "symbolRotate" =@ (!? T<float>)
            "symbolKeepAspect" =@ (!? T<bool>)
            "symbolOffset" =@ (!? (!| T<obj>))
            "label" =@ (!? T<obj>)
            "itemStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesFunnel
        |> WithSourceName "SeriesFunnel"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetMin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.min = " + tr "v" + ", $this)")
            "SetMax" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.max = " + tr "v" + ", $this)")
            "SetMinSize" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.minSize = " + tr "v" + ", $this)")
            "SetMaxSize" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.maxSize = " + tr "v" + ", $this)")
            "SetSort" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.sort = " + tr "v" + ", $this)")
            "SetGap" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.gap = " + tr "v" + ", $this)")
            "SetLegendHoverLink" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.legendHoverLink = " + tr "v" + ", $this)")
            "SetFunnelAlign" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.funnelAlign = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetLabelLine" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.labelLine = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetSeriesLayoutBy" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.seriesLayoutBy = " + tr "v" + ", $this)")
            "SetDatasetIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.datasetIndex = " + tr "v" + ", $this)")
            "SetData" => ((!| T<float>) + (!| (!| T<float>)) + (!| SeriesFunnel_DataObject))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetMarkPoint" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markPoint = " + tr "v" + ", $this)")
            "SetMarkLine" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markLine = " + tr "v" + ", $this)")
            "SetMarkArea" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markArea = " + tr "v" + ", $this)")
            "SetAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animation = " + tr "v" + ", $this)")
            "SetAnimationThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationThreshold = " + tr "v" + ", $this)")
            "SetAnimationDuration" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDuration = " + tr "v" + ", $this)")
            "SetAnimationEasing" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasing = " + tr "v" + ", $this)")
            "SetAnimationDelay" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelay = " + tr "v" + ", $this)")
            "SetAnimationDurationUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDurationUpdate = " + tr "v" + ", $this)")
            "SetAnimationEasingUpdate" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasingUpdate = " + tr "v" + ", $this)")
            "SetAnimationDelayUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelayUpdate = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "name" =@ (!? T<string>)
            "min" =@ (!? T<float>)
            "max" =@ (!? T<float>)
            "minSize" =@ (!? T<string>)
            "maxSize" =@ (!? T<string>)
            "sort" =@ (!? T<string>)
            "gap" =@ (!? T<float>)
            "legendHoverLink" =@ (!? T<bool>)
            "funnelAlign" =@ (!? T<string>)
            "label" =@ (!? T<obj>)
            "labelLine" =@ (!? T<obj>)
            "itemStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "seriesLayoutBy" =@ (!? T<string>)
            "datasetIndex" =@ (!? T<float>)
            "data" =@ (!? ((!| T<float>) + (!| (!| T<float>)) + (!| SeriesFunnel_DataObject)))
            "markPoint" =@ (!? T<obj>)
            "markLine" =@ (!? T<obj>)
            "markArea" =@ (!? T<obj>)
            "animation" =@ (!? T<bool>)
            "animationThreshold" =@ (!? T<float>)
            "animationDuration" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasing" =@ (!? T<string>)
            "animationDelay" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationDurationUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasingUpdate" =@ (!? T<string>)
            "animationDelayUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesFunnel_DataObject
        |> WithSourceName "SeriesFunnel_DataObject"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetValue" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.value = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetLabelLine" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.labelLine = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "name" =@ (!? T<string>)
            "value" =@ (!? T<float>)
            "itemStyle" =@ (!? T<obj>)
            "label" =@ (!? T<obj>)
            "labelLine" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesGauge
        |> WithSourceName "SeriesGauge"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetRadius" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.radius = " + tr "v" + ", $this)")
            "SetStartAngle" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.startAngle = " + tr "v" + ", $this)")
            "SetEndAngle" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.endAngle = " + tr "v" + ", $this)")
            "SetClockwise" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.clockwise = " + tr "v" + ", $this)")
            "SetData" => ((!| (T<unit> + T<string> + T<float> + SeriesGauge_DataObject)) + (!| (!| (T<unit> + T<string> + T<float> + SeriesGauge_DataObject))))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetMin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.min = " + tr "v" + ", $this)")
            "SetMax" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.max = " + tr "v" + ", $this)")
            "SetSplitNumber" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.splitNumber = " + tr "v" + ", $this)")
            "SetAxisLine" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.axisLine = " + tr "v" + ", $this)")
            "SetSplitLine" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.splitLine = " + tr "v" + ", $this)")
            "SetAxisTick" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.axisTick = " + tr "v" + ", $this)")
            "SetAxisLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.axisLabel = " + tr "v" + ", $this)")
            "SetPointer" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.pointer = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetTitle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.title = " + tr "v" + ", $this)")
            "SetDetail" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.detail = " + tr "v" + ", $this)")
            "SetMarkPoint" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markPoint = " + tr "v" + ", $this)")
            "SetMarkLine" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markLine = " + tr "v" + ", $this)")
            "SetMarkArea" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markArea = " + tr "v" + ", $this)")
            "SetAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animation = " + tr "v" + ", $this)")
            "SetAnimationThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationThreshold = " + tr "v" + ", $this)")
            "SetAnimationDuration" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDuration = " + tr "v" + ", $this)")
            "SetAnimationEasing" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasing = " + tr "v" + ", $this)")
            "SetAnimationDelay" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelay = " + tr "v" + ", $this)")
            "SetAnimationDurationUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDurationUpdate = " + tr "v" + ", $this)")
            "SetAnimationEasingUpdate" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasingUpdate = " + tr "v" + ", $this)")
            "SetAnimationDelayUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelayUpdate = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "name" =@ (!? T<string>)
            "radius" =@ (!? (T<float> + T<string>))
            "startAngle" =@ (!? T<float>)
            "endAngle" =@ (!? T<float>)
            "clockwise" =@ (!? T<bool>)
            "data" =@ (!? ((!| (T<unit> + T<string> + T<float> + SeriesGauge_DataObject)) + (!| (!| (T<unit> + T<string> + T<float> + SeriesGauge_DataObject)))))
            "min" =@ (!? T<float>)
            "max" =@ (!? T<float>)
            "splitNumber" =@ (!? T<float>)
            "axisLine" =@ (!? T<obj>)
            "splitLine" =@ (!? T<obj>)
            "axisTick" =@ (!? T<obj>)
            "axisLabel" =@ (!? T<obj>)
            "pointer" =@ (!? T<obj>)
            "itemStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "title" =@ (!? T<obj>)
            "detail" =@ (!? T<obj>)
            "markPoint" =@ (!? T<obj>)
            "markLine" =@ (!? T<obj>)
            "markArea" =@ (!? T<obj>)
            "animation" =@ (!? T<bool>)
            "animationThreshold" =@ (!? T<float>)
            "animationDuration" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasing" =@ (!? T<string>)
            "animationDelay" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationDurationUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasingUpdate" =@ (!? T<string>)
            "animationDelayUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesGauge_DataObject
        |> WithSourceName "SeriesGauge_DataObject"
        |+> Static [ ObjectConstructor ((!? T<string>)?name * (!? T<float>)?value) ]
        |+> Instance [
            "name" =@ (!? T<string>)
            "value" =@ (!? T<float>)
        ]
        |> ignore

    SeriesGraph
        |> WithSourceName "SeriesGraph"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetLegendHoverLink" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.legendHoverLink = " + tr "v" + ", $this)")
            "SetCoordinateSystem" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.coordinateSystem = " + tr "v" + ", $this)")
            "SetXAxisIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.xAxisIndex = " + tr "v" + ", $this)")
            "SetYAxisIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.yAxisIndex = " + tr "v" + ", $this)")
            "SetPolarIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.polarIndex = " + tr "v" + ", $this)")
            "SetGeoIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.geoIndex = " + tr "v" + ", $this)")
            "SetCalendarIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.calendarIndex = " + tr "v" + ", $this)")
            "SetHoverAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.hoverAnimation = " + tr "v" + ", $this)")
            "SetLayout" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.layout = " + tr "v" + ", $this)")
            "SetCircular" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.circular = " + tr "v" + ", $this)")
            "SetForce" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.force = " + tr "v" + ", $this)")
            "SetRoam" => (T<bool> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.roam = " + tr "v" + ", $this)")
            "SetNodeScaleRatio" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nodeScaleRatio = " + tr "v" + ", $this)")
            "SetDraggable" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.draggable = " + tr "v" + ", $this)")
            "SetSymbol" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbol = " + tr "v" + ", $this)")
            "SetSymbolSize" => ((!| T<obj>) + (T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolSize = " + tr "v" + ", $this)")
            "SetSymbolRotate" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolRotate = " + tr "v" + ", $this)")
            "SetSymbolKeepAspect" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolKeepAspect = " + tr "v" + ", $this)")
            "SetSymbolOffset" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolOffset = " + tr "v" + ", $this)")
            "SetFocusNodeAdjacency" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.focusNodeAdjacency = " + tr "v" + ", $this)")
            "SetEdgeSymbol" => ((!| T<obj>) + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.edgeSymbol = " + tr "v" + ", $this)")
            "SetEdgeSymbolSize" => ((!| T<obj>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.edgeSymbolSize = " + tr "v" + ", $this)")
            "SetCursor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.cursor = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetLineStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineStyle = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetEdgeLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.edgeLabel = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetCategories" => (!| SeriesGraph_CategoryObject)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.categories = " + tr "v" + ", $this)")
            "SetAutoCurveness" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.autoCurveness = " + tr "v" + ", $this)")
            "SetData" => (!| SeriesGraph_DataObject)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetNodes" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nodes = " + tr "v" + ", $this)")
            "SetLinks" => (!| SeriesGraph_LinkObject)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.links = " + tr "v" + ", $this)")
            "SetEdges" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.edges = " + tr "v" + ", $this)")
            "SetMarkPoint" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markPoint = " + tr "v" + ", $this)")
            "SetMarkLine" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markLine = " + tr "v" + ", $this)")
            "SetMarkArea" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markArea = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetLeft" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.left = " + tr "v" + ", $this)")
            "SetTop" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.top = " + tr "v" + ", $this)")
            "SetRight" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.right = " + tr "v" + ", $this)")
            "SetBottom" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.bottom = " + tr "v" + ", $this)")
            "SetWidth" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.width = " + tr "v" + ", $this)")
            "SetHeight" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.height = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animation = " + tr "v" + ", $this)")
            "SetAnimationThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationThreshold = " + tr "v" + ", $this)")
            "SetAnimationDuration" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDuration = " + tr "v" + ", $this)")
            "SetAnimationEasing" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasing = " + tr "v" + ", $this)")
            "SetAnimationDelay" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelay = " + tr "v" + ", $this)")
            "SetAnimationDurationUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDurationUpdate = " + tr "v" + ", $this)")
            "SetAnimationEasingUpdate" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasingUpdate = " + tr "v" + ", $this)")
            "SetAnimationDelayUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelayUpdate = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "name" =@ (!? T<string>)
            "legendHoverLink" =@ (!? T<bool>)
            "coordinateSystem" =@ (!? T<string>)
            "xAxisIndex" =@ (!? T<float>)
            "yAxisIndex" =@ (!? T<float>)
            "polarIndex" =@ (!? T<float>)
            "geoIndex" =@ (!? T<float>)
            "calendarIndex" =@ (!? T<float>)
            "hoverAnimation" =@ (!? T<bool>)
            "layout" =@ (!? T<string>)
            "circular" =@ (!? T<obj>)
            "force" =@ (!? T<obj>)
            "roam" =@ (!? (T<bool> + T<string>))
            "nodeScaleRatio" =@ (!? T<float>)
            "draggable" =@ (!? T<bool>)
            "symbol" =@ (!? T<string>)
            "symbolSize" =@ (!? ((!| T<obj>) + (T<unit> ^-> T<unit>) + T<float>))
            "symbolRotate" =@ (!? T<float>)
            "symbolKeepAspect" =@ (!? T<bool>)
            "symbolOffset" =@ (!? (!| T<obj>))
            "focusNodeAdjacency" =@ (!? T<bool>)
            "edgeSymbol" =@ (!? ((!| T<obj>) + T<string>))
            "edgeSymbolSize" =@ (!? ((!| T<obj>) + T<float>))
            "cursor" =@ (!? T<string>)
            "itemStyle" =@ (!? T<obj>)
            "lineStyle" =@ (!? T<obj>)
            "label" =@ (!? T<obj>)
            "edgeLabel" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "categories" =@ (!? (!| SeriesGraph_CategoryObject))
            "autoCurveness" =@ (!? (T<float> + (!| T<float>)))
            "data" =@ (!? (!| SeriesGraph_DataObject))
            "nodes" =@ (!? (!| T<obj>))
            "links" =@ (!? (!| SeriesGraph_LinkObject))
            "edges" =@ (!? (!| T<obj>))
            "markPoint" =@ (!? T<obj>)
            "markLine" =@ (!? T<obj>)
            "markArea" =@ (!? T<obj>)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "left" =@ (!? (T<float> + T<string>))
            "top" =@ (!? (T<float> + T<string>))
            "right" =@ (!? (T<float> + T<string>))
            "bottom" =@ (!? (T<float> + T<string>))
            "width" =@ (!? (T<float> + T<string>))
            "height" =@ (!? (T<float> + T<string>))
            "silent" =@ (!? T<bool>)
            "animation" =@ (!? T<bool>)
            "animationThreshold" =@ (!? T<float>)
            "animationDuration" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasing" =@ (!? T<string>)
            "animationDelay" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationDurationUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasingUpdate" =@ (!? T<string>)
            "animationDelayUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesGraph_CategoryObject
        |> WithSourceName "SeriesGraph_CategoryObject"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetSymbol" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbol = " + tr "v" + ", $this)")
            "SetSymbolSize" => ((!| T<obj>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolSize = " + tr "v" + ", $this)")
            "SetSymbolRotate" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolRotate = " + tr "v" + ", $this)")
            "SetSymbolKeepAspect" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolKeepAspect = " + tr "v" + ", $this)")
            "SetSymbolOffset" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolOffset = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "name" =@ (!? T<string>)
            "symbol" =@ (!? T<string>)
            "symbolSize" =@ (!? ((!| T<obj>) + T<float>))
            "symbolRotate" =@ (!? T<float>)
            "symbolKeepAspect" =@ (!? T<bool>)
            "symbolOffset" =@ (!? (!| T<obj>))
            "itemStyle" =@ (!? T<obj>)
            "label" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
        ]
        |> ignore

    SeriesGraph_DataObject
        |> WithSourceName "SeriesGraph_DataObject"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetX" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.x = " + tr "v" + ", $this)")
            "SetY" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.y = " + tr "v" + ", $this)")
            "SetFixed" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fixed = " + tr "v" + ", $this)")
            "SetValue" => ((!| T<obj>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.value = " + tr "v" + ", $this)")
            "SetCategory" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.category = " + tr "v" + ", $this)")
            "SetSymbol" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbol = " + tr "v" + ", $this)")
            "SetSymbolSize" => ((!| T<obj>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolSize = " + tr "v" + ", $this)")
            "SetSymbolRotate" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolRotate = " + tr "v" + ", $this)")
            "SetSymbolKeepAspect" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolKeepAspect = " + tr "v" + ", $this)")
            "SetSymbolOffset" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolOffset = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "name" =@ (!? T<string>)
            "x" =@ (!? T<float>)
            "y" =@ (!? T<float>)
            "fixed" =@ (!? T<bool>)
            "value" =@ (!? ((!| T<obj>) + T<float>))
            "category" =@ (!? T<float>)
            "symbol" =@ (!? T<string>)
            "symbolSize" =@ (!? ((!| T<obj>) + T<float>))
            "symbolRotate" =@ (!? T<float>)
            "symbolKeepAspect" =@ (!? T<bool>)
            "symbolOffset" =@ (!? (!| T<obj>))
            "itemStyle" =@ (!? T<obj>)
            "label" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesGraph_LinkObject
        |> WithSourceName "SeriesGraph_LinkObject"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetSource" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.source = " + tr "v" + ", $this)")
            "SetTarget" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.target = " + tr "v" + ", $this)")
            "SetValue" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.value = " + tr "v" + ", $this)")
            "SetLineStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineStyle = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetSymbol" => ((!| T<obj>) + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbol = " + tr "v" + ", $this)")
            "SetSymbolSize" => ((!| T<obj>) + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolSize = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "source" =@ (!? T<string>)
            "target" =@ (!? T<string>)
            "value" =@ (!? T<float>)
            "lineStyle" =@ (!? T<obj>)
            "label" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "symbol" =@ (!? ((!| T<obj>) + T<string>))
            "symbolSize" =@ (!? ((!| T<obj>) + T<string>))
        ]
        |> ignore

    SeriesHeatmap
        |> WithSourceName "SeriesHeatmap"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetCoordinateSystem" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.coordinateSystem = " + tr "v" + ", $this)")
            "SetXAxisIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.xAxisIndex = " + tr "v" + ", $this)")
            "SetYAxisIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.yAxisIndex = " + tr "v" + ", $this)")
            "SetGeoIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.geoIndex = " + tr "v" + ", $this)")
            "SetCalendarIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.calendarIndex = " + tr "v" + ", $this)")
            "SetBlurSize" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.blurSize = " + tr "v" + ", $this)")
            "SetMinOpacity" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.minOpacity = " + tr "v" + ", $this)")
            "SetMaxOpacity" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.maxOpacity = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetData" => ((!| (T<unit> + T<float> + T<string> + SeriesHeatmap_DataObject)) + (!| (!| (T<unit> + T<float> + T<string> + SeriesHeatmap_DataObject))))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetMarkPoint" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markPoint = " + tr "v" + ", $this)")
            "SetMarkLine" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markLine = " + tr "v" + ", $this)")
            "SetMarkArea" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markArea = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "name" =@ (!? T<string>)
            "coordinateSystem" =@ (!? T<string>)
            "xAxisIndex" =@ (!? T<float>)
            "yAxisIndex" =@ (!? T<float>)
            "geoIndex" =@ (!? T<float>)
            "calendarIndex" =@ (!? T<float>)
            "blurSize" =@ (!? T<float>)
            "minOpacity" =@ (!? T<float>)
            "maxOpacity" =@ (!? T<float>)
            "label" =@ (!? T<obj>)
            "itemStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "data" =@ (!? ((!| (T<unit> + T<float> + T<string> + SeriesHeatmap_DataObject)) + (!| (!| (T<unit> + T<float> + T<string> + SeriesHeatmap_DataObject)))))
            "markPoint" =@ (!? T<obj>)
            "markLine" =@ (!? T<obj>)
            "markArea" =@ (!? T<obj>)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "silent" =@ (!? T<bool>)
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesHeatmap_DataObject
        |> WithSourceName "SeriesHeatmap_DataObject"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetValue" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.value = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "name" =@ (!? T<string>)
            "value" =@ (!? (!| T<obj>))
            "label" =@ (!? T<obj>)
            "itemStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
        ]
        |> ignore

    SeriesLine
        |> WithSourceName "SeriesLine"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetCoordinateSystem" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.coordinateSystem = " + tr "v" + ", $this)")
            "SetXAxisIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.xAxisIndex = " + tr "v" + ", $this)")
            "SetYAxisIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.yAxisIndex = " + tr "v" + ", $this)")
            "SetPolarIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.polarIndex = " + tr "v" + ", $this)")
            "SetSymbol" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbol = " + tr "v" + ", $this)")
            "SetSymbolSize" => ((!| T<obj>) + (T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolSize = " + tr "v" + ", $this)")
            "SetSymbolRotate" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolRotate = " + tr "v" + ", $this)")
            "SetSymbolKeepAspect" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolKeepAspect = " + tr "v" + ", $this)")
            "SetSymbolOffset" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolOffset = " + tr "v" + ", $this)")
            "SetShowSymbol" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.showSymbol = " + tr "v" + ", $this)")
            "SetShowAllSymbol" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.showAllSymbol = " + tr "v" + ", $this)")
            "SetHoverAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.hoverAnimation = " + tr "v" + ", $this)")
            "SetLegendHoverLink" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.legendHoverLink = " + tr "v" + ", $this)")
            "SetStack" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.stack = " + tr "v" + ", $this)")
            "SetCursor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.cursor = " + tr "v" + ", $this)")
            "SetConnectNulls" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.connectNulls = " + tr "v" + ", $this)")
            "SetClipOverflow" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.clipOverflow = " + tr "v" + ", $this)")
            "SetStep" => (T<bool> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.step = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetLineStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineStyle = " + tr "v" + ", $this)")
            "SetAreaStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.areaStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetSmooth" => (T<bool> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.smooth = " + tr "v" + ", $this)")
            "SetSmoothMonotone" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.smoothMonotone = " + tr "v" + ", $this)")
            "SetSampling" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.sampling = " + tr "v" + ", $this)")
            "SetDimensions" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.dimensions = " + tr "v" + ", $this)")
            "SetEncode" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.encode = " + tr "v" + ", $this)")
            "SetSeriesLayoutBy" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.seriesLayoutBy = " + tr "v" + ", $this)")
            "SetDatasetIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.datasetIndex = " + tr "v" + ", $this)")
            "SetData" => ((!| (T<unit> + T<string> + T<float> + SeriesLine_DataObject)) + (!| (!| (T<unit> + T<string> + T<float> + SeriesLine_DataObject))))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetMarkPoint" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markPoint = " + tr "v" + ", $this)")
            "SetMarkLine" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markLine = " + tr "v" + ", $this)")
            "SetMarkArea" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markArea = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animation = " + tr "v" + ", $this)")
            "SetAnimationThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationThreshold = " + tr "v" + ", $this)")
            "SetAnimationDuration" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDuration = " + tr "v" + ", $this)")
            "SetAnimationEasing" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasing = " + tr "v" + ", $this)")
            "SetAnimationDelay" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelay = " + tr "v" + ", $this)")
            "SetAnimationDurationUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDurationUpdate = " + tr "v" + ", $this)")
            "SetAnimationEasingUpdate" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasingUpdate = " + tr "v" + ", $this)")
            "SetAnimationDelayUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelayUpdate = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "name" =@ (!? T<string>)
            "coordinateSystem" =@ (!? T<string>)
            "xAxisIndex" =@ (!? T<float>)
            "yAxisIndex" =@ (!? T<float>)
            "polarIndex" =@ (!? T<float>)
            "symbol" =@ (!? T<string>)
            "symbolSize" =@ (!? ((!| T<obj>) + (T<unit> ^-> T<unit>) + T<float>))
            "symbolRotate" =@ (!? T<float>)
            "symbolKeepAspect" =@ (!? T<bool>)
            "symbolOffset" =@ (!? (!| T<obj>))
            "showSymbol" =@ (!? T<bool>)
            "showAllSymbol" =@ (!? T<bool>)
            "hoverAnimation" =@ (!? T<bool>)
            "legendHoverLink" =@ (!? T<bool>)
            "stack" =@ (!? T<string>)
            "cursor" =@ (!? T<string>)
            "connectNulls" =@ (!? T<bool>)
            "clipOverflow" =@ (!? T<bool>)
            "step" =@ (!? (T<bool> + T<string>))
            "label" =@ (!? T<obj>)
            "itemStyle" =@ (!? T<obj>)
            "lineStyle" =@ (!? T<obj>)
            "areaStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "smooth" =@ (!? (T<bool> + T<float>))
            "smoothMonotone" =@ (!? T<string>)
            "sampling" =@ (!? T<string>)
            "dimensions" =@ (!? (!| T<obj>))
            "encode" =@ (!? T<obj>)
            "seriesLayoutBy" =@ (!? T<string>)
            "datasetIndex" =@ (!? T<float>)
            "data" =@ (!? ((!| (T<unit> + T<string> + T<float> + SeriesLine_DataObject)) + (!| (!| (T<unit> + T<string> + T<float> + SeriesLine_DataObject)))))
            "markPoint" =@ (!? T<obj>)
            "markLine" =@ (!? T<obj>)
            "markArea" =@ (!? T<obj>)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "silent" =@ (!? T<bool>)
            "animation" =@ (!? T<bool>)
            "animationThreshold" =@ (!? T<float>)
            "animationDuration" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasing" =@ (!? T<string>)
            "animationDelay" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationDurationUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasingUpdate" =@ (!? T<string>)
            "animationDelayUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesLine_DataObject
        |> WithSourceName "SeriesLine_DataObject"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetValue" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.value = " + tr "v" + ", $this)")
            "SetSymbol" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbol = " + tr "v" + ", $this)")
            "SetSymbolSize" => ((!| T<obj>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolSize = " + tr "v" + ", $this)")
            "SetSymbolRotate" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolRotate = " + tr "v" + ", $this)")
            "SetSymbolKeepAspect" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolKeepAspect = " + tr "v" + ", $this)")
            "SetSymbolOffset" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolOffset = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "name" =@ (!? T<string>)
            "value" =@ (!? T<float>)
            "symbol" =@ (!? T<string>)
            "symbolSize" =@ (!? ((!| T<obj>) + T<float>))
            "symbolRotate" =@ (!? T<float>)
            "symbolKeepAspect" =@ (!? T<bool>)
            "symbolOffset" =@ (!? (!| T<obj>))
            "label" =@ (!? T<obj>)
            "itemStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesLines
        |> WithSourceName "SeriesLines"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetCoordinateSystem" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.coordinateSystem = " + tr "v" + ", $this)")
            "SetXAxisIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.xAxisIndex = " + tr "v" + ", $this)")
            "SetYAxisIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.yAxisIndex = " + tr "v" + ", $this)")
            "SetGeoIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.geoIndex = " + tr "v" + ", $this)")
            "SetPolyline" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.polyline = " + tr "v" + ", $this)")
            "SetEffect" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.effect = " + tr "v" + ", $this)")
            "SetLarge" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.large = " + tr "v" + ", $this)")
            "SetLargeThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.largeThreshold = " + tr "v" + ", $this)")
            "SetSymbol" => ((!| T<obj>) + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbol = " + tr "v" + ", $this)")
            "SetSymbolSize" => ((!| T<obj>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolSize = " + tr "v" + ", $this)")
            "SetLineStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineStyle = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetProgressive" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.progressive = " + tr "v" + ", $this)")
            "SetProgressiveThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.progressiveThreshold = " + tr "v" + ", $this)")
            "SetData" => (!| SeriesLines_DataObject)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetMarkPoint" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markPoint = " + tr "v" + ", $this)")
            "SetMarkLine" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markLine = " + tr "v" + ", $this)")
            "SetMarkArea" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markArea = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animation = " + tr "v" + ", $this)")
            "SetAnimationThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationThreshold = " + tr "v" + ", $this)")
            "SetAnimationDuration" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDuration = " + tr "v" + ", $this)")
            "SetAnimationEasing" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasing = " + tr "v" + ", $this)")
            "SetAnimationDelay" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelay = " + tr "v" + ", $this)")
            "SetAnimationDurationUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDurationUpdate = " + tr "v" + ", $this)")
            "SetAnimationEasingUpdate" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasingUpdate = " + tr "v" + ", $this)")
            "SetAnimationDelayUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelayUpdate = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "name" =@ (!? T<string>)
            "coordinateSystem" =@ (!? T<string>)
            "xAxisIndex" =@ (!? T<float>)
            "yAxisIndex" =@ (!? T<float>)
            "geoIndex" =@ (!? T<float>)
            "polyline" =@ (!? T<bool>)
            "effect" =@ (!? T<obj>)
            "large" =@ (!? T<bool>)
            "largeThreshold" =@ (!? T<float>)
            "symbol" =@ (!? ((!| T<obj>) + T<string>))
            "symbolSize" =@ (!? ((!| T<obj>) + T<float>))
            "lineStyle" =@ (!? T<obj>)
            "label" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "progressive" =@ (!? T<float>)
            "progressiveThreshold" =@ (!? T<float>)
            "data" =@ (!? (!| SeriesLines_DataObject))
            "markPoint" =@ (!? T<obj>)
            "markLine" =@ (!? T<obj>)
            "markArea" =@ (!? T<obj>)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "silent" =@ (!? T<bool>)
            "animation" =@ (!? T<bool>)
            "animationThreshold" =@ (!? T<float>)
            "animationDuration" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasing" =@ (!? T<string>)
            "animationDelay" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationDurationUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasingUpdate" =@ (!? T<string>)
            "animationDelayUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
        ]
        |> ignore

    SeriesLines_DataObject
        |> WithSourceName "SeriesLines_DataObject"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetCoords" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.coords = " + tr "v" + ", $this)")
            "SetLineStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineStyle = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "name" =@ (!? T<string>)
            "coords" =@ (!? (!| T<obj>))
            "lineStyle" =@ (!? T<obj>)
            "label" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
        ]
        |> ignore

    SeriesMap
        |> WithSourceName "SeriesMap"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetMap" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.map = " + tr "v" + ", $this)")
            "SetRoam" => (T<bool> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.roam = " + tr "v" + ", $this)")
            "SetCenter" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.center = " + tr "v" + ", $this)")
            "SetAspectScale" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.aspectScale = " + tr "v" + ", $this)")
            "SetBoundingCoords" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.boundingCoords = " + tr "v" + ", $this)")
            "SetZoom" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zoom = " + tr "v" + ", $this)")
            "SetScaleLimit" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.scaleLimit = " + tr "v" + ", $this)")
            "SetNameMap" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nameMap = " + tr "v" + ", $this)")
            "SetSelectedMode" => (T<bool> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.selectedMode = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetLeft" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.left = " + tr "v" + ", $this)")
            "SetTop" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.top = " + tr "v" + ", $this)")
            "SetRight" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.right = " + tr "v" + ", $this)")
            "SetBottom" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.bottom = " + tr "v" + ", $this)")
            "SetLayoutCenter" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.layoutCenter = " + tr "v" + ", $this)")
            "SetLayoutSize" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.layoutSize = " + tr "v" + ", $this)")
            "SetGeoIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.geoIndex = " + tr "v" + ", $this)")
            "SetMapValueCalculation" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.mapValueCalculation = " + tr "v" + ", $this)")
            "SetShowLegendSymbol" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.showLegendSymbol = " + tr "v" + ", $this)")
            "SetSeriesLayoutBy" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.seriesLayoutBy = " + tr "v" + ", $this)")
            "SetDatasetIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.datasetIndex = " + tr "v" + ", $this)")
            "SetData" => (!| (T<float> + SeriesMap_DataObject))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetMarkPoint" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markPoint = " + tr "v" + ", $this)")
            "SetMarkLine" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markLine = " + tr "v" + ", $this)")
            "SetMarkArea" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markArea = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "name" =@ (!? T<string>)
            "map" =@ (!? T<string>)
            "roam" =@ (!? (T<bool> + T<string>))
            "center" =@ (!? (!| T<obj>))
            "aspectScale" =@ (!? T<float>)
            "boundingCoords" =@ (!? (!| T<obj>))
            "zoom" =@ (!? T<float>)
            "scaleLimit" =@ (!? T<obj>)
            "nameMap" =@ (!? T<obj>)
            "selectedMode" =@ (!? (T<bool> + T<string>))
            "label" =@ (!? T<obj>)
            "itemStyle" =@ (!? T<obj>)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "left" =@ (!? (T<float> + T<string>))
            "top" =@ (!? (T<float> + T<string>))
            "right" =@ (!? (T<float> + T<string>))
            "bottom" =@ (!? (T<float> + T<string>))
            "layoutCenter" =@ (!? (!| T<obj>))
            "layoutSize" =@ (!? (T<float> + T<string>))
            "geoIndex" =@ (!? T<float>)
            "mapValueCalculation" =@ (!? T<string>)
            "showLegendSymbol" =@ (!? T<bool>)
            "seriesLayoutBy" =@ (!? T<string>)
            "datasetIndex" =@ (!? T<float>)
            "data" =@ (!? (!| (T<float> + SeriesMap_DataObject)))
            "markPoint" =@ (!? T<obj>)
            "markLine" =@ (!? T<obj>)
            "markArea" =@ (!? T<obj>)
            "silent" =@ (!? T<bool>)
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesMap_DataObject
        |> WithSourceName "SeriesMap_DataObject"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetValue" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.value = " + tr "v" + ", $this)")
            "SetSelected" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.selected = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "name" =@ (!? T<string>)
            "value" =@ (!? T<float>)
            "selected" =@ (!? T<bool>)
            "itemStyle" =@ (!? T<obj>)
            "label" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesParallel
        |> WithSourceName "SeriesParallel"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetCoordinateSystem" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.coordinateSystem = " + tr "v" + ", $this)")
            "SetParallelIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.parallelIndex = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetLineStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetInactiveOpacity" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.inactiveOpacity = " + tr "v" + ", $this)")
            "SetActiveOpacity" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.activeOpacity = " + tr "v" + ", $this)")
            "SetRealtime" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.realtime = " + tr "v" + ", $this)")
            "SetSmooth" => (T<bool> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.smooth = " + tr "v" + ", $this)")
            "SetProgressive" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.progressive = " + tr "v" + ", $this)")
            "SetProgressiveThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.progressiveThreshold = " + tr "v" + ", $this)")
            "SetProgressiveChunkMode" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.progressiveChunkMode = " + tr "v" + ", $this)")
            "SetData" => ((!| (T<float> + T<string> + SeriesParallel_DataObject)) + (!| (!| (T<float> + T<string> + SeriesParallel_DataObject))))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animation = " + tr "v" + ", $this)")
            "SetAnimationThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationThreshold = " + tr "v" + ", $this)")
            "SetAnimationDuration" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDuration = " + tr "v" + ", $this)")
            "SetAnimationEasing" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasing = " + tr "v" + ", $this)")
            "SetAnimationDelay" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelay = " + tr "v" + ", $this)")
            "SetAnimationDurationUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDurationUpdate = " + tr "v" + ", $this)")
            "SetAnimationEasingUpdate" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasingUpdate = " + tr "v" + ", $this)")
            "SetAnimationDelayUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelayUpdate = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "coordinateSystem" =@ (!? T<string>)
            "parallelIndex" =@ (!? T<float>)
            "name" =@ (!? T<string>)
            "lineStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "inactiveOpacity" =@ (!? T<float>)
            "activeOpacity" =@ (!? T<float>)
            "realtime" =@ (!? T<bool>)
            "smooth" =@ (!? (T<bool> + T<float>))
            "progressive" =@ (!? T<float>)
            "progressiveThreshold" =@ (!? T<float>)
            "progressiveChunkMode" =@ (!? T<string>)
            "data" =@ (!? ((!| (T<float> + T<string> + SeriesParallel_DataObject)) + (!| (!| (T<float> + T<string> + SeriesParallel_DataObject)))))
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "silent" =@ (!? T<bool>)
            "animation" =@ (!? T<bool>)
            "animationThreshold" =@ (!? T<float>)
            "animationDuration" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasing" =@ (!? T<string>)
            "animationDelay" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationDurationUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasingUpdate" =@ (!? T<string>)
            "animationDelayUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesParallel_DataObject
        |> WithSourceName "SeriesParallel_DataObject"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetValue" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.value = " + tr "v" + ", $this)")
            "SetLineStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineStyle = " + tr "v" + ", $this)")
            "SetColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.color = " + tr "v" + ", $this)")
            "SetWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.width = " + tr "v" + ", $this)")
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetShadowBlur" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowBlur = " + tr "v" + ", $this)")
            "SetShadowColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowColor = " + tr "v" + ", $this)")
            "SetShadowOffsetX" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowOffsetX = " + tr "v" + ", $this)")
            "SetShadowOffsetY" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowOffsetY = " + tr "v" + ", $this)")
            "SetOpacity" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.opacity = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "name" =@ (!? T<string>)
            "value" =@ (!? (!| T<obj>))
            "lineStyle" =@ (!? T<obj>)
            "color" =@ (!? T<string>)
            "width" =@ (!? T<float>)
            "type" =@ (!? T<string>)
            "shadowBlur" =@ (!? T<float>)
            "shadowColor" =@ (!? T<string>)
            "shadowOffsetX" =@ (!? T<float>)
            "shadowOffsetY" =@ (!? T<float>)
            "opacity" =@ (!? T<float>)
            "emphasis" =@ (!? T<obj>)
        ]
        |> ignore

    SeriesPictorialBar
        |> WithSourceName "SeriesPictorialBar"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetLegendHoverLink" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.legendHoverLink = " + tr "v" + ", $this)")
            "SetCoordinateSystem" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.coordinateSystem = " + tr "v" + ", $this)")
            "SetXAxisIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.xAxisIndex = " + tr "v" + ", $this)")
            "SetYAxisIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.yAxisIndex = " + tr "v" + ", $this)")
            "SetCursor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.cursor = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetBarWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.barWidth = " + tr "v" + ", $this)")
            "SetBarMaxWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.barMaxWidth = " + tr "v" + ", $this)")
            "SetBarMinHeight" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.barMinHeight = " + tr "v" + ", $this)")
            "SetBarGap" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.barGap = " + tr "v" + ", $this)")
            "SetBarCategoryGap" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.barCategoryGap = " + tr "v" + ", $this)")
            "SetSymbol" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbol = " + tr "v" + ", $this)")
            "SetSymbolSize" => ((!| T<obj>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolSize = " + tr "v" + ", $this)")
            "SetSymbolPosition" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolPosition = " + tr "v" + ", $this)")
            "SetSymbolOffset" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolOffset = " + tr "v" + ", $this)")
            "SetSymbolRotate" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolRotate = " + tr "v" + ", $this)")
            "SetSymbolRepeat" => (T<bool> + T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolRepeat = " + tr "v" + ", $this)")
            "SetSymbolRepeatDirection" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolRepeatDirection = " + tr "v" + ", $this)")
            "SetSymbolMargin" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolMargin = " + tr "v" + ", $this)")
            "SetSymbolClip" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolClip = " + tr "v" + ", $this)")
            "SetSymbolBoundingData" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolBoundingData = " + tr "v" + ", $this)")
            "SetSymbolPatternSize" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolPatternSize = " + tr "v" + ", $this)")
            "SetHoverAnimation" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.hoverAnimation = " + tr "v" + ", $this)")
            "SetDimensions" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.dimensions = " + tr "v" + ", $this)")
            "SetEncode" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.encode = " + tr "v" + ", $this)")
            "SetData" => ((!| (T<unit> + T<float> + T<string> + SeriesPictorialBar_DataObject)) + (!| (!| (T<unit> + T<float> + T<string> + SeriesPictorialBar_DataObject))))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetMarkPoint" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markPoint = " + tr "v" + ", $this)")
            "SetMarkLine" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markLine = " + tr "v" + ", $this)")
            "SetMarkArea" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markArea = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animation = " + tr "v" + ", $this)")
            "SetAnimationThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationThreshold = " + tr "v" + ", $this)")
            "SetAnimationDuration" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDuration = " + tr "v" + ", $this)")
            "SetAnimationEasing" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasing = " + tr "v" + ", $this)")
            "SetAnimationDurationUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDurationUpdate = " + tr "v" + ", $this)")
            "SetAnimationEasingUpdate" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasingUpdate = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "name" =@ (!? T<string>)
            "legendHoverLink" =@ (!? T<bool>)
            "coordinateSystem" =@ (!? T<string>)
            "xAxisIndex" =@ (!? T<float>)
            "yAxisIndex" =@ (!? T<float>)
            "cursor" =@ (!? T<string>)
            "label" =@ (!? T<obj>)
            "itemStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "barWidth" =@ (!? T<float>)
            "barMaxWidth" =@ (!? T<float>)
            "barMinHeight" =@ (!? T<float>)
            "barGap" =@ (!? T<string>)
            "barCategoryGap" =@ (!? T<string>)
            "symbol" =@ (!? T<string>)
            "symbolSize" =@ (!? ((!| T<obj>) + T<float>))
            "symbolPosition" =@ (!? T<string>)
            "symbolOffset" =@ (!? (!| T<obj>))
            "symbolRotate" =@ (!? T<float>)
            "symbolRepeat" =@ (!? (T<bool> + T<float> + T<string>))
            "symbolRepeatDirection" =@ (!? T<string>)
            "symbolMargin" =@ (!? (T<float> + T<string>))
            "symbolClip" =@ (!? T<bool>)
            "symbolBoundingData" =@ (!? T<float>)
            "symbolPatternSize" =@ (!? T<float>)
            "hoverAnimation" =@ (!? T<obj>)
            "dimensions" =@ (!? (!| T<obj>))
            "encode" =@ (!? T<obj>)
            "data" =@ (!? ((!| (T<unit> + T<float> + T<string> + SeriesPictorialBar_DataObject)) + (!| (!| (T<unit> + T<float> + T<string> + SeriesPictorialBar_DataObject)))))
            "markPoint" =@ (!? T<obj>)
            "markLine" =@ (!? T<obj>)
            "markArea" =@ (!? T<obj>)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "silent" =@ (!? T<bool>)
            "animation" =@ (!? T<bool>)
            "animationThreshold" =@ (!? T<float>)
            "animationDuration" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasing" =@ (!? T<string>)
            "animationDurationUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasingUpdate" =@ (!? T<obj>)
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesPictorialBar_DataObject
        |> WithSourceName "SeriesPictorialBar_DataObject"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetValue" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.value = " + tr "v" + ", $this)")
            "SetSymbol" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbol = " + tr "v" + ", $this)")
            "SetSymbolSize" => ((!| T<obj>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolSize = " + tr "v" + ", $this)")
            "SetSymbolPosition" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolPosition = " + tr "v" + ", $this)")
            "SetSymbolOffset" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolOffset = " + tr "v" + ", $this)")
            "SetSymbolRotate" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolRotate = " + tr "v" + ", $this)")
            "SetSymbolRepeat" => (T<bool> + T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolRepeat = " + tr "v" + ", $this)")
            "SetSymbolRepeatDirection" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolRepeatDirection = " + tr "v" + ", $this)")
            "SetSymbolMargin" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolMargin = " + tr "v" + ", $this)")
            "SetSymbolClip" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolClip = " + tr "v" + ", $this)")
            "SetSymbolBoundingData" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolBoundingData = " + tr "v" + ", $this)")
            "SetSymbolPatternSize" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolPatternSize = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetHoverAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.hoverAnimation = " + tr "v" + ", $this)")
            "SetAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animation = " + tr "v" + ", $this)")
            "SetAnimationThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationThreshold = " + tr "v" + ", $this)")
            "SetAnimationDuration" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDuration = " + tr "v" + ", $this)")
            "SetAnimationEasing" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasing = " + tr "v" + ", $this)")
            "SetAnimationDurationUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDurationUpdate = " + tr "v" + ", $this)")
            "SetAnimationEasingUpdate" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasingUpdate = " + tr "v" + ", $this)")
            "SetAnimationDelay" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelay = " + tr "v" + ", $this)")
            "SetAnimationDelayUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelayUpdate = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "name" =@ (!? T<string>)
            "value" =@ (!? T<float>)
            "symbol" =@ (!? T<string>)
            "symbolSize" =@ (!? ((!| T<obj>) + T<float>))
            "symbolPosition" =@ (!? T<string>)
            "symbolOffset" =@ (!? (!| T<obj>))
            "symbolRotate" =@ (!? T<float>)
            "symbolRepeat" =@ (!? (T<bool> + T<float> + T<string>))
            "symbolRepeatDirection" =@ (!? T<string>)
            "symbolMargin" =@ (!? (T<float> + T<string>))
            "symbolClip" =@ (!? T<bool>)
            "symbolBoundingData" =@ (!? T<float>)
            "symbolPatternSize" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "hoverAnimation" =@ (!? T<bool>)
            "animation" =@ (!? T<bool>)
            "animationThreshold" =@ (!? T<float>)
            "animationDuration" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasing" =@ (!? T<string>)
            "animationDurationUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasingUpdate" =@ (!? T<string>)
            "animationDelay" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationDelayUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "label" =@ (!? T<obj>)
            "itemStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesPie
        |> WithSourceName "SeriesPie"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetLegendHoverLink" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.legendHoverLink = " + tr "v" + ", $this)")
            "SetHoverAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.hoverAnimation = " + tr "v" + ", $this)")
            "SetHoverOffset" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.hoverOffset = " + tr "v" + ", $this)")
            "SetSelectedMode" => (T<bool> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.selectedMode = " + tr "v" + ", $this)")
            "SetSelectedOffset" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.selectedOffset = " + tr "v" + ", $this)")
            "SetClockwise" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.clockwise = " + tr "v" + ", $this)")
            "SetStartAngle" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.startAngle = " + tr "v" + ", $this)")
            "SetMinAngle" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.minAngle = " + tr "v" + ", $this)")
            "SetMinShowLabelAngle" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.minShowLabelAngle = " + tr "v" + ", $this)")
            "SetRoseType" => (T<bool> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.roseType = " + tr "v" + ", $this)")
            "SetAvoidLabelOverlap" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.avoidLabelOverlap = " + tr "v" + ", $this)")
            "SetStillShowZeroSum" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.stillShowZeroSum = " + tr "v" + ", $this)")
            "SetCursor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.cursor = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetLabelLine" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.labelLine = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetCenter" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.center = " + tr "v" + ", $this)")
            "SetRadius" => ((!| T<obj>) + T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.radius = " + tr "v" + ", $this)")
            "SetSeriesLayoutBy" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.seriesLayoutBy = " + tr "v" + ", $this)")
            "SetDatasetIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.datasetIndex = " + tr "v" + ", $this)")
            "SetData" => ((!| T<float>) + (!| (!| T<float>)) + (!| SeriesPie_DataObject))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetMarkPoint" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markPoint = " + tr "v" + ", $this)")
            "SetMarkLine" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markLine = " + tr "v" + ", $this)")
            "SetMarkArea" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markArea = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetAnimationType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationType = " + tr "v" + ", $this)")
            "SetAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animation = " + tr "v" + ", $this)")
            "SetAnimationThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationThreshold = " + tr "v" + ", $this)")
            "SetAnimationDuration" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDuration = " + tr "v" + ", $this)")
            "SetAnimationEasing" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasing = " + tr "v" + ", $this)")
            "SetAnimationDelay" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelay = " + tr "v" + ", $this)")
            "SetAnimationDurationUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDurationUpdate = " + tr "v" + ", $this)")
            "SetAnimationEasingUpdate" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasingUpdate = " + tr "v" + ", $this)")
            "SetAnimationDelayUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelayUpdate = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "name" =@ (!? T<string>)
            "legendHoverLink" =@ (!? T<bool>)
            "hoverAnimation" =@ (!? T<bool>)
            "hoverOffset" =@ (!? T<float>)
            "selectedMode" =@ (!? (T<bool> + T<string>))
            "selectedOffset" =@ (!? T<float>)
            "clockwise" =@ (!? T<bool>)
            "startAngle" =@ (!? T<float>)
            "minAngle" =@ (!? T<float>)
            "minShowLabelAngle" =@ (!? T<float>)
            "roseType" =@ (!? (T<bool> + T<string>))
            "avoidLabelOverlap" =@ (!? T<bool>)
            "stillShowZeroSum" =@ (!? T<bool>)
            "cursor" =@ (!? T<string>)
            "label" =@ (!? T<obj>)
            "labelLine" =@ (!? T<obj>)
            "itemStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "center" =@ (!? (!| T<obj>))
            "radius" =@ (!? ((!| T<obj>) + T<float> + T<string>))
            "seriesLayoutBy" =@ (!? T<string>)
            "datasetIndex" =@ (!? T<float>)
            "data" =@ (!? ((!| T<float>) + (!| (!| T<float>)) + (!| SeriesPie_DataObject)))
            "markPoint" =@ (!? T<obj>)
            "markLine" =@ (!? T<obj>)
            "markArea" =@ (!? T<obj>)
            "silent" =@ (!? T<bool>)
            "animationType" =@ (!? T<string>)
            "animation" =@ (!? T<bool>)
            "animationThreshold" =@ (!? T<float>)
            "animationDuration" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasing" =@ (!? T<string>)
            "animationDelay" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationDurationUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasingUpdate" =@ (!? T<string>)
            "animationDelayUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesPie_DataObject
        |> WithSourceName "SeriesPie_DataObject"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetValue" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.value = " + tr "v" + ", $this)")
            "SetSelected" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.selected = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetLabelLine" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.labelLine = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "name" =@ (!? T<string>)
            "value" =@ (!? T<float>)
            "selected" =@ (!? T<bool>)
            "label" =@ (!? T<obj>)
            "labelLine" =@ (!? T<obj>)
            "itemStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesRadar
        |> WithSourceName "SeriesRadar"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetRadarIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.radarIndex = " + tr "v" + ", $this)")
            "SetSymbol" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbol = " + tr "v" + ", $this)")
            "SetSymbolSize" => ((!| T<obj>) + (T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolSize = " + tr "v" + ", $this)")
            "SetSymbolRotate" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolRotate = " + tr "v" + ", $this)")
            "SetSymbolKeepAspect" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolKeepAspect = " + tr "v" + ", $this)")
            "SetSymbolOffset" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolOffset = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetLineStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineStyle = " + tr "v" + ", $this)")
            "SetAreaStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.areaStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetData" => (!| SeriesRadar_DataObject)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animation = " + tr "v" + ", $this)")
            "SetAnimationThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationThreshold = " + tr "v" + ", $this)")
            "SetAnimationDuration" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDuration = " + tr "v" + ", $this)")
            "SetAnimationEasing" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasing = " + tr "v" + ", $this)")
            "SetAnimationDelay" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelay = " + tr "v" + ", $this)")
            "SetAnimationDurationUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDurationUpdate = " + tr "v" + ", $this)")
            "SetAnimationEasingUpdate" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasingUpdate = " + tr "v" + ", $this)")
            "SetAnimationDelayUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelayUpdate = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "name" =@ (!? T<string>)
            "radarIndex" =@ (!? T<float>)
            "symbol" =@ (!? T<string>)
            "symbolSize" =@ (!? ((!| T<obj>) + (T<unit> ^-> T<unit>) + T<float>))
            "symbolRotate" =@ (!? T<float>)
            "symbolKeepAspect" =@ (!? T<bool>)
            "symbolOffset" =@ (!? (!| T<obj>))
            "label" =@ (!? T<obj>)
            "itemStyle" =@ (!? T<obj>)
            "lineStyle" =@ (!? T<obj>)
            "areaStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "data" =@ (!? (!| SeriesRadar_DataObject))
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "silent" =@ (!? T<bool>)
            "animation" =@ (!? T<bool>)
            "animationThreshold" =@ (!? T<float>)
            "animationDuration" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasing" =@ (!? T<string>)
            "animationDelay" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationDurationUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasingUpdate" =@ (!? T<string>)
            "animationDelayUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesRadar_DataObject
        |> WithSourceName "SeriesRadar_DataObject"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetValue" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.value = " + tr "v" + ", $this)")
            "SetSymbol" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbol = " + tr "v" + ", $this)")
            "SetSymbolSize" => ((!| T<obj>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolSize = " + tr "v" + ", $this)")
            "SetSymbolRotate" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolRotate = " + tr "v" + ", $this)")
            "SetSymbolKeepAspect" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolKeepAspect = " + tr "v" + ", $this)")
            "SetSymbolOffset" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolOffset = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetLineStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineStyle = " + tr "v" + ", $this)")
            "SetAreaStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.areaStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "name" =@ (!? T<string>)
            "value" =@ (!? (T<float> + (!| T<float>)))
            "symbol" =@ (!? T<string>)
            "symbolSize" =@ (!? ((!| T<obj>) + T<float>))
            "symbolRotate" =@ (!? T<float>)
            "symbolKeepAspect" =@ (!? T<bool>)
            "symbolOffset" =@ (!? (!| T<obj>))
            "label" =@ (!? T<obj>)
            "itemStyle" =@ (!? T<obj>)
            "lineStyle" =@ (!? T<obj>)
            "areaStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesSankey
        |> WithSourceName "SeriesSankey"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetLeft" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.left = " + tr "v" + ", $this)")
            "SetTop" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.top = " + tr "v" + ", $this)")
            "SetRight" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.right = " + tr "v" + ", $this)")
            "SetBottom" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.bottom = " + tr "v" + ", $this)")
            "SetWidth" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.width = " + tr "v" + ", $this)")
            "SetHeight" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.height = " + tr "v" + ", $this)")
            "SetNodeWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nodeWidth = " + tr "v" + ", $this)")
            "SetNodeGap" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nodeGap = " + tr "v" + ", $this)")
            "SetNodeAlign" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nodeAlign = " + tr "v" + ", $this)")
            "SetLayoutIterations" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.layoutIterations = " + tr "v" + ", $this)")
            "SetOrient" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.orient = " + tr "v" + ", $this)")
            "SetDraggable" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.draggable = " + tr "v" + ", $this)")
            "SetFocusNodeAdjacency" => (T<bool> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.focusNodeAdjacency = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetLineStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetData" => ((!| (T<float> + SeriesSankey_DataObject)) + (!| (!| (T<float> + SeriesSankey_DataObject))))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetNodes" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nodes = " + tr "v" + ", $this)")
            "SetLinks" => (!| SeriesSankey_LinkObject)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.links = " + tr "v" + ", $this)")
            "SetEdges" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.edges = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animation = " + tr "v" + ", $this)")
            "SetAnimationThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationThreshold = " + tr "v" + ", $this)")
            "SetAnimationDuration" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDuration = " + tr "v" + ", $this)")
            "SetAnimationEasing" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasing = " + tr "v" + ", $this)")
            "SetAnimationDelay" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelay = " + tr "v" + ", $this)")
            "SetAnimationDurationUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDurationUpdate = " + tr "v" + ", $this)")
            "SetAnimationEasingUpdate" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasingUpdate = " + tr "v" + ", $this)")
            "SetAnimationDelayUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelayUpdate = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "name" =@ (!? T<string>)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "left" =@ (!? (T<float> + T<string>))
            "top" =@ (!? (T<float> + T<string>))
            "right" =@ (!? (T<float> + T<string>))
            "bottom" =@ (!? (T<float> + T<string>))
            "width" =@ (!? (T<float> + T<string>))
            "height" =@ (!? (T<float> + T<string>))
            "nodeWidth" =@ (!? T<float>)
            "nodeGap" =@ (!? T<float>)
            "nodeAlign" =@ (!? T<string>)
            "layoutIterations" =@ (!? T<float>)
            "orient" =@ (!? T<string>)
            "draggable" =@ (!? T<bool>)
            "focusNodeAdjacency" =@ (!? (T<bool> + T<string>))
            "label" =@ (!? T<obj>)
            "itemStyle" =@ (!? T<obj>)
            "lineStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "data" =@ (!? ((!| (T<float> + SeriesSankey_DataObject)) + (!| (!| (T<float> + SeriesSankey_DataObject)))))
            "nodes" =@ (!? (!| T<obj>))
            "links" =@ (!? (!| SeriesSankey_LinkObject))
            "edges" =@ (!? (!| T<obj>))
            "silent" =@ (!? T<bool>)
            "animation" =@ (!? T<bool>)
            "animationThreshold" =@ (!? T<float>)
            "animationDuration" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasing" =@ (!? T<string>)
            "animationDelay" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationDurationUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasingUpdate" =@ (!? T<string>)
            "animationDelayUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesSankey_DataObject
        |> WithSourceName "SeriesSankey_DataObject"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetValue" => ((!| T<obj>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.value = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "name" =@ (!? T<string>)
            "value" =@ (!? ((!| T<obj>) + T<float>))
            "itemStyle" =@ (!? T<obj>)
            "label" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesSankey_LinkObject
        |> WithSourceName "SeriesSankey_LinkObject"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetSource" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.source = " + tr "v" + ", $this)")
            "SetTarget" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.target = " + tr "v" + ", $this)")
            "SetValue" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.value = " + tr "v" + ", $this)")
            "SetLineStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "source" =@ (!? T<string>)
            "target" =@ (!? T<string>)
            "value" =@ (!? T<float>)
            "lineStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
        ]
        |> ignore

    SeriesScatter
        |> WithSourceName "SeriesScatter"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetCoordinateSystem" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.coordinateSystem = " + tr "v" + ", $this)")
            "SetXAxisIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.xAxisIndex = " + tr "v" + ", $this)")
            "SetYAxisIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.yAxisIndex = " + tr "v" + ", $this)")
            "SetPolarIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.polarIndex = " + tr "v" + ", $this)")
            "SetGeoIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.geoIndex = " + tr "v" + ", $this)")
            "SetCalendarIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.calendarIndex = " + tr "v" + ", $this)")
            "SetHoverAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.hoverAnimation = " + tr "v" + ", $this)")
            "SetLegendHoverLink" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.legendHoverLink = " + tr "v" + ", $this)")
            "SetSymbol" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbol = " + tr "v" + ", $this)")
            "SetSymbolSize" => ((!| T<obj>) + (T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolSize = " + tr "v" + ", $this)")
            "SetSymbolRotate" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolRotate = " + tr "v" + ", $this)")
            "SetSymbolKeepAspect" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolKeepAspect = " + tr "v" + ", $this)")
            "SetSymbolOffset" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolOffset = " + tr "v" + ", $this)")
            "SetLarge" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.large = " + tr "v" + ", $this)")
            "SetLargeThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.largeThreshold = " + tr "v" + ", $this)")
            "SetCursor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.cursor = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetProgressive" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.progressive = " + tr "v" + ", $this)")
            "SetProgressiveThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.progressiveThreshold = " + tr "v" + ", $this)")
            "SetDimensions" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.dimensions = " + tr "v" + ", $this)")
            "SetEncode" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.encode = " + tr "v" + ", $this)")
            "SetSeriesLayoutBy" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.seriesLayoutBy = " + tr "v" + ", $this)")
            "SetDatasetIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.datasetIndex = " + tr "v" + ", $this)")
            "SetData" => ((!| (T<unit> + T<float> + T<string> + SeriesScatter_DataObject)) + (!| (!| (T<unit> + T<float> + T<string> + SeriesScatter_DataObject))))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetMarkPoint" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markPoint = " + tr "v" + ", $this)")
            "SetMarkLine" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markLine = " + tr "v" + ", $this)")
            "SetMarkArea" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.markArea = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animation = " + tr "v" + ", $this)")
            "SetAnimationThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationThreshold = " + tr "v" + ", $this)")
            "SetAnimationDuration" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDuration = " + tr "v" + ", $this)")
            "SetAnimationEasing" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasing = " + tr "v" + ", $this)")
            "SetAnimationDelay" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelay = " + tr "v" + ", $this)")
            "SetAnimationDurationUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDurationUpdate = " + tr "v" + ", $this)")
            "SetAnimationEasingUpdate" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasingUpdate = " + tr "v" + ", $this)")
            "SetAnimationDelayUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelayUpdate = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "name" =@ (!? T<string>)
            "coordinateSystem" =@ (!? T<string>)
            "xAxisIndex" =@ (!? T<float>)
            "yAxisIndex" =@ (!? T<float>)
            "polarIndex" =@ (!? T<float>)
            "geoIndex" =@ (!? T<float>)
            "calendarIndex" =@ (!? T<float>)
            "hoverAnimation" =@ (!? T<bool>)
            "legendHoverLink" =@ (!? T<bool>)
            "symbol" =@ (!? T<string>)
            "symbolSize" =@ (!? ((!| T<obj>) + (T<unit> ^-> T<unit>) + T<float>))
            "symbolRotate" =@ (!? T<float>)
            "symbolKeepAspect" =@ (!? T<bool>)
            "symbolOffset" =@ (!? (!| T<obj>))
            "large" =@ (!? T<bool>)
            "largeThreshold" =@ (!? T<float>)
            "cursor" =@ (!? T<string>)
            "label" =@ (!? T<obj>)
            "itemStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "progressive" =@ (!? T<float>)
            "progressiveThreshold" =@ (!? T<float>)
            "dimensions" =@ (!? (!| T<obj>))
            "encode" =@ (!? T<obj>)
            "seriesLayoutBy" =@ (!? T<string>)
            "datasetIndex" =@ (!? T<float>)
            "data" =@ (!? ((!| (T<unit> + T<float> + T<string> + SeriesScatter_DataObject)) + (!| (!| (T<unit> + T<float> + T<string> + SeriesScatter_DataObject)))))
            "markPoint" =@ (!? T<obj>)
            "markLine" =@ (!? T<obj>)
            "markArea" =@ (!? T<obj>)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "silent" =@ (!? T<bool>)
            "animation" =@ (!? T<bool>)
            "animationThreshold" =@ (!? T<float>)
            "animationDuration" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasing" =@ (!? T<string>)
            "animationDelay" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationDurationUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasingUpdate" =@ (!? T<string>)
            "animationDelayUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesScatter_DataObject
        |> WithSourceName "SeriesScatter_DataObject"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetValue" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.value = " + tr "v" + ", $this)")
            "SetSymbol" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbol = " + tr "v" + ", $this)")
            "SetSymbolSize" => ((!| T<obj>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolSize = " + tr "v" + ", $this)")
            "SetSymbolRotate" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolRotate = " + tr "v" + ", $this)")
            "SetSymbolKeepAspect" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolKeepAspect = " + tr "v" + ", $this)")
            "SetSymbolOffset" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolOffset = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "name" =@ (!? T<string>)
            "value" =@ (!? (!| T<obj>))
            "symbol" =@ (!? T<string>)
            "symbolSize" =@ (!? ((!| T<obj>) + T<float>))
            "symbolRotate" =@ (!? T<float>)
            "symbolKeepAspect" =@ (!? T<bool>)
            "symbolOffset" =@ (!? (!| T<obj>))
            "label" =@ (!? T<obj>)
            "itemStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesSunburst
        |> WithSourceName "SeriesSunburst"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetCenter" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.center = " + tr "v" + ", $this)")
            "SetRadius" => ((!| T<obj>) + T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.radius = " + tr "v" + ", $this)")
            "SetData" => (!| SeriesSunburst_DataObject)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetHighlightPolicy" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.highlightPolicy = " + tr "v" + ", $this)")
            "SetNodeClick" => (T<bool> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nodeClick = " + tr "v" + ", $this)")
            "SetSort" => ((T<unit> ^-> T<unit>) + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.sort = " + tr "v" + ", $this)")
            "SetRenderLabelForZeroData" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.renderLabelForZeroData = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetHighlight" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.highlight = " + tr "v" + ", $this)")
            "SetDownplay" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.downplay = " + tr "v" + ", $this)")
            "SetLevels" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.levels = " + tr "v" + ", $this)")
            "SetAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animation = " + tr "v" + ", $this)")
            "SetAnimationThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationThreshold = " + tr "v" + ", $this)")
            "SetAnimationDuration" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDuration = " + tr "v" + ", $this)")
            "SetAnimationEasing" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasing = " + tr "v" + ", $this)")
            "SetAnimationDelay" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelay = " + tr "v" + ", $this)")
            "SetAnimationDurationUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDurationUpdate = " + tr "v" + ", $this)")
            "SetAnimationEasingUpdate" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasingUpdate = " + tr "v" + ", $this)")
            "SetAnimationDelayUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelayUpdate = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "name" =@ (!? T<string>)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "center" =@ (!? (!| T<obj>))
            "radius" =@ (!? ((!| T<obj>) + T<float> + T<string>))
            "data" =@ (!? (!| SeriesSunburst_DataObject))
            "label" =@ (!? T<obj>)
            "itemStyle" =@ (!? T<obj>)
            "highlightPolicy" =@ (!? T<string>)
            "nodeClick" =@ (!? (T<bool> + T<string>))
            "sort" =@ (!? ((T<unit> ^-> T<unit>) + T<string>))
            "renderLabelForZeroData" =@ (!? T<bool>)
            "emphasis" =@ (!? T<obj>)
            "highlight" =@ (!? T<obj>)
            "downplay" =@ (!? T<obj>)
            "levels" =@ (!? T<obj>)
            "animation" =@ (!? T<bool>)
            "animationThreshold" =@ (!? T<float>)
            "animationDuration" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasing" =@ (!? T<string>)
            "animationDelay" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationDurationUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasingUpdate" =@ (!? T<string>)
            "animationDelayUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
        ]
        |> ignore

    SeriesSunburst_DataObject
        |> WithSourceName "SeriesSunburst_DataObject"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetValue" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.value = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetLink" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.link = " + tr "v" + ", $this)")
            "SetTarget" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.target = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetHighlight" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.highlight = " + tr "v" + ", $this)")
            "SetDownplay" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.downplay = " + tr "v" + ", $this)")
            "SetChildren" => (!| SeriesSunburst_DataObject)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.children = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "value" =@ (!? T<float>)
            "name" =@ (!? T<string>)
            "link" =@ (!? T<string>)
            "target" =@ (!? T<string>)
            "label" =@ (!? T<obj>)
            "itemStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "highlight" =@ (!? T<obj>)
            "downplay" =@ (!? T<obj>)
            "children" =@ (!? (!| SeriesSunburst_DataObject))
        ]
        |> ignore

    SeriesThemeRiver
        |> WithSourceName "SeriesThemeRiver"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetLeft" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.left = " + tr "v" + ", $this)")
            "SetTop" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.top = " + tr "v" + ", $this)")
            "SetRight" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.right = " + tr "v" + ", $this)")
            "SetBottom" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.bottom = " + tr "v" + ", $this)")
            "SetWidth" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.width = " + tr "v" + ", $this)")
            "SetHeight" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.height = " + tr "v" + ", $this)")
            "SetCoordinateSystem" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.coordinateSystem = " + tr "v" + ", $this)")
            "SetBoundaryGap" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.boundaryGap = " + tr "v" + ", $this)")
            "SetSingleAxisIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.singleAxisIndex = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetData" => ((!| (T<unit> + T<float> + T<string> + SeriesThemeRiver_DataObject)) + (!| (!| (T<unit> + T<float> + T<string> + SeriesThemeRiver_DataObject))))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "name" =@ (!? T<string>)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "left" =@ (!? (T<float> + T<string>))
            "top" =@ (!? (T<float> + T<string>))
            "right" =@ (!? (T<float> + T<string>))
            "bottom" =@ (!? (T<float> + T<string>))
            "width" =@ (!? (T<float> + T<string>))
            "height" =@ (!? (T<float> + T<string>))
            "coordinateSystem" =@ (!? T<string>)
            "boundaryGap" =@ (!? (!| T<obj>))
            "singleAxisIndex" =@ (!? T<float>)
            "label" =@ (!? T<obj>)
            "itemStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "data" =@ (!? ((!| (T<unit> + T<float> + T<string> + SeriesThemeRiver_DataObject)) + (!| (!| (T<unit> + T<float> + T<string> + SeriesThemeRiver_DataObject)))))
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesThemeRiver_DataObject
        |> WithSourceName "SeriesThemeRiver_DataObject"
        |+> Static [ ObjectConstructor ((!? T<string>)?date * (!? (T<float> + (!| T<float>)))?value * (!? T<string>)?name) ]
        |+> Instance [
            "date" =@ (!? T<string>)
            "value" =@ (!? (T<float> + (!| T<float>)))
            "name" =@ (!? T<string>)
        ]
        |> ignore

    SeriesTree
        |> WithSourceName "SeriesTree"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetLeft" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.left = " + tr "v" + ", $this)")
            "SetTop" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.top = " + tr "v" + ", $this)")
            "SetRight" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.right = " + tr "v" + ", $this)")
            "SetBottom" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.bottom = " + tr "v" + ", $this)")
            "SetWidth" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.width = " + tr "v" + ", $this)")
            "SetHeight" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.height = " + tr "v" + ", $this)")
            "SetLayout" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.layout = " + tr "v" + ", $this)")
            "SetOrient" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.orient = " + tr "v" + ", $this)")
            "SetSymbol" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbol = " + tr "v" + ", $this)")
            "SetSymbolSize" => ((!| T<obj>) + (T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolSize = " + tr "v" + ", $this)")
            "SetSymbolRotate" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolRotate = " + tr "v" + ", $this)")
            "SetSymbolKeepAspect" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolKeepAspect = " + tr "v" + ", $this)")
            "SetSymbolOffset" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolOffset = " + tr "v" + ", $this)")
            "SetRoam" => (T<bool> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.roam = " + tr "v" + ", $this)")
            "SetExpandAndCollapse" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.expandAndCollapse = " + tr "v" + ", $this)")
            "SetInitialTreeDepth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.initialTreeDepth = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetLineStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetLeaves" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.leaves = " + tr "v" + ", $this)")
            "SetData" => (!| SeriesTree_DataObject)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "name" =@ (!? T<string>)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "left" =@ (!? (T<float> + T<string>))
            "top" =@ (!? (T<float> + T<string>))
            "right" =@ (!? (T<float> + T<string>))
            "bottom" =@ (!? (T<float> + T<string>))
            "width" =@ (!? (T<float> + T<string>))
            "height" =@ (!? (T<float> + T<string>))
            "layout" =@ (!? T<string>)
            "orient" =@ (!? T<string>)
            "symbol" =@ (!? T<string>)
            "symbolSize" =@ (!? ((!| T<obj>) + (T<unit> ^-> T<unit>) + T<float>))
            "symbolRotate" =@ (!? T<float>)
            "symbolKeepAspect" =@ (!? T<bool>)
            "symbolOffset" =@ (!? (!| T<obj>))
            "roam" =@ (!? (T<bool> + T<string>))
            "expandAndCollapse" =@ (!? T<bool>)
            "initialTreeDepth" =@ (!? T<float>)
            "itemStyle" =@ (!? T<obj>)
            "label" =@ (!? T<obj>)
            "lineStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "leaves" =@ (!? T<obj>)
            "data" =@ (!? (!| SeriesTree_DataObject))
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesTree_DataObject
        |> WithSourceName "SeriesTree_DataObject"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetChildren" => (!| SeriesTree_DataObject)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.children = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetValue" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.value = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
            "SetAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animation = " + tr "v" + ", $this)")
            "SetAnimationThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationThreshold = " + tr "v" + ", $this)")
            "SetAnimationDuration" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDuration = " + tr "v" + ", $this)")
            "SetAnimationEasing" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasing = " + tr "v" + ", $this)")
            "SetAnimationDelay" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelay = " + tr "v" + ", $this)")
            "SetAnimationDurationUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDurationUpdate = " + tr "v" + ", $this)")
            "SetAnimationEasingUpdate" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasingUpdate = " + tr "v" + ", $this)")
            "SetAnimationDelayUpdate" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelayUpdate = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "children" =@ (!? (!| SeriesTree_DataObject))
            "name" =@ (!? T<string>)
            "value" =@ (!? T<float>)
            "itemStyle" =@ (!? T<obj>)
            "label" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "tooltip" =@ (!? BaseTooltip)
            "animation" =@ (!? T<bool>)
            "animationThreshold" =@ (!? T<float>)
            "animationDuration" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasing" =@ (!? T<string>)
            "animationDelay" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationDurationUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasingUpdate" =@ (!? T<string>)
            "animationDelayUpdate" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
        ]
        |> ignore

    SeriesTreemap
        |> WithSourceName "SeriesTreemap"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetLeft" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.left = " + tr "v" + ", $this)")
            "SetTop" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.top = " + tr "v" + ", $this)")
            "SetRight" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.right = " + tr "v" + ", $this)")
            "SetBottom" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.bottom = " + tr "v" + ", $this)")
            "SetWidth" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.width = " + tr "v" + ", $this)")
            "SetHeight" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.height = " + tr "v" + ", $this)")
            "SetSquareRatio" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.squareRatio = " + tr "v" + ", $this)")
            "SetLeafDepth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.leafDepth = " + tr "v" + ", $this)")
            "SetDrillDownIcon" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.drillDownIcon = " + tr "v" + ", $this)")
            "SetRoam" => (T<bool> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.roam = " + tr "v" + ", $this)")
            "SetNodeClick" => (T<bool> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nodeClick = " + tr "v" + ", $this)")
            "SetZoomToNodeRatio" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zoomToNodeRatio = " + tr "v" + ", $this)")
            "SetLevels" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.levels = " + tr "v" + ", $this)")
            "SetSilent" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetVisualDimension" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.visualDimension = " + tr "v" + ", $this)")
            "SetVisualMin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.visualMin = " + tr "v" + ", $this)")
            "SetVisualMax" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.visualMax = " + tr "v" + ", $this)")
            "SetColorAlpha" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.colorAlpha = " + tr "v" + ", $this)")
            "SetColorSaturation" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.colorSaturation = " + tr "v" + ", $this)")
            "SetColorMappingBy" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.colorMappingBy = " + tr "v" + ", $this)")
            "SetVisibleMin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.visibleMin = " + tr "v" + ", $this)")
            "SetChildrenVisibleMin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.childrenVisibleMin = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetUpperLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.upperLabel = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
            "SetBreadcrumb" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.breadcrumb = " + tr "v" + ", $this)")
            "SetData" => (!| SeriesTreemap_DataObject)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetAnimationDuration" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDuration = " + tr "v" + ", $this)")
            "SetAnimationEasing" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasing = " + tr "v" + ", $this)")
            "SetAnimationDelay" => ((T<unit> ^-> T<unit>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelay = " + tr "v" + ", $this)")
            "SetTooltip" => BaseTooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "name" =@ (!? T<string>)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "left" =@ (!? (T<float> + T<string>))
            "top" =@ (!? (T<float> + T<string>))
            "right" =@ (!? (T<float> + T<string>))
            "bottom" =@ (!? (T<float> + T<string>))
            "width" =@ (!? (T<float> + T<string>))
            "height" =@ (!? (T<float> + T<string>))
            "squareRatio" =@ (!? T<float>)
            "leafDepth" =@ (!? T<float>)
            "drillDownIcon" =@ (!? T<string>)
            "roam" =@ (!? (T<bool> + T<string>))
            "nodeClick" =@ (!? (T<bool> + T<string>))
            "zoomToNodeRatio" =@ (!? T<float>)
            "levels" =@ (!? T<obj>)
            "silent" =@ (!? T<obj>)
            "visualDimension" =@ (!? T<float>)
            "visualMin" =@ (!? T<float>)
            "visualMax" =@ (!? T<float>)
            "colorAlpha" =@ (!? (!| T<obj>))
            "colorSaturation" =@ (!? T<float>)
            "colorMappingBy" =@ (!? T<string>)
            "visibleMin" =@ (!? T<float>)
            "childrenVisibleMin" =@ (!? T<float>)
            "label" =@ (!? T<obj>)
            "upperLabel" =@ (!? T<obj>)
            "itemStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
            "breadcrumb" =@ (!? T<obj>)
            "data" =@ (!? (!| SeriesTreemap_DataObject))
            "animationDuration" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "animationEasing" =@ (!? T<string>)
            "animationDelay" =@ (!? ((T<unit> ^-> T<unit>) + T<float>))
            "tooltip" =@ (!? BaseTooltip)
        ]
        |> ignore

    SeriesTreemap_DataObject
        |> WithSourceName "SeriesTreemap_DataObject"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetChildren" => (!| SeriesTreemap_DataObject)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.children = " + tr "v" + ", $this)")
            "SetValue" => ((!| T<obj>) + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.value = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetVisualDimension" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.visualDimension = " + tr "v" + ", $this)")
            "SetVisualMin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.visualMin = " + tr "v" + ", $this)")
            "SetVisualMax" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.visualMax = " + tr "v" + ", $this)")
            "SetColor" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.color = " + tr "v" + ", $this)")
            "SetColorAlpha" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.colorAlpha = " + tr "v" + ", $this)")
            "SetColorSaturation" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.colorSaturation = " + tr "v" + ", $this)")
            "SetColorMappingBy" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.colorMappingBy = " + tr "v" + ", $this)")
            "SetVisibleMin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.visibleMin = " + tr "v" + ", $this)")
            "SetChildrenVisibleMin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.childrenVisibleMin = " + tr "v" + ", $this)")
            "SetLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetUpperLabel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.upperLabel = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetEmphasis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.emphasis = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "children" =@ (!? (!| SeriesTreemap_DataObject))
            "value" =@ (!? ((!| T<obj>) + T<float>))
            "id" =@ (!? T<string>)
            "name" =@ (!? T<string>)
            "visualDimension" =@ (!? T<float>)
            "visualMin" =@ (!? T<float>)
            "visualMax" =@ (!? T<float>)
            "color" =@ (!? (!| T<obj>))
            "colorAlpha" =@ (!? (!| T<obj>))
            "colorSaturation" =@ (!? T<float>)
            "colorMappingBy" =@ (!? T<string>)
            "visibleMin" =@ (!? T<float>)
            "childrenVisibleMin" =@ (!? T<float>)
            "label" =@ (!? T<obj>)
            "upperLabel" =@ (!? T<obj>)
            "itemStyle" =@ (!? T<obj>)
            "emphasis" =@ (!? T<obj>)
        ]
        |> ignore

    AxisPointer
        |> WithSourceName "AxisPointer"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetLink" => (!| T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.link = " + tr "v" + ", $this)")
            "SetTriggerOn" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.triggerOn = " + tr "v" + ", $this)")
            "SetShow" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.show = " + tr "v" + ", $this)")
            "SetType" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetSnap" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.snap = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetLabel" => CartesianAxis_PointerLabel?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetLineStyle" => LineStyle?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineStyle = " + tr "v" + ", $this)")
            "SetShadowStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowStyle = " + tr "v" + ", $this)")
            "SetTriggerTooltip" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.triggerTooltip = " + tr "v" + ", $this)")
            "SetValue" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.value = " + tr "v" + ", $this)")
            "SetStatus" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.status = " + tr "v" + ", $this)")
            "SetHandle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.handle = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "id" =@ (!? T<string>)
            "link" =@ (!? (!| T<obj>))
            "triggerOn" =@ (!? (T<string>))
            "show" =@ (!? T<bool>)
            "type" =@ (!? (T<string>))
            "snap" =@ (!? T<bool>)
            "z" =@ (!? T<float>)
            "label" =@ (!? CartesianAxis_PointerLabel)
            "lineStyle" =@ (!? LineStyle)
            "shadowStyle" =@ (!? T<obj>)
            "triggerTooltip" =@ (!? T<bool>)
            "value" =@ (!? T<float>)
            "status" =@ (!? T<bool>)
            "handle" =@ (!? T<obj>)
        ]
        |> ignore

    Calendar
        |> WithSourceName "Calendar"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetLeft" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.left = " + tr "v" + ", $this)")
            "SetTop" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.top = " + tr "v" + ", $this)")
            "SetRight" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.right = " + tr "v" + ", $this)")
            "SetBottom" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.bottom = " + tr "v" + ", $this)")
            "SetWidth" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.width = " + tr "v" + ", $this)")
            "SetHeight" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.height = " + tr "v" + ", $this)")
            "SetRange" => (T<float> + T<string> + (!| T<float>) + (!| T<string>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.range = " + tr "v" + ", $this)")
            "SetCellSize" => (T<float> + T<string> + (!| (T<string> + T<float>)))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.cellSize = " + tr "v" + ", $this)")
            "SetOrient" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.orient = " + tr "v" + ", $this)")
            "SetSplitLine" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.splitLine = " + tr "v" + ", $this)")
            "SetItemStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemStyle = " + tr "v" + ", $this)")
            "SetDayLabel" => Calendar_DayLabel?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.dayLabel = " + tr "v" + ", $this)")
            "SetMonthLabel" => Calendar_MonthLabel?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.monthLabel = " + tr "v" + ", $this)")
            "SetYearLabel" => Calendar_YearLabel?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.yearLabel = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "id" =@ (!? T<string>)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "left" =@ (!? (T<float> + T<string>))
            "top" =@ (!? (T<float> + T<string>))
            "right" =@ (!? (T<float> + T<string>))
            "bottom" =@ (!? (T<float> + T<string>))
            "width" =@ (!? (T<float> + T<string>))
            "height" =@ (!? (T<float> + T<string>))
            "range" =@ (!? (T<float> + T<string> + (!| T<float>) + (!| T<string>)))
            "cellSize" =@ (!? (T<float> + T<string> + (!| (T<string> + T<float>))))
            "orient" =@ (!? (T<string>))
            "splitLine" =@ (!? T<obj>)
            "itemStyle" =@ (!? T<obj>)
            "dayLabel" =@ (!? Calendar_DayLabel)
            "monthLabel" =@ (!? Calendar_MonthLabel)
            "yearLabel" =@ (!? Calendar_YearLabel)
            "silent" =@ (!? T<bool>)
        ]
        |> ignore

    Calendar_Label
        |> WithSourceName "Calendar_Label"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetShow" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.show = " + tr "v" + ", $this)")
            "SetMargin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.margin = " + tr "v" + ", $this)")
            "SetNameMap" => (T<string> + T<float> + (!| (T<string> + T<float>)))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nameMap = " + tr "v" + ", $this)")
            "SetRich" => RichStyle?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.rich = " + tr "v" + ", $this)")
            "SetAlign" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.align = " + tr "v" + ", $this)")
            "SetVerticalAlign" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.verticalAlign = " + tr "v" + ", $this)")
            "SetBackgroundColor" => (T<string> + T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.backgroundColor = " + tr "v" + ", $this)")
            "SetBorderColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderColor = " + tr "v" + ", $this)")
            "SetBorderWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderWidth = " + tr "v" + ", $this)")
            "SetBorderRadius" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderRadius = " + tr "v" + ", $this)")
            "SetPadding" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.padding = " + tr "v" + ", $this)")
            "SetShadowColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowColor = " + tr "v" + ", $this)")
            "SetShadowBlur" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowBlur = " + tr "v" + ", $this)")
            "SetShadowOffsetX" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowOffsetX = " + tr "v" + ", $this)")
            "SetShadowOffsetY" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowOffsetY = " + tr "v" + ", $this)")
            "SetColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.color = " + tr "v" + ", $this)")
            "SetFontStyle" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontStyle = " + tr "v" + ", $this)")
            "SetFontWeight" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontWeight = " + tr "v" + ", $this)")
            "SetFontFamily" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontFamily = " + tr "v" + ", $this)")
            "SetFontSize" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontSize = " + tr "v" + ", $this)")
            "SetLineHeight" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineHeight = " + tr "v" + ", $this)")
            "SetWidth" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.width = " + tr "v" + ", $this)")
            "SetHeight" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.height = " + tr "v" + ", $this)")
            "SetTextBorderColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textBorderColor = " + tr "v" + ", $this)")
            "SetTextBorderWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textBorderWidth = " + tr "v" + ", $this)")
            "SetTextShadowColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowColor = " + tr "v" + ", $this)")
            "SetTextShadowBlur" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowBlur = " + tr "v" + ", $this)")
            "SetTextShadowOffsetX" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowOffsetX = " + tr "v" + ", $this)")
            "SetTextShadowOffsetY" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowOffsetY = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "show" =@ (!? T<bool>)
            "margin" =@ (!? T<float>)
            "nameMap" =@ (!? (T<string> + T<float> + (!| (T<string> + T<float>))))
            "rich" =@ (!? RichStyle)
            "align" =@ (!? T<string>)
            "verticalAlign" =@ (!? T<string>)
            "backgroundColor" =@ (!? (T<string> + T<obj>))
            "borderColor" =@ (!? T<string>)
            "borderWidth" =@ (!? T<float>)
            "borderRadius" =@ (!? T<float>)
            "padding" =@ (!? (T<float> + (!| T<float>)))
            "shadowColor" =@ (!? T<string>)
            "shadowBlur" =@ (!? T<float>)
            "shadowOffsetX" =@ (!? T<float>)
            "shadowOffsetY" =@ (!? T<float>)
            "color" =@ (!? T<string>)
            "fontStyle" =@ (!? (T<string>))
            "fontWeight" =@ (!? (T<string> + T<float>))
            "fontFamily" =@ (!? T<string>)
            "fontSize" =@ (!? T<float>)
            "lineHeight" =@ (!? T<float>)
            "width" =@ (!? (T<float> + T<string>))
            "height" =@ (!? (T<float> + T<string>))
            "textBorderColor" =@ (!? T<string>)
            "textBorderWidth" =@ (!? T<float>)
            "textShadowColor" =@ (!? T<string>)
            "textShadowBlur" =@ (!? T<float>)
            "textShadowOffsetX" =@ (!? T<float>)
            "textShadowOffsetY" =@ (!? T<float>)
        ]
        |> ignore

    Calendar_DayLabel
        |> WithSourceName "Calendar_DayLabel"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetFirstDay" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.firstDay = " + tr "v" + ", $this)")
            "SetPosition" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.position = " + tr "v" + ", $this)")
            "SetShow" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.show = " + tr "v" + ", $this)")
            "SetMargin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.margin = " + tr "v" + ", $this)")
            "SetNameMap" => (T<string> + T<float> + (!| (T<string> + T<float>)))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nameMap = " + tr "v" + ", $this)")
            "SetRich" => RichStyle?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.rich = " + tr "v" + ", $this)")
            "SetAlign" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.align = " + tr "v" + ", $this)")
            "SetVerticalAlign" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.verticalAlign = " + tr "v" + ", $this)")
            "SetBackgroundColor" => (T<string> + T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.backgroundColor = " + tr "v" + ", $this)")
            "SetBorderColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderColor = " + tr "v" + ", $this)")
            "SetBorderWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderWidth = " + tr "v" + ", $this)")
            "SetBorderRadius" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderRadius = " + tr "v" + ", $this)")
            "SetPadding" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.padding = " + tr "v" + ", $this)")
            "SetShadowColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowColor = " + tr "v" + ", $this)")
            "SetShadowBlur" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowBlur = " + tr "v" + ", $this)")
            "SetShadowOffsetX" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowOffsetX = " + tr "v" + ", $this)")
            "SetShadowOffsetY" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowOffsetY = " + tr "v" + ", $this)")
            "SetColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.color = " + tr "v" + ", $this)")
            "SetFontStyle" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontStyle = " + tr "v" + ", $this)")
            "SetFontWeight" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontWeight = " + tr "v" + ", $this)")
            "SetFontFamily" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontFamily = " + tr "v" + ", $this)")
            "SetFontSize" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontSize = " + tr "v" + ", $this)")
            "SetLineHeight" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineHeight = " + tr "v" + ", $this)")
            "SetWidth" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.width = " + tr "v" + ", $this)")
            "SetHeight" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.height = " + tr "v" + ", $this)")
            "SetTextBorderColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textBorderColor = " + tr "v" + ", $this)")
            "SetTextBorderWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textBorderWidth = " + tr "v" + ", $this)")
            "SetTextShadowColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowColor = " + tr "v" + ", $this)")
            "SetTextShadowBlur" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowBlur = " + tr "v" + ", $this)")
            "SetTextShadowOffsetX" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowOffsetX = " + tr "v" + ", $this)")
            "SetTextShadowOffsetY" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowOffsetY = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "firstDay" =@ (!? T<float>)
            "position" =@ (!? (T<string>))
            "show" =@ (!? T<bool>)
            "margin" =@ (!? T<float>)
            "nameMap" =@ (!? (T<string> + T<float> + (!| (T<string> + T<float>))))
            "rich" =@ (!? RichStyle)
            "align" =@ (!? T<string>)
            "verticalAlign" =@ (!? T<string>)
            "backgroundColor" =@ (!? (T<string> + T<obj>))
            "borderColor" =@ (!? T<string>)
            "borderWidth" =@ (!? T<float>)
            "borderRadius" =@ (!? T<float>)
            "padding" =@ (!? (T<float> + (!| T<float>)))
            "shadowColor" =@ (!? T<string>)
            "shadowBlur" =@ (!? T<float>)
            "shadowOffsetX" =@ (!? T<float>)
            "shadowOffsetY" =@ (!? T<float>)
            "color" =@ (!? T<string>)
            "fontStyle" =@ (!? (T<string>))
            "fontWeight" =@ (!? (T<string> + T<float>))
            "fontFamily" =@ (!? T<string>)
            "fontSize" =@ (!? T<float>)
            "lineHeight" =@ (!? T<float>)
            "width" =@ (!? (T<float> + T<string>))
            "height" =@ (!? (T<float> + T<string>))
            "textBorderColor" =@ (!? T<string>)
            "textBorderWidth" =@ (!? T<float>)
            "textShadowColor" =@ (!? T<string>)
            "textShadowBlur" =@ (!? T<float>)
            "textShadowOffsetX" =@ (!? T<float>)
            "textShadowOffsetY" =@ (!? T<float>)
        ]
        |> ignore

    Calendar_MonthLabel
        |> WithSourceName "Calendar_MonthLabel"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetFormatter" => (T<string> + (Calendar_MonthLabelFormatterParams?``params`` ^-> T<string>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.formatter = " + tr "v" + ", $this)")
            "SetPosition" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.position = " + tr "v" + ", $this)")
            "SetShow" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.show = " + tr "v" + ", $this)")
            "SetMargin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.margin = " + tr "v" + ", $this)")
            "SetNameMap" => (T<string> + T<float> + (!| (T<string> + T<float>)))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nameMap = " + tr "v" + ", $this)")
            "SetRich" => RichStyle?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.rich = " + tr "v" + ", $this)")
            "SetAlign" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.align = " + tr "v" + ", $this)")
            "SetVerticalAlign" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.verticalAlign = " + tr "v" + ", $this)")
            "SetBackgroundColor" => (T<string> + T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.backgroundColor = " + tr "v" + ", $this)")
            "SetBorderColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderColor = " + tr "v" + ", $this)")
            "SetBorderWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderWidth = " + tr "v" + ", $this)")
            "SetBorderRadius" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderRadius = " + tr "v" + ", $this)")
            "SetPadding" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.padding = " + tr "v" + ", $this)")
            "SetShadowColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowColor = " + tr "v" + ", $this)")
            "SetShadowBlur" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowBlur = " + tr "v" + ", $this)")
            "SetShadowOffsetX" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowOffsetX = " + tr "v" + ", $this)")
            "SetShadowOffsetY" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowOffsetY = " + tr "v" + ", $this)")
            "SetColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.color = " + tr "v" + ", $this)")
            "SetFontStyle" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontStyle = " + tr "v" + ", $this)")
            "SetFontWeight" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontWeight = " + tr "v" + ", $this)")
            "SetFontFamily" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontFamily = " + tr "v" + ", $this)")
            "SetFontSize" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontSize = " + tr "v" + ", $this)")
            "SetLineHeight" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineHeight = " + tr "v" + ", $this)")
            "SetWidth" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.width = " + tr "v" + ", $this)")
            "SetHeight" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.height = " + tr "v" + ", $this)")
            "SetTextBorderColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textBorderColor = " + tr "v" + ", $this)")
            "SetTextBorderWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textBorderWidth = " + tr "v" + ", $this)")
            "SetTextShadowColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowColor = " + tr "v" + ", $this)")
            "SetTextShadowBlur" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowBlur = " + tr "v" + ", $this)")
            "SetTextShadowOffsetX" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowOffsetX = " + tr "v" + ", $this)")
            "SetTextShadowOffsetY" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowOffsetY = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "formatter" =@ (!? (T<string> + (Calendar_MonthLabelFormatterParams?``params`` ^-> T<string>)))
            "position" =@ (!? (T<string>))
            "show" =@ (!? T<bool>)
            "margin" =@ (!? T<float>)
            "nameMap" =@ (!? (T<string> + T<float> + (!| (T<string> + T<float>))))
            "rich" =@ (!? RichStyle)
            "align" =@ (!? T<string>)
            "verticalAlign" =@ (!? T<string>)
            "backgroundColor" =@ (!? (T<string> + T<obj>))
            "borderColor" =@ (!? T<string>)
            "borderWidth" =@ (!? T<float>)
            "borderRadius" =@ (!? T<float>)
            "padding" =@ (!? (T<float> + (!| T<float>)))
            "shadowColor" =@ (!? T<string>)
            "shadowBlur" =@ (!? T<float>)
            "shadowOffsetX" =@ (!? T<float>)
            "shadowOffsetY" =@ (!? T<float>)
            "color" =@ (!? T<string>)
            "fontStyle" =@ (!? (T<string>))
            "fontWeight" =@ (!? (T<string> + T<float>))
            "fontFamily" =@ (!? T<string>)
            "fontSize" =@ (!? T<float>)
            "lineHeight" =@ (!? T<float>)
            "width" =@ (!? (T<float> + T<string>))
            "height" =@ (!? (T<float> + T<string>))
            "textBorderColor" =@ (!? T<string>)
            "textBorderWidth" =@ (!? T<float>)
            "textShadowColor" =@ (!? T<string>)
            "textShadowBlur" =@ (!? T<float>)
            "textShadowOffsetX" =@ (!? T<float>)
            "textShadowOffsetY" =@ (!? T<float>)
        ]
        |> ignore

    Calendar_MonthLabelFormatterParams
        |> WithSourceName "Calendar_MonthLabelFormatterParams"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetNameMap" => (T<string> + T<float> + (!| (T<string> + T<float>)))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nameMap = " + tr "v" + ", $this)")
            "SetYyyy" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.yyyy = " + tr "v" + ", $this)")
            "SetYy" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.yy = " + tr "v" + ", $this)")
            "SetMM" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.MM = " + tr "v" + ", $this)")
            "SetM" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.M = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "nameMap" =@ (!? (T<string> + T<float> + (!| (T<string> + T<float>))))
            "yyyy" =@ (!? T<float>)
            "yy" =@ (!? T<float>)
            "MM" =@ (!? T<float>)
            "M" =@ (!? T<float>)
        ]
        |> ignore

    Calendar_YearLabel
        |> WithSourceName "Calendar_YearLabel"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetFormatter" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.formatter = " + tr "v" + ", $this)")
            "SetPosition" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.position = " + tr "v" + ", $this)")
            "SetShow" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.show = " + tr "v" + ", $this)")
            "SetMargin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.margin = " + tr "v" + ", $this)")
            "SetNameMap" => (T<string> + T<float> + (!| (T<string> + T<float>)))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nameMap = " + tr "v" + ", $this)")
            "SetRich" => RichStyle?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.rich = " + tr "v" + ", $this)")
            "SetAlign" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.align = " + tr "v" + ", $this)")
            "SetVerticalAlign" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.verticalAlign = " + tr "v" + ", $this)")
            "SetBackgroundColor" => (T<string> + T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.backgroundColor = " + tr "v" + ", $this)")
            "SetBorderColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderColor = " + tr "v" + ", $this)")
            "SetBorderWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderWidth = " + tr "v" + ", $this)")
            "SetBorderRadius" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderRadius = " + tr "v" + ", $this)")
            "SetPadding" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.padding = " + tr "v" + ", $this)")
            "SetShadowColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowColor = " + tr "v" + ", $this)")
            "SetShadowBlur" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowBlur = " + tr "v" + ", $this)")
            "SetShadowOffsetX" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowOffsetX = " + tr "v" + ", $this)")
            "SetShadowOffsetY" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowOffsetY = " + tr "v" + ", $this)")
            "SetColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.color = " + tr "v" + ", $this)")
            "SetFontStyle" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontStyle = " + tr "v" + ", $this)")
            "SetFontWeight" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontWeight = " + tr "v" + ", $this)")
            "SetFontFamily" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontFamily = " + tr "v" + ", $this)")
            "SetFontSize" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontSize = " + tr "v" + ", $this)")
            "SetLineHeight" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineHeight = " + tr "v" + ", $this)")
            "SetWidth" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.width = " + tr "v" + ", $this)")
            "SetHeight" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.height = " + tr "v" + ", $this)")
            "SetTextBorderColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textBorderColor = " + tr "v" + ", $this)")
            "SetTextBorderWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textBorderWidth = " + tr "v" + ", $this)")
            "SetTextShadowColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowColor = " + tr "v" + ", $this)")
            "SetTextShadowBlur" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowBlur = " + tr "v" + ", $this)")
            "SetTextShadowOffsetX" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowOffsetX = " + tr "v" + ", $this)")
            "SetTextShadowOffsetY" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowOffsetY = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "formatter" =@ (!? T<string>)
            "position" =@ (!? (T<string>))
            "show" =@ (!? T<bool>)
            "margin" =@ (!? T<float>)
            "nameMap" =@ (!? (T<string> + T<float> + (!| (T<string> + T<float>))))
            "rich" =@ (!? RichStyle)
            "align" =@ (!? T<string>)
            "verticalAlign" =@ (!? T<string>)
            "backgroundColor" =@ (!? (T<string> + T<obj>))
            "borderColor" =@ (!? T<string>)
            "borderWidth" =@ (!? T<float>)
            "borderRadius" =@ (!? T<float>)
            "padding" =@ (!? (T<float> + (!| T<float>)))
            "shadowColor" =@ (!? T<string>)
            "shadowBlur" =@ (!? T<float>)
            "shadowOffsetX" =@ (!? T<float>)
            "shadowOffsetY" =@ (!? T<float>)
            "color" =@ (!? T<string>)
            "fontStyle" =@ (!? (T<string>))
            "fontWeight" =@ (!? (T<string> + T<float>))
            "fontFamily" =@ (!? T<string>)
            "fontSize" =@ (!? T<float>)
            "lineHeight" =@ (!? T<float>)
            "width" =@ (!? (T<float> + T<string>))
            "height" =@ (!? (T<float> + T<string>))
            "textBorderColor" =@ (!? T<string>)
            "textBorderWidth" =@ (!? T<float>)
            "textShadowColor" =@ (!? T<string>)
            "textShadowBlur" =@ (!? T<float>)
            "textShadowOffsetX" =@ (!? T<float>)
            "textShadowOffsetY" =@ (!? T<float>)
        ]
        |> ignore

    Calendar_YearLabelFormatterParams
        |> WithSourceName "Calendar_YearLabelFormatterParams"
        |+> Static [ ObjectConstructor ((!? (T<string> + T<float> + (!| (T<string> + T<float>))))?nameMap * (!? T<float>)?start * (!? T<float>)?``end``) ]
        |+> Instance [
            "nameMap" =@ (!? (T<string> + T<float> + (!| (T<string> + T<float>))))
            "start" =@ (!? T<float>)
            "end" =@ (!? T<float>)
        ]
        |> ignore

    Dataset
        |> WithSourceName "Dataset"
        |+> Static [ ObjectConstructor ((!? T<string>)?id * (!? ((!| T<obj>) + T<obj>))?source * (!? ((!| T<string>) + (!| Dataset_DimensionObject)))?dimensions) ]
        |+> Instance [
            "id" =@ (!? T<string>)
            "source" =@ (!? ((!| T<obj>) + T<obj>))
            "dimensions" =@ (!? ((!| T<string>) + (!| Dataset_DimensionObject)))
        ]
        |> ignore

    Dataset_DimensionObject
        |> WithSourceName "Dataset_DimensionObject"
        |+> Static [ ObjectConstructor ((!? T<string>)?name * (!? (T<string>))?``type`` * (!? T<string>)?displayName) ]
        |+> Instance [
            "name" =@ (!? T<string>)
            "type" =@ (!? (T<string>))
            "displayName" =@ (!? T<string>)
        ]
        |> ignore

    DataZoom_Inside
        |> WithSourceName "DataZoom_Inside"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetDisabled" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.disabled = " + tr "v" + ", $this)")
            "SetXAxisIndex" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.xAxisIndex = " + tr "v" + ", $this)")
            "SetYAxisIndex" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.yAxisIndex = " + tr "v" + ", $this)")
            "SetRadiusAxisIndex" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.radiusAxisIndex = " + tr "v" + ", $this)")
            "SetAngleAxisIndex" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.angleAxisIndex = " + tr "v" + ", $this)")
            "SetSingleAxisIndex" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.singleAxisIndex = " + tr "v" + ", $this)")
            "SetFilterMode" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.filterMode = " + tr "v" + ", $this)")
            "SetStart" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.start = " + tr "v" + ", $this)")
            "SetEnd" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.end = " + tr "v" + ", $this)")
            "SetStartValue" => (T<float> + T<string> + T<Date>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.startValue = " + tr "v" + ", $this)")
            "SetEndValue" => (T<float> + T<string> + T<Date>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.endValue = " + tr "v" + ", $this)")
            "SetMinSpan" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.minSpan = " + tr "v" + ", $this)")
            "SetMaxSpan" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.maxSpan = " + tr "v" + ", $this)")
            "SetMinValueSpan" => (T<float> + T<string> + T<Date>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.minValueSpan = " + tr "v" + ", $this)")
            "SetMaxValueSpan" => (T<float> + T<string> + T<Date>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.maxValueSpan = " + tr "v" + ", $this)")
            "SetOrient" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.orient = " + tr "v" + ", $this)")
            "SetZoomLock" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zoomLock = " + tr "v" + ", $this)")
            "SetThrottle" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.throttle = " + tr "v" + ", $this)")
            "SetRangeMode" => (!| T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.rangeMode = " + tr "v" + ", $this)")
            "SetZoomOnMouseWheel" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zoomOnMouseWheel = " + tr "v" + ", $this)")
            "SetMoveOnMouseMove" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.moveOnMouseMove = " + tr "v" + ", $this)")
            "SetMoveOnMouseWheel" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.moveOnMouseWheel = " + tr "v" + ", $this)")
            "SetPreventDefaultMouseMove" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.preventDefaultMouseMove = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "disabled" =@ (!? T<bool>)
            "xAxisIndex" =@ (!? (T<float> + (!| T<float>)))
            "yAxisIndex" =@ (!? (T<float> + (!| T<float>)))
            "radiusAxisIndex" =@ (!? (T<float> + (!| T<float>)))
            "angleAxisIndex" =@ (!? (T<float> + (!| T<float>)))
            "singleAxisIndex" =@ (!? (T<float> + (!| T<float>)))
            "filterMode" =@ (!? (T<string>))
            "start" =@ (!? T<float>)
            "end" =@ (!? T<float>)
            "startValue" =@ (!? (T<float> + T<string> + T<Date>))
            "endValue" =@ (!? (T<float> + T<string> + T<Date>))
            "minSpan" =@ (!? T<float>)
            "maxSpan" =@ (!? T<float>)
            "minValueSpan" =@ (!? (T<float> + T<string> + T<Date>))
            "maxValueSpan" =@ (!? (T<float> + T<string> + T<Date>))
            "orient" =@ (!? T<string>)
            "zoomLock" =@ (!? T<bool>)
            "throttle" =@ (!? T<float>)
            "rangeMode" =@ (!? (!| T<string>))
            "zoomOnMouseWheel" =@ (!? T<bool>)
            "moveOnMouseMove" =@ (!? T<bool>)
            "moveOnMouseWheel" =@ (!? T<bool>)
            "preventDefaultMouseMove" =@ (!? T<bool>)
        ]
        |> ignore

    DataZoom_Slider
        |> WithSourceName "DataZoom_Slider"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetShow" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.show = " + tr "v" + ", $this)")
            "SetBackgroundColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.backgroundColor = " + tr "v" + ", $this)")
            "SetDataBackground" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.dataBackground = " + tr "v" + ", $this)")
            "SetFillerColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fillerColor = " + tr "v" + ", $this)")
            "SetBorderColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderColor = " + tr "v" + ", $this)")
            "SetHandleIcon" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.handleIcon = " + tr "v" + ", $this)")
            "SetHandleSize" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.handleSize = " + tr "v" + ", $this)")
            "SetHandleStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.handleStyle = " + tr "v" + ", $this)")
            "SetLabelPrecision" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.labelPrecision = " + tr "v" + ", $this)")
            "SetLabelFormatter" => (T<string> + (T<unit> ^-> T<unit>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.labelFormatter = " + tr "v" + ", $this)")
            "SetShowDetail" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.showDetail = " + tr "v" + ", $this)")
            "SetShowDataShadow" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.showDataShadow = " + tr "v" + ", $this)")
            "SetRealtime" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.realtime = " + tr "v" + ", $this)")
            "SetTextStyle" => BaseTextStyle?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textStyle = " + tr "v" + ", $this)")
            "SetXAxisIndex" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.xAxisIndex = " + tr "v" + ", $this)")
            "SetYAxisIndex" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.yAxisIndex = " + tr "v" + ", $this)")
            "SetRadiusAxisIndex" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.radiusAxisIndex = " + tr "v" + ", $this)")
            "SetAngleAxisIndex" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.angleAxisIndex = " + tr "v" + ", $this)")
            "SetSingleAxisIndex" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.singleAxisIndex = " + tr "v" + ", $this)")
            "SetFilterMode" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.filterMode = " + tr "v" + ", $this)")
            "SetStart" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.start = " + tr "v" + ", $this)")
            "SetEnd" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.end = " + tr "v" + ", $this)")
            "SetStartValue" => (T<float> + T<string> + T<Date>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.startValue = " + tr "v" + ", $this)")
            "SetEndValue" => (T<float> + T<string> + T<Date>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.endValue = " + tr "v" + ", $this)")
            "SetMinSpan" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.minSpan = " + tr "v" + ", $this)")
            "SetMaxSpan" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.maxSpan = " + tr "v" + ", $this)")
            "SetMinValueSpan" => (T<float> + T<string> + T<Date>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.minValueSpan = " + tr "v" + ", $this)")
            "SetMaxValueSpan" => (T<float> + T<string> + T<Date>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.maxValueSpan = " + tr "v" + ", $this)")
            "SetOrient" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.orient = " + tr "v" + ", $this)")
            "SetZoomLock" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zoomLock = " + tr "v" + ", $this)")
            "SetThrottle" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.throttle = " + tr "v" + ", $this)")
            "SetRangeMode" => (!| T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.rangeMode = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetLeft" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.left = " + tr "v" + ", $this)")
            "SetTop" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.top = " + tr "v" + ", $this)")
            "SetRight" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.right = " + tr "v" + ", $this)")
            "SetBottom" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.bottom = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "show" =@ (!? T<bool>)
            "backgroundColor" =@ (!? T<string>)
            "dataBackground" =@ (!? T<obj>)
            "fillerColor" =@ (!? T<string>)
            "borderColor" =@ (!? T<string>)
            "handleIcon" =@ (!? T<string>)
            "handleSize" =@ (!? (T<float> + T<string>))
            "handleStyle" =@ (!? T<obj>)
            "labelPrecision" =@ (!? T<float>)
            "labelFormatter" =@ (!? (T<string> + (T<unit> ^-> T<unit>)))
            "showDetail" =@ (!? T<bool>)
            "showDataShadow" =@ (!? T<string>)
            "realtime" =@ (!? T<bool>)
            "textStyle" =@ (!? BaseTextStyle)
            "xAxisIndex" =@ (!? (T<float> + (!| T<float>)))
            "yAxisIndex" =@ (!? (T<float> + (!| T<float>)))
            "radiusAxisIndex" =@ (!? (T<float> + (!| T<float>)))
            "angleAxisIndex" =@ (!? (T<float> + (!| T<float>)))
            "singleAxisIndex" =@ (!? (T<float> + (!| T<float>)))
            "filterMode" =@ (!? (T<string>))
            "start" =@ (!? T<float>)
            "end" =@ (!? T<float>)
            "startValue" =@ (!? (T<float> + T<string> + T<Date>))
            "endValue" =@ (!? (T<float> + T<string> + T<Date>))
            "minSpan" =@ (!? T<float>)
            "maxSpan" =@ (!? T<float>)
            "minValueSpan" =@ (!? (T<float> + T<string> + T<Date>))
            "maxValueSpan" =@ (!? (T<float> + T<string> + T<Date>))
            "orient" =@ (!? (T<string>))
            "zoomLock" =@ (!? T<bool>)
            "throttle" =@ (!? T<float>)
            "rangeMode" =@ (!? (!| T<string>))
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "left" =@ (!? (T<string> + T<float>))
            "top" =@ (!? (T<string> + T<float>))
            "right" =@ (!? (T<string> + T<float>))
            "bottom" =@ (!? (T<string> + T<float>))
        ]
        |> ignore

    Grid
        |> WithSourceName "Grid"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetShow" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.show = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetLeft" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.left = " + tr "v" + ", $this)")
            "SetTop" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.top = " + tr "v" + ", $this)")
            "SetRight" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.right = " + tr "v" + ", $this)")
            "SetBottom" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.bottom = " + tr "v" + ", $this)")
            "SetWidth" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.width = " + tr "v" + ", $this)")
            "SetHeight" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.height = " + tr "v" + ", $this)")
            "SetContainLabel" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.containLabel = " + tr "v" + ", $this)")
            "SetBackgroundColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.backgroundColor = " + tr "v" + ", $this)")
            "SetBorderColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderColor = " + tr "v" + ", $this)")
            "SetBorderWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderWidth = " + tr "v" + ", $this)")
            "SetShadowBlur" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowBlur = " + tr "v" + ", $this)")
            "SetShadowColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowColor = " + tr "v" + ", $this)")
            "SetShadowOffsetX" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowOffsetX = " + tr "v" + ", $this)")
            "SetShadowOffsetY" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowOffsetY = " + tr "v" + ", $this)")
            "SetTooltip" => Tooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "id" =@ (!? T<string>)
            "show" =@ (!? T<bool>)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "left" =@ (!? (T<float> + T<string>))
            "top" =@ (!? (T<float> + T<string>))
            "right" =@ (!? (T<float> + T<string>))
            "bottom" =@ (!? (T<float> + T<string>))
            "width" =@ (!? (T<float> + T<string>))
            "height" =@ (!? (T<float> + T<string>))
            "containLabel" =@ (!? T<bool>)
            "backgroundColor" =@ (!? T<string>)
            "borderColor" =@ (!? T<string>)
            "borderWidth" =@ (!? T<float>)
            "shadowBlur" =@ (!? T<float>)
            "shadowColor" =@ (!? T<string>)
            "shadowOffsetX" =@ (!? T<float>)
            "shadowOffsetY" =@ (!? T<float>)
            "tooltip" =@ (!? Tooltip)
        ]
        |> ignore

    Legend
        |> WithSourceName "Legend"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetShow" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.show = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetLeft" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.left = " + tr "v" + ", $this)")
            "SetTop" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.top = " + tr "v" + ", $this)")
            "SetRight" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.right = " + tr "v" + ", $this)")
            "SetBottom" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.bottom = " + tr "v" + ", $this)")
            "SetWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.width = " + tr "v" + ", $this)")
            "SetHeight" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.height = " + tr "v" + ", $this)")
            "SetOrient" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.orient = " + tr "v" + ", $this)")
            "SetAlign" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.align = " + tr "v" + ", $this)")
            "SetPadding" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.padding = " + tr "v" + ", $this)")
            "SetItemGap" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemGap = " + tr "v" + ", $this)")
            "SetItemWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemWidth = " + tr "v" + ", $this)")
            "SetItemHeight" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemHeight = " + tr "v" + ", $this)")
            "SetSymbolKeepAspect" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolKeepAspect = " + tr "v" + ", $this)")
            "SetFormatter" => (T<string> + (T<string>?name ^-> T<string>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.formatter = " + tr "v" + ", $this)")
            "SetSelectedMode" => (T<bool> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.selectedMode = " + tr "v" + ", $this)")
            "SetInactiveColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.inactiveColor = " + tr "v" + ", $this)")
            "SetSelected" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.selected = " + tr "v" + ", $this)")
            "SetTextStyle" => TextStyleWithRich?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textStyle = " + tr "v" + ", $this)")
            "SetTooltip" => Tooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
            "SetIcon" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.icon = " + tr "v" + ", $this)")
            "SetData" => ((!| T<string>) + (!| Legend_LegendDataObject))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetBackgroundColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.backgroundColor = " + tr "v" + ", $this)")
            "SetBorderColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderColor = " + tr "v" + ", $this)")
            "SetBorderWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderWidth = " + tr "v" + ", $this)")
            "SetBorderRadius" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderRadius = " + tr "v" + ", $this)")
            "SetShadowBlur" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowBlur = " + tr "v" + ", $this)")
            "SetShadowColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowColor = " + tr "v" + ", $this)")
            "SetShadowOffsetX" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowOffsetX = " + tr "v" + ", $this)")
            "SetShadowOffsetY" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowOffsetY = " + tr "v" + ", $this)")
            "SetScrollDataIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.scrollDataIndex = " + tr "v" + ", $this)")
            "SetPageButtonItemGap" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.pageButtonItemGap = " + tr "v" + ", $this)")
            "SetPageButtonGap" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.pageButtonGap = " + tr "v" + ", $this)")
            "SetPageButtonPosition" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.pageButtonPosition = " + tr "v" + ", $this)")
            "SetPageFormatter" => (T<string> + (T<float>?current * T<float>?total ^-> T<string>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.pageFormatter = " + tr "v" + ", $this)")
            "SetPageIcons" => Legend_PageIcons?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.pageIcons = " + tr "v" + ", $this)")
            "SetPageIconColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.pageIconColor = " + tr "v" + ", $this)")
            "SetPageIconInactiveColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.pageIconInactiveColor = " + tr "v" + ", $this)")
            "SetPageIconSize" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.pageIconSize = " + tr "v" + ", $this)")
            "SetPageTextStyle" => TextStyle?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.pageTextStyle = " + tr "v" + ", $this)")
            "SetAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animation = " + tr "v" + ", $this)")
            "SetAnimationDurationUpdate" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDurationUpdate = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? (T<string>))
            "id" =@ (!? T<string>)
            "show" =@ (!? T<bool>)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "left" =@ (!? (T<string> + T<float>))
            "top" =@ (!? (T<string> + T<float>))
            "right" =@ (!? (T<string> + T<float>))
            "bottom" =@ (!? (T<string> + T<float>))
            "width" =@ (!? T<float>)
            "height" =@ (!? T<float>)
            "orient" =@ (!? (T<string>))
            "align" =@ (!? (T<string>))
            "padding" =@ (!? (T<float> + (!| T<float>)))
            "itemGap" =@ (!? T<float>)
            "itemWidth" =@ (!? T<float>)
            "itemHeight" =@ (!? T<float>)
            "symbolKeepAspect" =@ (!? T<bool>)
            "formatter" =@ (!? (T<string> + (T<string>?name ^-> T<string>)))
            "selectedMode" =@ (!? (T<bool> + T<string>))
            "inactiveColor" =@ (!? T<string>)
            "selected" =@ (!? T<obj>)
            "textStyle" =@ (!? TextStyleWithRich)
            "tooltip" =@ (!? Tooltip)
            "icon" =@ (!? T<string>)
            "data" =@ (!? ((!| T<string>) + (!| Legend_LegendDataObject)))
            "backgroundColor" =@ (!? T<string>)
            "borderColor" =@ (!? T<string>)
            "borderWidth" =@ (!? T<float>)
            "borderRadius" =@ (!? (T<float> + (!| T<float>)))
            "shadowBlur" =@ (!? T<float>)
            "shadowColor" =@ (!? T<string>)
            "shadowOffsetX" =@ (!? T<float>)
            "shadowOffsetY" =@ (!? T<float>)
            "scrollDataIndex" =@ (!? T<float>)
            "pageButtonItemGap" =@ (!? T<float>)
            "pageButtonGap" =@ (!? T<float>)
            "pageButtonPosition" =@ (!? (T<string>))
            "pageFormatter" =@ (!? (T<string> + (T<float>?current * T<float>?total ^-> T<string>)))
            "pageIcons" =@ (!? Legend_PageIcons)
            "pageIconColor" =@ (!? T<string>)
            "pageIconInactiveColor" =@ (!? T<string>)
            "pageIconSize" =@ (!? (T<float> + (!| T<float>)))
            "pageTextStyle" =@ (!? TextStyle)
            "animation" =@ (!? T<bool>)
            "animationDurationUpdate" =@ (!? T<float>)
        ]
        |> ignore

    Legend_LegendDataObject
        |> WithSourceName "Legend_LegendDataObject"
        |+> Static [ ObjectConstructor ((!? T<string>)?name * (!? T<string>)?icon * (!? TextStyle)?textStyle) ]
        |+> Instance [
            "name" =@ (!? T<string>)
            "icon" =@ (!? T<string>)
            "textStyle" =@ (!? TextStyle)
        ]
        |> ignore

    Legend_PageIcons
        |> WithSourceName "Legend_PageIcons"
        |+> Static [ ObjectConstructor ((!? (!| T<string>))?horizontal * (!? (!| T<string>))?vertical) ]
        |+> Instance [
            "horizontal" =@ (!? (!| T<string>))
            "vertical" =@ (!? (!| T<string>))
        ]
        |> ignore

    LineStyle
        |> WithSourceName "LineStyle"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetColor" => (T<string> + (!| T<string>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.color = " + tr "v" + ", $this)")
            "SetWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.width = " + tr "v" + ", $this)")
            "SetType" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetShadowBlur" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowBlur = " + tr "v" + ", $this)")
            "SetShadowColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowColor = " + tr "v" + ", $this)")
            "SetShadowOffsetX" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowOffsetX = " + tr "v" + ", $this)")
            "SetShadowOffsetY" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowOffsetY = " + tr "v" + ", $this)")
            "SetOpacity" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.opacity = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "color" =@ (!? (T<string> + (!| T<string>)))
            "width" =@ (!? T<float>)
            "type" =@ (!? (T<string>))
            "shadowBlur" =@ (!? T<float>)
            "shadowColor" =@ (!? T<string>)
            "shadowOffsetX" =@ (!? T<float>)
            "shadowOffsetY" =@ (!? T<float>)
            "opacity" =@ (!? T<float>)
        ]
        |> ignore

    SingleAxis
        |> WithSourceName "SingleAxis"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetOrient" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.orient = " + tr "v" + ", $this)")
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetLeft" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.left = " + tr "v" + ", $this)")
            "SetTop" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.top = " + tr "v" + ", $this)")
            "SetRight" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.right = " + tr "v" + ", $this)")
            "SetBottom" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.bottom = " + tr "v" + ", $this)")
            "SetWidth" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.width = " + tr "v" + ", $this)")
            "SetHeight" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.height = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetShow" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.show = " + tr "v" + ", $this)")
            "SetGridIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.gridIndex = " + tr "v" + ", $this)")
            "SetOffset" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.offset = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetNameLocation" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nameLocation = " + tr "v" + ", $this)")
            "SetNameTextStyle" => TextStyleWithRich?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nameTextStyle = " + tr "v" + ", $this)")
            "SetNameGap" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nameGap = " + tr "v" + ", $this)")
            "SetNameRotate" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nameRotate = " + tr "v" + ", $this)")
            "SetInverse" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.inverse = " + tr "v" + ", $this)")
            "SetBoundaryGap" => (T<bool> + (!| (T<string> + T<float>)))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.boundaryGap = " + tr "v" + ", $this)")
            "SetMin" => (T<float> + T<string> + (ChartExtremes?value ^-> T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.min = " + tr "v" + ", $this)")
            "SetMax" => (T<float> + T<string> + (ChartExtremes?value ^-> T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.max = " + tr "v" + ", $this)")
            "SetScale" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.scale = " + tr "v" + ", $this)")
            "SetSplitNumber" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.splitNumber = " + tr "v" + ", $this)")
            "SetMinInterval" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.minInterval = " + tr "v" + ", $this)")
            "SetInterval" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.interval = " + tr "v" + ", $this)")
            "SetLogBase" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.logBase = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetTriggerEvent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.triggerEvent = " + tr "v" + ", $this)")
            "SetAxisLine" => Line?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.axisLine = " + tr "v" + ", $this)")
            "SetAxisTick" => CartesianAxis_Tick?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.axisTick = " + tr "v" + ", $this)")
            "SetMinorTick" => CartesianAxis_MinorTick?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.minorTick = " + tr "v" + ", $this)")
            "SetAxisLabel" => CartesianAxis_Label?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.axisLabel = " + tr "v" + ", $this)")
            "SetSplitLine" => CartesianAxis_SplitLine?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.splitLine = " + tr "v" + ", $this)")
            "SetMinorSplitLine" => CartesianAxis_MinorSplitLine?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.minorSplitLine = " + tr "v" + ", $this)")
            "SetSplitArea" => CartesianAxis_SplitArea?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.splitArea = " + tr "v" + ", $this)")
            "SetData" => (!| (T<string> + T<float> + CartesianAxis_DataObject))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetAxisPointer" => CartesianAxis_Pointer?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.axisPointer = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "orient" =@ (!? (T<string>))
            "type" =@ (!? T<string>)
            "left" =@ (!? (T<string> + T<float>))
            "top" =@ (!? (T<string> + T<float>))
            "right" =@ (!? (T<string> + T<float>))
            "bottom" =@ (!? (T<string> + T<float>))
            "width" =@ (!? (T<string> + T<float>))
            "height" =@ (!? (T<string> + T<float>))
            "id" =@ (!? T<string>)
            "show" =@ (!? T<bool>)
            "gridIndex" =@ (!? T<float>)
            "offset" =@ (!? T<float>)
            "name" =@ (!? T<string>)
            "nameLocation" =@ (!? (T<string>))
            "nameTextStyle" =@ (!? TextStyleWithRich)
            "nameGap" =@ (!? T<float>)
            "nameRotate" =@ (!? T<float>)
            "inverse" =@ (!? T<bool>)
            "boundaryGap" =@ (!? (T<bool> + (!| (T<string> + T<float>))))
            "min" =@ (!? (T<float> + T<string> + (ChartExtremes?value ^-> T<float>)))
            "max" =@ (!? (T<float> + T<string> + (ChartExtremes?value ^-> T<float>)))
            "scale" =@ (!? T<bool>)
            "splitNumber" =@ (!? T<float>)
            "minInterval" =@ (!? T<obj>)
            "interval" =@ (!? T<float>)
            "logBase" =@ (!? T<float>)
            "silent" =@ (!? T<bool>)
            "triggerEvent" =@ (!? T<bool>)
            "axisLine" =@ (!? Line)
            "axisTick" =@ (!? CartesianAxis_Tick)
            "minorTick" =@ (!? CartesianAxis_MinorTick)
            "axisLabel" =@ (!? CartesianAxis_Label)
            "splitLine" =@ (!? CartesianAxis_SplitLine)
            "minorSplitLine" =@ (!? CartesianAxis_MinorSplitLine)
            "splitArea" =@ (!? CartesianAxis_SplitArea)
            "data" =@ (!? (!| (T<string> + T<float> + CartesianAxis_DataObject)))
            "axisPointer" =@ (!? CartesianAxis_Pointer)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
        ]
        |> ignore

    BaseTextStyle
        |> WithSourceName "BaseTextStyle"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.color = " + tr "v" + ", $this)")
            "SetFontStyle" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontStyle = " + tr "v" + ", $this)")
            "SetFontWeight" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontWeight = " + tr "v" + ", $this)")
            "SetFontFamily" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontFamily = " + tr "v" + ", $this)")
            "SetFontSize" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontSize = " + tr "v" + ", $this)")
            "SetLineHeight" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineHeight = " + tr "v" + ", $this)")
            "SetWidth" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.width = " + tr "v" + ", $this)")
            "SetHeight" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.height = " + tr "v" + ", $this)")
            "SetTextBorderColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textBorderColor = " + tr "v" + ", $this)")
            "SetTextBorderWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textBorderWidth = " + tr "v" + ", $this)")
            "SetTextShadowColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowColor = " + tr "v" + ", $this)")
            "SetTextShadowBlur" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowBlur = " + tr "v" + ", $this)")
            "SetTextShadowOffsetX" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowOffsetX = " + tr "v" + ", $this)")
            "SetTextShadowOffsetY" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowOffsetY = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "color" =@ (!? T<string>)
            "fontStyle" =@ (!? (T<string>))
            "fontWeight" =@ (!? (T<string> + T<float>))
            "fontFamily" =@ (!? T<string>)
            "fontSize" =@ (!? T<float>)
            "lineHeight" =@ (!? T<float>)
            "width" =@ (!? (T<float> + T<string>))
            "height" =@ (!? (T<float> + T<string>))
            "textBorderColor" =@ (!? T<string>)
            "textBorderWidth" =@ (!? T<float>)
            "textShadowColor" =@ (!? T<string>)
            "textShadowBlur" =@ (!? T<float>)
            "textShadowOffsetX" =@ (!? T<float>)
            "textShadowOffsetY" =@ (!? T<float>)
        ]
        |> ignore

    TextStyle
        |> WithSourceName "TextStyle"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetAlign" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.align = " + tr "v" + ", $this)")
            "SetVerticalAlign" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.verticalAlign = " + tr "v" + ", $this)")
            "SetBackgroundColor" => (T<string> + T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.backgroundColor = " + tr "v" + ", $this)")
            "SetBorderColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderColor = " + tr "v" + ", $this)")
            "SetBorderWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderWidth = " + tr "v" + ", $this)")
            "SetBorderRadius" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderRadius = " + tr "v" + ", $this)")
            "SetPadding" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.padding = " + tr "v" + ", $this)")
            "SetShadowColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowColor = " + tr "v" + ", $this)")
            "SetShadowBlur" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowBlur = " + tr "v" + ", $this)")
            "SetShadowOffsetX" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowOffsetX = " + tr "v" + ", $this)")
            "SetShadowOffsetY" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowOffsetY = " + tr "v" + ", $this)")
            "SetColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.color = " + tr "v" + ", $this)")
            "SetFontStyle" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontStyle = " + tr "v" + ", $this)")
            "SetFontWeight" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontWeight = " + tr "v" + ", $this)")
            "SetFontFamily" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontFamily = " + tr "v" + ", $this)")
            "SetFontSize" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontSize = " + tr "v" + ", $this)")
            "SetLineHeight" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineHeight = " + tr "v" + ", $this)")
            "SetWidth" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.width = " + tr "v" + ", $this)")
            "SetHeight" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.height = " + tr "v" + ", $this)")
            "SetTextBorderColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textBorderColor = " + tr "v" + ", $this)")
            "SetTextBorderWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textBorderWidth = " + tr "v" + ", $this)")
            "SetTextShadowColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowColor = " + tr "v" + ", $this)")
            "SetTextShadowBlur" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowBlur = " + tr "v" + ", $this)")
            "SetTextShadowOffsetX" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowOffsetX = " + tr "v" + ", $this)")
            "SetTextShadowOffsetY" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowOffsetY = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "align" =@ (!? T<string>)
            "verticalAlign" =@ (!? T<string>)
            "backgroundColor" =@ (!? (T<string> + T<obj>))
            "borderColor" =@ (!? T<string>)
            "borderWidth" =@ (!? T<float>)
            "borderRadius" =@ (!? T<float>)
            "padding" =@ (!? (T<float> + (!| T<float>)))
            "shadowColor" =@ (!? T<string>)
            "shadowBlur" =@ (!? T<float>)
            "shadowOffsetX" =@ (!? T<float>)
            "shadowOffsetY" =@ (!? T<float>)
            "color" =@ (!? T<string>)
            "fontStyle" =@ (!? (T<string>))
            "fontWeight" =@ (!? (T<string> + T<float>))
            "fontFamily" =@ (!? T<string>)
            "fontSize" =@ (!? T<float>)
            "lineHeight" =@ (!? T<float>)
            "width" =@ (!? (T<float> + T<string>))
            "height" =@ (!? (T<float> + T<string>))
            "textBorderColor" =@ (!? T<string>)
            "textBorderWidth" =@ (!? T<float>)
            "textShadowColor" =@ (!? T<string>)
            "textShadowBlur" =@ (!? T<float>)
            "textShadowOffsetX" =@ (!? T<float>)
            "textShadowOffsetY" =@ (!? T<float>)
        ]
        |> ignore

    RichStyle
        |> WithSourceName "RichStyle"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
        ]
        |> ignore

    BaseTextStyleWithRich
        |> WithSourceName "BaseTextStyleWithRich"
        |+> Static [ ObjectConstructor ((!? RichStyle)?rich) ]
        |+> Instance [
            "rich" =@ (!? RichStyle)
        ]
        |> ignore

    TextStyleWithRich
        |> WithSourceName "TextStyleWithRich"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetRich" => RichStyle?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.rich = " + tr "v" + ", $this)")
            "SetAlign" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.align = " + tr "v" + ", $this)")
            "SetVerticalAlign" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.verticalAlign = " + tr "v" + ", $this)")
            "SetBackgroundColor" => (T<string> + T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.backgroundColor = " + tr "v" + ", $this)")
            "SetBorderColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderColor = " + tr "v" + ", $this)")
            "SetBorderWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderWidth = " + tr "v" + ", $this)")
            "SetBorderRadius" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderRadius = " + tr "v" + ", $this)")
            "SetPadding" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.padding = " + tr "v" + ", $this)")
            "SetShadowColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowColor = " + tr "v" + ", $this)")
            "SetShadowBlur" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowBlur = " + tr "v" + ", $this)")
            "SetShadowOffsetX" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowOffsetX = " + tr "v" + ", $this)")
            "SetShadowOffsetY" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowOffsetY = " + tr "v" + ", $this)")
            "SetColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.color = " + tr "v" + ", $this)")
            "SetFontStyle" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontStyle = " + tr "v" + ", $this)")
            "SetFontWeight" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontWeight = " + tr "v" + ", $this)")
            "SetFontFamily" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontFamily = " + tr "v" + ", $this)")
            "SetFontSize" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontSize = " + tr "v" + ", $this)")
            "SetLineHeight" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineHeight = " + tr "v" + ", $this)")
            "SetWidth" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.width = " + tr "v" + ", $this)")
            "SetHeight" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.height = " + tr "v" + ", $this)")
            "SetTextBorderColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textBorderColor = " + tr "v" + ", $this)")
            "SetTextBorderWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textBorderWidth = " + tr "v" + ", $this)")
            "SetTextShadowColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowColor = " + tr "v" + ", $this)")
            "SetTextShadowBlur" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowBlur = " + tr "v" + ", $this)")
            "SetTextShadowOffsetX" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowOffsetX = " + tr "v" + ", $this)")
            "SetTextShadowOffsetY" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowOffsetY = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "rich" =@ (!? RichStyle)
            "align" =@ (!? T<string>)
            "verticalAlign" =@ (!? T<string>)
            "backgroundColor" =@ (!? (T<string> + T<obj>))
            "borderColor" =@ (!? T<string>)
            "borderWidth" =@ (!? T<float>)
            "borderRadius" =@ (!? T<float>)
            "padding" =@ (!? (T<float> + (!| T<float>)))
            "shadowColor" =@ (!? T<string>)
            "shadowBlur" =@ (!? T<float>)
            "shadowOffsetX" =@ (!? T<float>)
            "shadowOffsetY" =@ (!? T<float>)
            "color" =@ (!? T<string>)
            "fontStyle" =@ (!? (T<string>))
            "fontWeight" =@ (!? (T<string> + T<float>))
            "fontFamily" =@ (!? T<string>)
            "fontSize" =@ (!? T<float>)
            "lineHeight" =@ (!? T<float>)
            "width" =@ (!? (T<float> + T<string>))
            "height" =@ (!? (T<float> + T<string>))
            "textBorderColor" =@ (!? T<string>)
            "textBorderWidth" =@ (!? T<float>)
            "textShadowColor" =@ (!? T<string>)
            "textShadowBlur" =@ (!? T<float>)
            "textShadowOffsetX" =@ (!? T<float>)
            "textShadowOffsetY" =@ (!? T<float>)
        ]
        |> ignore

    BaseTooltip
        |> WithSourceName "BaseTooltip"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetPosition" => (T<string> + Tooltip_Position_Obj + (!| (T<string> + T<float>)) + ((!| (T<float> + T<string>))?point * (T<obj> + (!| T<obj>))?``params`` * T<HTMLElement>?element * T<obj>?rect * T<obj>?size ^-> ((!| (T<float> + T<string>)) + Tooltip_Position_Obj)))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.position = " + tr "v" + ", $this)")
            "SetFormatter" => (T<string> + ((Tooltip_Format + (!| Tooltip_Format))?``params`` * T<string>?ticket * (T<string>?ticket * T<string>?html ^-> T<unit>)?callback ^-> T<string>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.formatter = " + tr "v" + ", $this)")
            "SetBackgroundColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.backgroundColor = " + tr "v" + ", $this)")
            "SetBorderColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderColor = " + tr "v" + ", $this)")
            "SetBorderWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderWidth = " + tr "v" + ", $this)")
            "SetPadding" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.padding = " + tr "v" + ", $this)")
            "SetTextStyle" => BaseTextStyle?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textStyle = " + tr "v" + ", $this)")
            "SetExtraCssText" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.extraCssText = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "position" =@ (!? (T<string> + Tooltip_Position_Obj + (!| (T<string> + T<float>)) + ((!| (T<float> + T<string>))?point * (T<obj> + (!| T<obj>))?``params`` * T<HTMLElement>?element * T<obj>?rect * T<obj>?size ^-> ((!| (T<float> + T<string>)) + Tooltip_Position_Obj))))
            "formatter" =@ (!? (T<string> + ((Tooltip_Format + (!| Tooltip_Format))?``params`` * T<string>?ticket * (T<string>?ticket * T<string>?html ^-> T<unit>)?callback ^-> T<string>)))
            "backgroundColor" =@ (!? T<string>)
            "borderColor" =@ (!? T<string>)
            "borderWidth" =@ (!? T<float>)
            "padding" =@ (!? (T<float> + (!| T<float>)))
            "textStyle" =@ (!? BaseTextStyle)
            "extraCssText" =@ (!? T<string>)
        ]
        |> ignore

    Tooltip
        |> WithSourceName "Tooltip"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetShow" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.show = " + tr "v" + ", $this)")
            "SetTrigger" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.trigger = " + tr "v" + ", $this)")
            "SetAxisPointer" => Tooltip_AxisPointer?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.axisPointer = " + tr "v" + ", $this)")
            "SetShowContent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.showContent = " + tr "v" + ", $this)")
            "SetAlwaysShowContent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.alwaysShowContent = " + tr "v" + ", $this)")
            "SetTriggerOn" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.triggerOn = " + tr "v" + ", $this)")
            "SetShowDelay" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.showDelay = " + tr "v" + ", $this)")
            "SetHideDelay" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.hideDelay = " + tr "v" + ", $this)")
            "SetEnterable" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.enterable = " + tr "v" + ", $this)")
            "SetRenderMode" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.renderMode = " + tr "v" + ", $this)")
            "SetConfine" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.confine = " + tr "v" + ", $this)")
            "SetTransitionDuration" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.transitionDuration = " + tr "v" + ", $this)")
            "SetPosition" => (T<string> + Tooltip_Position_Obj + (!| (T<string> + T<float>)) + ((!| (T<float> + T<string>))?point * (T<obj> + (!| T<obj>))?``params`` * T<HTMLElement>?element * T<obj>?rect * T<obj>?size ^-> ((!| (T<float> + T<string>)) + Tooltip_Position_Obj)))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.position = " + tr "v" + ", $this)")
            "SetFormatter" => (T<string> + ((Tooltip_Format + (!| Tooltip_Format))?``params`` * T<string>?ticket * (T<string>?ticket * T<string>?html ^-> T<unit>)?callback ^-> T<string>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.formatter = " + tr "v" + ", $this)")
            "SetBackgroundColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.backgroundColor = " + tr "v" + ", $this)")
            "SetBorderColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderColor = " + tr "v" + ", $this)")
            "SetBorderWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderWidth = " + tr "v" + ", $this)")
            "SetPadding" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.padding = " + tr "v" + ", $this)")
            "SetTextStyle" => BaseTextStyle?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textStyle = " + tr "v" + ", $this)")
            "SetExtraCssText" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.extraCssText = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "show" =@ (!? T<bool>)
            "trigger" =@ (!? (T<string>))
            "axisPointer" =@ (!? Tooltip_AxisPointer)
            "showContent" =@ (!? T<bool>)
            "alwaysShowContent" =@ (!? T<bool>)
            "triggerOn" =@ (!? (T<string>))
            "showDelay" =@ (!? T<float>)
            "hideDelay" =@ (!? T<float>)
            "enterable" =@ (!? T<bool>)
            "renderMode" =@ (!? T<string>)
            "confine" =@ (!? T<bool>)
            "transitionDuration" =@ (!? T<float>)
            "position" =@ (!? (T<string> + Tooltip_Position_Obj + (!| (T<string> + T<float>)) + ((!| (T<float> + T<string>))?point * (T<obj> + (!| T<obj>))?``params`` * T<HTMLElement>?element * T<obj>?rect * T<obj>?size ^-> ((!| (T<float> + T<string>)) + Tooltip_Position_Obj))))
            "formatter" =@ (!? (T<string> + ((Tooltip_Format + (!| Tooltip_Format))?``params`` * T<string>?ticket * (T<string>?ticket * T<string>?html ^-> T<unit>)?callback ^-> T<string>)))
            "backgroundColor" =@ (!? T<string>)
            "borderColor" =@ (!? T<string>)
            "borderWidth" =@ (!? T<float>)
            "padding" =@ (!? (T<float> + (!| T<float>)))
            "textStyle" =@ (!? BaseTextStyle)
            "extraCssText" =@ (!? T<string>)
        ]
        |> ignore

    Tooltip_Position_Obj
        |> WithSourceName "Tooltip_Position_Obj"
        |+> Static [ ObjectConstructor ((!? (T<string> + T<float>))?top * (!? (T<string> + T<float>))?right * (!? (T<string> + T<float>))?bottom * (!? (T<string> + T<float>))?left) ]
        |+> Instance [
            "top" =@ (!? (T<string> + T<float>))
            "right" =@ (!? (T<string> + T<float>))
            "bottom" =@ (!? (T<string> + T<float>))
            "left" =@ (!? (T<string> + T<float>))
        ]
        |> ignore

    Tooltip_Format
        |> WithSourceName "Tooltip_Format"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetComponentType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.componentType = " + tr "v" + ", $this)")
            "SetSeriesType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.seriesType = " + tr "v" + ", $this)")
            "SetSeriesIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.seriesIndex = " + tr "v" + ", $this)")
            "SetSeriesName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.seriesName = " + tr "v" + ", $this)")
            "SetMarker" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.marker = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetDataIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.dataIndex = " + tr "v" + ", $this)")
            "SetData" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetValue" => (T<float> + (!| T<obj>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.value = " + tr "v" + ", $this)")
            "SetAxisValue" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.axisValue = " + tr "v" + ", $this)")
            "SetAxisValueLabel" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.axisValueLabel = " + tr "v" + ", $this)")
            "SetEncode" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.encode = " + tr "v" + ", $this)")
            "SetDimensionNames" => (!| T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.dimensionNames = " + tr "v" + ", $this)")
            "SetDimensionIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.dimensionIndex = " + tr "v" + ", $this)")
            "SetColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.color = " + tr "v" + ", $this)")
            "SetPercent" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.percent = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "componentType" =@ (!? T<string>)
            "seriesType" =@ (!? T<string>)
            "seriesIndex" =@ (!? T<float>)
            "seriesName" =@ (!? T<string>)
            "marker" =@ (!? T<string>)
            "name" =@ (!? T<string>)
            "dataIndex" =@ (!? T<float>)
            "data" =@ (!? T<obj>)
            "value" =@ (!? (T<float> + (!| T<obj>)))
            "axisValue" =@ (!? (T<float> + T<string>))
            "axisValueLabel" =@ (!? T<string>)
            "encode" =@ (!? T<obj>)
            "dimensionNames" =@ (!? (!| T<string>))
            "dimensionIndex" =@ (!? T<float>)
            "color" =@ (!? T<string>)
            "percent" =@ (!? T<float>)
        ]
        |> ignore

    Tooltip_AxisPointer
        |> WithSourceName "Tooltip_AxisPointer"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetShow" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.show = " + tr "v" + ", $this)")
            "SetType" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetAxis" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.axis = " + tr "v" + ", $this)")
            "SetSnap" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.snap = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetLabel" => CartesianAxis_PointerLabel?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetLineStyle" => LineStyle?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineStyle = " + tr "v" + ", $this)")
            "SetShadowStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowStyle = " + tr "v" + ", $this)")
            "SetCrossStyle" => LineStyle?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.crossStyle = " + tr "v" + ", $this)")
            "SetAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animation = " + tr "v" + ", $this)")
            "SetAnimationThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationThreshold = " + tr "v" + ", $this)")
            "SetAnimationDuration" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDuration = " + tr "v" + ", $this)")
            "SetAnimationEasing" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasing = " + tr "v" + ", $this)")
            "SetAnimationDelay" => (T<float> + (T<unit> ^-> T<unit>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelay = " + tr "v" + ", $this)")
            "SetAnimationDurationUpdate" => (T<float> + (T<unit> ^-> T<unit>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDurationUpdate = " + tr "v" + ", $this)")
            "SetAnimationEasingUpdate" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasingUpdate = " + tr "v" + ", $this)")
            "SetAnimationDelayUpdate" => (T<float> + (T<unit> ^-> T<unit>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelayUpdate = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "show" =@ (!? T<bool>)
            "type" =@ (!? (T<string>))
            "axis" =@ (!? (T<string>))
            "snap" =@ (!? T<bool>)
            "z" =@ (!? T<float>)
            "label" =@ (!? CartesianAxis_PointerLabel)
            "lineStyle" =@ (!? LineStyle)
            "shadowStyle" =@ (!? T<obj>)
            "crossStyle" =@ (!? LineStyle)
            "animation" =@ (!? T<bool>)
            "animationThreshold" =@ (!? T<float>)
            "animationDuration" =@ (!? T<float>)
            "animationEasing" =@ (!? T<string>)
            "animationDelay" =@ (!? (T<float> + (T<unit> ^-> T<unit>)))
            "animationDurationUpdate" =@ (!? (T<float> + (T<unit> ^-> T<unit>)))
            "animationEasingUpdate" =@ (!? T<string>)
            "animationDelayUpdate" =@ (!? (T<float> + (T<unit> ^-> T<unit>)))
        ]
        |> ignore

    VisualMap_Continuous
        |> WithSourceName "VisualMap_Continuous"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetMin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.min = " + tr "v" + ", $this)")
            "SetMax" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.max = " + tr "v" + ", $this)")
            "SetRange" => (!| T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.range = " + tr "v" + ", $this)")
            "SetCalculable" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.calculable = " + tr "v" + ", $this)")
            "SetRealtime" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.realtime = " + tr "v" + ", $this)")
            "SetInverse" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.inverse = " + tr "v" + ", $this)")
            "SetPrecision" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.precision = " + tr "v" + ", $this)")
            "SetItemWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemWidth = " + tr "v" + ", $this)")
            "SetItemHeight" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemHeight = " + tr "v" + ", $this)")
            "SetAlign" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.align = " + tr "v" + ", $this)")
            "SetText" => (!| T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.text = " + tr "v" + ", $this)")
            "SetTextGap" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textGap = " + tr "v" + ", $this)")
            "SetShow" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.show = " + tr "v" + ", $this)")
            "SetDimension" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.dimension = " + tr "v" + ", $this)")
            "SetSeriesIndex" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.seriesIndex = " + tr "v" + ", $this)")
            "SetHoverLink" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.hoverLink = " + tr "v" + ", $this)")
            "SetInRange" => VisualMap_RangeObject?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.inRange = " + tr "v" + ", $this)")
            "SetOutOfRange" => VisualMap_RangeObject?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.outOfRange = " + tr "v" + ", $this)")
            "SetController" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.controller = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetLeft" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.left = " + tr "v" + ", $this)")
            "SetTop" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.top = " + tr "v" + ", $this)")
            "SetRight" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.right = " + tr "v" + ", $this)")
            "SetBottom" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.bottom = " + tr "v" + ", $this)")
            "SetOrient" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.orient = " + tr "v" + ", $this)")
            "SetPadding" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.padding = " + tr "v" + ", $this)")
            "SetBackgroundColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.backgroundColor = " + tr "v" + ", $this)")
            "SetBorderColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderColor = " + tr "v" + ", $this)")
            "SetBorderWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderWidth = " + tr "v" + ", $this)")
            "SetColor" => (!| T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.color = " + tr "v" + ", $this)")
            "SetTextStyle" => BaseTextStyleWithRich?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textStyle = " + tr "v" + ", $this)")
            "SetFormatter" => (T<string> + (T<unit> ^-> T<unit>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.formatter = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "min" =@ (!? T<float>)
            "max" =@ (!? T<float>)
            "range" =@ (!? (!| T<float>))
            "calculable" =@ (!? T<bool>)
            "realtime" =@ (!? T<bool>)
            "inverse" =@ (!? T<bool>)
            "precision" =@ (!? T<float>)
            "itemWidth" =@ (!? T<float>)
            "itemHeight" =@ (!? T<float>)
            "align" =@ (!? (T<string>))
            "text" =@ (!? (!| T<string>))
            "textGap" =@ (!? T<float>)
            "show" =@ (!? T<bool>)
            "dimension" =@ (!? (T<string> + T<float>))
            "seriesIndex" =@ (!? (T<float> + (!| T<float>)))
            "hoverLink" =@ (!? T<bool>)
            "inRange" =@ (!? VisualMap_RangeObject)
            "outOfRange" =@ (!? VisualMap_RangeObject)
            "controller" =@ (!? T<obj>)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "left" =@ (!? (T<float> + T<string>))
            "top" =@ (!? (T<float> + T<string>))
            "right" =@ (!? (T<float> + T<string>))
            "bottom" =@ (!? (T<float> + T<string>))
            "orient" =@ (!? (T<string>))
            "padding" =@ (!? (T<float> + (!| T<float>)))
            "backgroundColor" =@ (!? T<string>)
            "borderColor" =@ (!? T<string>)
            "borderWidth" =@ (!? T<float>)
            "color" =@ (!? (!| T<string>))
            "textStyle" =@ (!? BaseTextStyleWithRich)
            "formatter" =@ (!? (T<string> + (T<unit> ^-> T<unit>)))
        ]
        |> ignore

    VisualMap_Piecewise
        |> WithSourceName "VisualMap_Piecewise"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetSplitNumber" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.splitNumber = " + tr "v" + ", $this)")
            "SetPieces" => (!| VisualMap_PiecesObject)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.pieces = " + tr "v" + ", $this)")
            "SetCategories" => (!| T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.categories = " + tr "v" + ", $this)")
            "SetMin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.min = " + tr "v" + ", $this)")
            "SetMax" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.max = " + tr "v" + ", $this)")
            "SetMinOpen" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.minOpen = " + tr "v" + ", $this)")
            "SetMaxOpen" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.maxOpen = " + tr "v" + ", $this)")
            "SetSelectedMode" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.selectedMode = " + tr "v" + ", $this)")
            "SetInverse" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.inverse = " + tr "v" + ", $this)")
            "SetPrecision" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.precision = " + tr "v" + ", $this)")
            "SetItemWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemWidth = " + tr "v" + ", $this)")
            "SetItemHeight" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemHeight = " + tr "v" + ", $this)")
            "SetAlign" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.align = " + tr "v" + ", $this)")
            "SetText" => (!| T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.text = " + tr "v" + ", $this)")
            "SetTextGap" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textGap = " + tr "v" + ", $this)")
            "SetShowLabel" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.showLabel = " + tr "v" + ", $this)")
            "SetItemGap" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemGap = " + tr "v" + ", $this)")
            "SetItemSymbol" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemSymbol = " + tr "v" + ", $this)")
            "SetShow" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.show = " + tr "v" + ", $this)")
            "SetDimension" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.dimension = " + tr "v" + ", $this)")
            "SetSeriesIndex" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.seriesIndex = " + tr "v" + ", $this)")
            "SetHoverLink" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.hoverLink = " + tr "v" + ", $this)")
            "SetInRange" => VisualMap_RangeObject?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.inRange = " + tr "v" + ", $this)")
            "SetOutOfRange" => VisualMap_RangeObject?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.outOfRange = " + tr "v" + ", $this)")
            "SetController" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.controller = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetLeft" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.left = " + tr "v" + ", $this)")
            "SetTop" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.top = " + tr "v" + ", $this)")
            "SetRight" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.right = " + tr "v" + ", $this)")
            "SetBottom" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.bottom = " + tr "v" + ", $this)")
            "SetOrient" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.orient = " + tr "v" + ", $this)")
            "SetPadding" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.padding = " + tr "v" + ", $this)")
            "SetBackgroundColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.backgroundColor = " + tr "v" + ", $this)")
            "SetBorderColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderColor = " + tr "v" + ", $this)")
            "SetBorderWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderWidth = " + tr "v" + ", $this)")
            "SetColor" => (!| T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.color = " + tr "v" + ", $this)")
            "SetTextStyle" => TextStyle?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textStyle = " + tr "v" + ", $this)")
            "SetFormatter" => (T<string> + (T<unit> ^-> T<unit>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.formatter = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "type" =@ (!? T<string>)
            "id" =@ (!? T<string>)
            "splitNumber" =@ (!? T<float>)
            "pieces" =@ (!? (!| VisualMap_PiecesObject))
            "categories" =@ (!? (!| T<string>))
            "min" =@ (!? T<float>)
            "max" =@ (!? T<float>)
            "minOpen" =@ (!? T<bool>)
            "maxOpen" =@ (!? T<bool>)
            "selectedMode" =@ (!? (T<string>))
            "inverse" =@ (!? T<bool>)
            "precision" =@ (!? T<float>)
            "itemWidth" =@ (!? T<float>)
            "itemHeight" =@ (!? T<float>)
            "align" =@ (!? (T<string>))
            "text" =@ (!? (!| T<string>))
            "textGap" =@ (!? (T<float> + (!| T<float>)))
            "showLabel" =@ (!? T<bool>)
            "itemGap" =@ (!? T<float>)
            "itemSymbol" =@ (!? T<string>)
            "show" =@ (!? T<bool>)
            "dimension" =@ (!? (T<string> + T<float>))
            "seriesIndex" =@ (!? (T<float> + (!| T<float>)))
            "hoverLink" =@ (!? T<bool>)
            "inRange" =@ (!? VisualMap_RangeObject)
            "outOfRange" =@ (!? VisualMap_RangeObject)
            "controller" =@ (!? T<obj>)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "left" =@ (!? (T<float> + T<string>))
            "top" =@ (!? (T<float> + T<string>))
            "right" =@ (!? (T<float> + T<string>))
            "bottom" =@ (!? (T<float> + T<string>))
            "orient" =@ (!? (T<string>))
            "padding" =@ (!? (T<float> + (!| T<float>)))
            "backgroundColor" =@ (!? T<string>)
            "borderColor" =@ (!? T<string>)
            "borderWidth" =@ (!? T<float>)
            "color" =@ (!? (!| T<string>))
            "textStyle" =@ (!? TextStyle)
            "formatter" =@ (!? (T<string> + (T<unit> ^-> T<unit>)))
        ]
        |> ignore

    VisualMap_RangeObject
        |> WithSourceName "VisualMap_RangeObject"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetSymbol" => (T<string> + (!| T<string>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbol = " + tr "v" + ", $this)")
            "SetSymbolSize" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolSize = " + tr "v" + ", $this)")
            "SetColor" => (T<string> + (!| T<string>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.color = " + tr "v" + ", $this)")
            "SetColorAlpha" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.colorAlpha = " + tr "v" + ", $this)")
            "SetOpacity" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.opacity = " + tr "v" + ", $this)")
            "SetColorLightness" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.colorLightness = " + tr "v" + ", $this)")
            "SetColorSaturation" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.colorSaturation = " + tr "v" + ", $this)")
            "SetColorHue" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.colorHue = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "symbol" =@ (!? (T<string> + (!| T<string>)))
            "symbolSize" =@ (!? (T<float> + (!| T<float>)))
            "color" =@ (!? (T<string> + (!| T<string>)))
            "colorAlpha" =@ (!? (T<float> + (!| T<float>)))
            "opacity" =@ (!? (T<float> + (!| T<float>)))
            "colorLightness" =@ (!? (T<float> + (!| T<float>)))
            "colorSaturation" =@ (!? (T<float> + (!| T<float>)))
            "colorHue" =@ (!? (T<float> + (!| T<float>)))
        ]
        |> ignore

    VisualMap_PiecesObject
        |> WithSourceName "VisualMap_PiecesObject"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetMin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.min = " + tr "v" + ", $this)")
            "SetMax" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.max = " + tr "v" + ", $this)")
            "SetLabel" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetValue" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.value = " + tr "v" + ", $this)")
            "SetColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.color = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "min" =@ (!? T<float>)
            "max" =@ (!? T<float>)
            "label" =@ (!? T<string>)
            "value" =@ (!? T<float>)
            "color" =@ (!? T<string>)
        ]
        |> ignore

    XAxis
        |> WithSourceName "XAxis"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetPosition" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.position = " + tr "v" + ", $this)")
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetOffset" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.offset = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetShow" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.show = " + tr "v" + ", $this)")
            "SetGridIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.gridIndex = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetNameLocation" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nameLocation = " + tr "v" + ", $this)")
            "SetNameTextStyle" => TextStyleWithRich?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nameTextStyle = " + tr "v" + ", $this)")
            "SetNameGap" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nameGap = " + tr "v" + ", $this)")
            "SetNameRotate" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nameRotate = " + tr "v" + ", $this)")
            "SetInverse" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.inverse = " + tr "v" + ", $this)")
            "SetBoundaryGap" => (T<bool> + (!| (T<string> + T<float>)))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.boundaryGap = " + tr "v" + ", $this)")
            "SetMin" => (T<float> + T<string> + (ChartExtremes?value ^-> T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.min = " + tr "v" + ", $this)")
            "SetMax" => (T<float> + T<string> + (ChartExtremes?value ^-> T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.max = " + tr "v" + ", $this)")
            "SetScale" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.scale = " + tr "v" + ", $this)")
            "SetSplitNumber" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.splitNumber = " + tr "v" + ", $this)")
            "SetMinInterval" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.minInterval = " + tr "v" + ", $this)")
            "SetInterval" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.interval = " + tr "v" + ", $this)")
            "SetLogBase" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.logBase = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetTriggerEvent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.triggerEvent = " + tr "v" + ", $this)")
            "SetAxisLine" => Line?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.axisLine = " + tr "v" + ", $this)")
            "SetAxisTick" => CartesianAxis_Tick?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.axisTick = " + tr "v" + ", $this)")
            "SetMinorTick" => CartesianAxis_MinorTick?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.minorTick = " + tr "v" + ", $this)")
            "SetAxisLabel" => CartesianAxis_Label?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.axisLabel = " + tr "v" + ", $this)")
            "SetSplitLine" => CartesianAxis_SplitLine?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.splitLine = " + tr "v" + ", $this)")
            "SetMinorSplitLine" => CartesianAxis_MinorSplitLine?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.minorSplitLine = " + tr "v" + ", $this)")
            "SetSplitArea" => CartesianAxis_SplitArea?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.splitArea = " + tr "v" + ", $this)")
            "SetData" => (!| (T<string> + T<float> + CartesianAxis_DataObject))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetAxisPointer" => CartesianAxis_Pointer?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.axisPointer = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "position" =@ (!? (T<string>))
            "type" =@ (!? T<string>)
            "offset" =@ (!? T<float>)
            "id" =@ (!? T<string>)
            "show" =@ (!? T<bool>)
            "gridIndex" =@ (!? T<float>)
            "name" =@ (!? T<string>)
            "nameLocation" =@ (!? (T<string>))
            "nameTextStyle" =@ (!? TextStyleWithRich)
            "nameGap" =@ (!? T<float>)
            "nameRotate" =@ (!? T<float>)
            "inverse" =@ (!? T<bool>)
            "boundaryGap" =@ (!? (T<bool> + (!| (T<string> + T<float>))))
            "min" =@ (!? (T<float> + T<string> + (ChartExtremes?value ^-> T<float>)))
            "max" =@ (!? (T<float> + T<string> + (ChartExtremes?value ^-> T<float>)))
            "scale" =@ (!? T<bool>)
            "splitNumber" =@ (!? T<float>)
            "minInterval" =@ (!? T<obj>)
            "interval" =@ (!? T<float>)
            "logBase" =@ (!? T<float>)
            "silent" =@ (!? T<bool>)
            "triggerEvent" =@ (!? T<bool>)
            "axisLine" =@ (!? Line)
            "axisTick" =@ (!? CartesianAxis_Tick)
            "minorTick" =@ (!? CartesianAxis_MinorTick)
            "axisLabel" =@ (!? CartesianAxis_Label)
            "splitLine" =@ (!? CartesianAxis_SplitLine)
            "minorSplitLine" =@ (!? CartesianAxis_MinorSplitLine)
            "splitArea" =@ (!? CartesianAxis_SplitArea)
            "data" =@ (!? (!| (T<string> + T<float> + CartesianAxis_DataObject)))
            "axisPointer" =@ (!? CartesianAxis_Pointer)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
        ]
        |> ignore

    YAxis
        |> WithSourceName "YAxis"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetPosition" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.position = " + tr "v" + ", $this)")
            "SetType" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetOffset" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.offset = " + tr "v" + ", $this)")
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetShow" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.show = " + tr "v" + ", $this)")
            "SetGridIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.gridIndex = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetNameLocation" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nameLocation = " + tr "v" + ", $this)")
            "SetNameTextStyle" => TextStyleWithRich?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nameTextStyle = " + tr "v" + ", $this)")
            "SetNameGap" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nameGap = " + tr "v" + ", $this)")
            "SetNameRotate" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nameRotate = " + tr "v" + ", $this)")
            "SetInverse" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.inverse = " + tr "v" + ", $this)")
            "SetBoundaryGap" => (T<bool> + (!| (T<string> + T<float>)))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.boundaryGap = " + tr "v" + ", $this)")
            "SetMin" => (T<float> + T<string> + (ChartExtremes?value ^-> T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.min = " + tr "v" + ", $this)")
            "SetMax" => (T<float> + T<string> + (ChartExtremes?value ^-> T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.max = " + tr "v" + ", $this)")
            "SetScale" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.scale = " + tr "v" + ", $this)")
            "SetSplitNumber" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.splitNumber = " + tr "v" + ", $this)")
            "SetMinInterval" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.minInterval = " + tr "v" + ", $this)")
            "SetInterval" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.interval = " + tr "v" + ", $this)")
            "SetLogBase" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.logBase = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetTriggerEvent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.triggerEvent = " + tr "v" + ", $this)")
            "SetAxisLine" => Line?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.axisLine = " + tr "v" + ", $this)")
            "SetAxisTick" => CartesianAxis_Tick?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.axisTick = " + tr "v" + ", $this)")
            "SetMinorTick" => CartesianAxis_MinorTick?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.minorTick = " + tr "v" + ", $this)")
            "SetAxisLabel" => CartesianAxis_Label?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.axisLabel = " + tr "v" + ", $this)")
            "SetSplitLine" => CartesianAxis_SplitLine?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.splitLine = " + tr "v" + ", $this)")
            "SetMinorSplitLine" => CartesianAxis_MinorSplitLine?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.minorSplitLine = " + tr "v" + ", $this)")
            "SetSplitArea" => CartesianAxis_SplitArea?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.splitArea = " + tr "v" + ", $this)")
            "SetData" => (!| (T<string> + T<float> + CartesianAxis_DataObject))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetAxisPointer" => CartesianAxis_Pointer?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.axisPointer = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "position" =@ (!? (T<string>))
            "type" =@ (!? T<string>)
            "offset" =@ (!? T<float>)
            "id" =@ (!? T<string>)
            "show" =@ (!? T<bool>)
            "gridIndex" =@ (!? T<float>)
            "name" =@ (!? T<string>)
            "nameLocation" =@ (!? (T<string>))
            "nameTextStyle" =@ (!? TextStyleWithRich)
            "nameGap" =@ (!? T<float>)
            "nameRotate" =@ (!? T<float>)
            "inverse" =@ (!? T<bool>)
            "boundaryGap" =@ (!? (T<bool> + (!| (T<string> + T<float>))))
            "min" =@ (!? (T<float> + T<string> + (ChartExtremes?value ^-> T<float>)))
            "max" =@ (!? (T<float> + T<string> + (ChartExtremes?value ^-> T<float>)))
            "scale" =@ (!? T<bool>)
            "splitNumber" =@ (!? T<float>)
            "minInterval" =@ (!? T<obj>)
            "interval" =@ (!? T<float>)
            "logBase" =@ (!? T<float>)
            "silent" =@ (!? T<bool>)
            "triggerEvent" =@ (!? T<bool>)
            "axisLine" =@ (!? Line)
            "axisTick" =@ (!? CartesianAxis_Tick)
            "minorTick" =@ (!? CartesianAxis_MinorTick)
            "axisLabel" =@ (!? CartesianAxis_Label)
            "splitLine" =@ (!? CartesianAxis_SplitLine)
            "minorSplitLine" =@ (!? CartesianAxis_MinorSplitLine)
            "splitArea" =@ (!? CartesianAxis_SplitArea)
            "data" =@ (!? (!| (T<string> + T<float> + CartesianAxis_DataObject)))
            "axisPointer" =@ (!? CartesianAxis_Pointer)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
        ]
        |> ignore

    MapObj
        |> WithSourceName "MapObj"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetGeoJson" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.geoJson = " + tr "v" + ", $this)")
            "SetSpecialAreas" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.specialAreas = " + tr "v" + ", $this)")
        ]
        |+> Pattern.RequiredFields [
            "geoJson", Helpers.NormaliseType(T<obj>)
            "specialAreas", Helpers.NormaliseType(T<obj>)
        ]
        |> ignore

    Graphic
        |> WithSourceName "Graphic"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetLinearGradient" => zrender_LinearGradient?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.LinearGradient = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "clipPointsByRect" => (!| (!| T<float>))?points * ERectangle?rect ^-> (!| (!| T<float>))
            "clipRectByRect" => ERectangle?targetRect * ERectangle?rect ^-> ERectangle
        ]
        |+> Pattern.RequiredFields [
            "LinearGradient", Helpers.NormaliseType(zrender_LinearGradient)
        ]
        |> ignore

    ECharts
        |> WithSourceName "ECharts"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetGroup" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.group = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "setOption" => (EChartOption + EChartsResponsiveOption)?option * (!? T<bool>)?notMerge * (!? T<bool>)?lazyUpdate ^-> T<unit>
            "getWidth" => T<unit> ^-> T<float>
            "getHeight" => T<unit> ^-> T<float>
            "getDom" => T<unit> ^-> (T<HTMLElement> + T<HTMLElement>)
            "getOption" => T<unit> ^-> EChartOption
            "resize" => (!? EChartsResizeOption)?opts ^-> T<unit>
            "dispatchAction" => T<obj>?payload ^-> T<unit>
            "on" => T<string>?eventName * (T<unit> ^-> T<unit>)?handler * (!? T<obj>)?context ^-> T<unit>
            "on" => T<string>?eventName * (T<string> + T<obj>)?query * (T<unit> ^-> T<unit>)?handler * (!? T<obj>)?context ^-> T<unit>
            "off" => T<string>?eventName * (!? (T<unit> ^-> T<unit>))?handler ^-> T<unit>
            "convertToPixel" => EChartsConvertFinder?finder * (T<string> + (!| T<obj>))?value ^-> (T<string> + (!| T<obj>))
            "convertFromPixel" => EChartsConvertFinder?finder * ((!| T<obj>) + T<string>)?value ^-> ((!| T<obj>) + T<string>)
            "containPixel" => EChartsConvertFinder?finder * (!| T<obj>)?value ^-> T<bool>
            "showLoading" => (!? T<string>)?``type`` * (!? EChartsLoadingOption)?opts ^-> T<unit>
            "hideLoading" => T<unit> ^-> T<unit>
            "getDataURL" => T<obj>?opts ^-> T<string>
            "getConnectedDataURL" => T<obj>?opts ^-> T<string>
            "appendData" => T<obj>?opts ^-> T<unit>
            "clear" => T<unit> ^-> T<unit>
            "isDisposed" => T<unit> ^-> T<bool>
            "dispose" => T<unit> ^-> T<unit>
        ]
        |+> Pattern.RequiredFields [
            "group", Helpers.NormaliseType(T<string>)
        ]
        |> ignore

    EChartsConvertFinder
        |> WithSourceName "EChartsConvertFinder"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetSeriesIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.seriesIndex = " + tr "v" + ", $this)")
            "SetSeriesId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.seriesId = " + tr "v" + ", $this)")
            "SetSeriesName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.seriesName = " + tr "v" + ", $this)")
            "SetGeoIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.geoIndex = " + tr "v" + ", $this)")
            "SetGeoId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.geoId = " + tr "v" + ", $this)")
            "SetGeoName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.geoName = " + tr "v" + ", $this)")
            "SetXAxisIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.xAxisIndex = " + tr "v" + ", $this)")
            "SetXAxisId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.xAxisId = " + tr "v" + ", $this)")
            "SetXAxisName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.xAxisName = " + tr "v" + ", $this)")
            "SetYAxisIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.yAxisIndex = " + tr "v" + ", $this)")
            "SetYAxisId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.yAxisId = " + tr "v" + ", $this)")
            "SetYAxisName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.yAxisName = " + tr "v" + ", $this)")
            "SetGridIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.gridIndex = " + tr "v" + ", $this)")
            "SetGridId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.gridId = " + tr "v" + ", $this)")
            "SetGridName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.gridName = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "seriesIndex" =@ (!? T<float>)
            "seriesId" =@ (!? T<string>)
            "seriesName" =@ (!? T<string>)
            "geoIndex" =@ (!? T<float>)
            "geoId" =@ (!? T<string>)
            "geoName" =@ (!? T<string>)
            "xAxisIndex" =@ (!? T<float>)
            "xAxisId" =@ (!? T<string>)
            "xAxisName" =@ (!? T<string>)
            "yAxisIndex" =@ (!? T<float>)
            "yAxisId" =@ (!? T<string>)
            "yAxisName" =@ (!? T<string>)
            "gridIndex" =@ (!? T<float>)
            "gridId" =@ (!? T<string>)
            "gridName" =@ (!? T<string>)
        ]
        |> ignore

    ERectangle
        |> WithSourceName "ERectangle"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetX" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.x = " + tr "v" + ", $this)")
            "SetY" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.y = " + tr "v" + ", $this)")
            "SetWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.width = " + tr "v" + ", $this)")
            "SetHeight" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.height = " + tr "v" + ", $this)")
        ]
        |+> Pattern.RequiredFields [
            "x", Helpers.NormaliseType(T<float>)
            "y", Helpers.NormaliseType(T<float>)
            "width", Helpers.NormaliseType(T<float>)
            "height", Helpers.NormaliseType(T<float>)
        ]
        |> ignore

    EChartOption
        |> WithSourceName "EChartOption"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetTitle" => (EChartTitleOption + (!| EChartTitleOption))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.title = " + tr "v" + ", $this)")
            "SetLegend" => Legend?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.legend = " + tr "v" + ", $this)")
            "SetGrid" => (Grid + (!| Grid))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.grid = " + tr "v" + ", $this)")
            "SetXAxis" => (XAxis + (!| XAxis))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.xAxis = " + tr "v" + ", $this)")
            "SetYAxis" => (YAxis + (!| YAxis))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.yAxis = " + tr "v" + ", $this)")
            "SetPolar" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.polar = " + tr "v" + ", $this)")
            "SetRadiusAxis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.radiusAxis = " + tr "v" + ", $this)")
            "SetAngleAxis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.angleAxis = " + tr "v" + ", $this)")
            "SetRadar" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.radar = " + tr "v" + ", $this)")
            "SetDataZoom" => (!| (DataZoom_Inside + DataZoom_Slider))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.dataZoom = " + tr "v" + ", $this)")
            "SetVisualMap" => (!| (VisualMap_Continuous + VisualMap_Piecewise))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.visualMap = " + tr "v" + ", $this)")
            "SetTooltip" => Tooltip?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.tooltip = " + tr "v" + ", $this)")
            "SetAxisPointer" => AxisPointer?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.axisPointer = " + tr "v" + ", $this)")
            "SetToolbox" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.toolbox = " + tr "v" + ", $this)")
            "SetBrush" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.brush = " + tr "v" + ", $this)")
            "SetGeo" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.geo = " + tr "v" + ", $this)")
            "SetParallel" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.parallel = " + tr "v" + ", $this)")
            "SetParallelAxis" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.parallelAxis = " + tr "v" + ", $this)")
            "SetSingleAxis" => (SingleAxis + (!| SingleAxis))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.singleAxis = " + tr "v" + ", $this)")
            "SetTimeline" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.timeline = " + tr "v" + ", $this)")
            "SetGraphic" => (T<obj> + (!| T<obj>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.graphic = " + tr "v" + ", $this)")
            "SetCalendar" => (Calendar + (!| Calendar))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.calendar = " + tr "v" + ", $this)")
            "SetDataset" => (Dataset + (!| Dataset))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.dataset = " + tr "v" + ", $this)")
            "SetAria" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.aria = " + tr "v" + ", $this)")
            "SetSeries" => (!| (SeriesLine + SeriesBar + SeriesPie + SeriesScatter + SeriesEffectScatter + SeriesRadar + SeriesTree + SeriesTreemap + SeriesSunburst + SeriesBoxplot + SeriesCandlestick + SeriesHeatmap + SeriesMap + SeriesParallel + SeriesLines + SeriesGraph + SeriesSankey + SeriesFunnel + SeriesGauge + SeriesPictorialBar + SeriesThemeRiver + SeriesCustom))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.series = " + tr "v" + ", $this)")
            "SetColor" => (!| T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.color = " + tr "v" + ", $this)")
            "SetBackgroundColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.backgroundColor = " + tr "v" + ", $this)")
            "SetTextStyle" => BaseTextStyle?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textStyle = " + tr "v" + ", $this)")
            "SetAnimation" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animation = " + tr "v" + ", $this)")
            "SetAnimationThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationThreshold = " + tr "v" + ", $this)")
            "SetAnimationDuration" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDuration = " + tr "v" + ", $this)")
            "SetAnimationEasing" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasing = " + tr "v" + ", $this)")
            "SetAnimationDelay" => (T<float> + (T<unit> ^-> T<unit>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelay = " + tr "v" + ", $this)")
            "SetAnimationDurationUpdate" => (T<float> + (T<unit> ^-> T<unit>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDurationUpdate = " + tr "v" + ", $this)")
            "SetAnimationEasingUpdate" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationEasingUpdate = " + tr "v" + ", $this)")
            "SetAnimationDelayUpdate" => (T<float> + (T<unit> ^-> T<unit>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.animationDelayUpdate = " + tr "v" + ", $this)")
            "SetProgressive" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.progressive = " + tr "v" + ", $this)")
            "SetProgressiveThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.progressiveThreshold = " + tr "v" + ", $this)")
            "SetBlendMode" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.blendMode = " + tr "v" + ", $this)")
            "SetHoverLayerThreshold" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.hoverLayerThreshold = " + tr "v" + ", $this)")
            "SetUseUTC" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.useUTC = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "title" =@ (!? (EChartTitleOption + (!| EChartTitleOption)))
            "legend" =@ (!? Legend)
            "grid" =@ (!? (Grid + (!| Grid)))
            "xAxis" =@ (!? (XAxis + (!| XAxis)))
            "yAxis" =@ (!? (YAxis + (!| YAxis)))
            "polar" =@ (!? T<obj>)
            "radiusAxis" =@ (!? T<obj>)
            "angleAxis" =@ (!? T<obj>)
            "radar" =@ (!? T<obj>)
            "dataZoom" =@ (!? (!| (DataZoom_Inside + DataZoom_Slider)))
            "visualMap" =@ (!? (!| (VisualMap_Continuous + VisualMap_Piecewise)))
            "tooltip" =@ (!? Tooltip)
            "axisPointer" =@ (!? AxisPointer)
            "toolbox" =@ (!? T<obj>)
            "brush" =@ (!? T<obj>)
            "geo" =@ (!? T<obj>)
            "parallel" =@ (!? T<obj>)
            "parallelAxis" =@ (!? T<obj>)
            "singleAxis" =@ (!? (SingleAxis + (!| SingleAxis)))
            "timeline" =@ (!? T<obj>)
            "graphic" =@ (!? (T<obj> + (!| T<obj>)))
            "calendar" =@ (!? (Calendar + (!| Calendar)))
            "dataset" =@ (!? (Dataset + (!| Dataset)))
            "aria" =@ (!? T<obj>)
            "series" =@ (!? (!| (SeriesLine + SeriesBar + SeriesPie + SeriesScatter + SeriesEffectScatter + SeriesRadar + SeriesTree + SeriesTreemap + SeriesSunburst + SeriesBoxplot + SeriesCandlestick + SeriesHeatmap + SeriesMap + SeriesParallel + SeriesLines + SeriesGraph + SeriesSankey + SeriesFunnel + SeriesGauge + SeriesPictorialBar + SeriesThemeRiver + SeriesCustom)))
            "color" =@ (!? (!| T<string>))
            "backgroundColor" =@ (!? T<string>)
            "textStyle" =@ (!? BaseTextStyle)
            "animation" =@ (!? T<bool>)
            "animationThreshold" =@ (!? T<float>)
            "animationDuration" =@ (!? T<float>)
            "animationEasing" =@ (!? T<string>)
            "animationDelay" =@ (!? (T<float> + (T<unit> ^-> T<unit>)))
            "animationDurationUpdate" =@ (!? (T<float> + (T<unit> ^-> T<unit>)))
            "animationEasingUpdate" =@ (!? T<string>)
            "animationDelayUpdate" =@ (!? (T<float> + (T<unit> ^-> T<unit>)))
            "progressive" =@ (!? T<float>)
            "progressiveThreshold" =@ (!? T<float>)
            "blendMode" =@ (!? T<string>)
            "hoverLayerThreshold" =@ (!? T<float>)
            "useUTC" =@ (!? T<bool>)
        ]
        |> ignore

    EChartsResponsiveOption
        |> WithSourceName "EChartsResponsiveOption"
        |+> Static [ ObjectConstructor ((!? EChartOption)?baseOption * (!? (!| T<obj>))?media) ]
        |+> Instance [
            "baseOption" =@ (!? EChartOption)
            "media" =@ (!? (!| T<obj>))
        ]
        |> ignore

    EChartsOptionConfig
        |> WithSourceName "EChartsOptionConfig"
        |+> Static [ ObjectConstructor ((!? T<bool>)?notMerge * (!? T<bool>)?lazyUpdate * (!? T<bool>)?silent) ]
        |+> Instance [
            "notMerge" =@ (!? T<bool>)
            "lazyUpdate" =@ (!? T<bool>)
            "silent" =@ (!? T<bool>)
        ]
        |> ignore

    EChartsResizeOption
        |> WithSourceName "EChartsResizeOption"
        |+> Static [ ObjectConstructor ((!? (T<float> + T<string>))?width * (!? (T<float> + T<string>))?height * (!? T<bool>)?silent) ]
        |+> Instance [
            "width" =@ (!? (T<float> + T<string>))
            "height" =@ (!? (T<float> + T<string>))
            "silent" =@ (!? T<bool>)
        ]
        |> ignore

    EChartTitleOption
        |> WithSourceName "EChartTitleOption"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetShow" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.show = " + tr "v" + ", $this)")
            "SetText" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.text = " + tr "v" + ", $this)")
            "SetLink" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.link = " + tr "v" + ", $this)")
            "SetTarget" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.target = " + tr "v" + ", $this)")
            "SetTextStyle" => TextStyleWithRich?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textStyle = " + tr "v" + ", $this)")
            "SetSubtext" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.subtext = " + tr "v" + ", $this)")
            "SetSublink" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.sublink = " + tr "v" + ", $this)")
            "SetSubtarget" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.subtarget = " + tr "v" + ", $this)")
            "SetSubtextStyle" => TextStyleWithRich?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.subtextStyle = " + tr "v" + ", $this)")
            "SetTextAlign" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textAlign = " + tr "v" + ", $this)")
            "SetTextVerticalAlign" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textVerticalAlign = " + tr "v" + ", $this)")
            "SetTriggerEvent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.triggerEvent = " + tr "v" + ", $this)")
            "SetPadding" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.padding = " + tr "v" + ", $this)")
            "SetItemGap" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.itemGap = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetLeft" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.left = " + tr "v" + ", $this)")
            "SetTop" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.top = " + tr "v" + ", $this)")
            "SetRight" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.right = " + tr "v" + ", $this)")
            "SetBottom" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.bottom = " + tr "v" + ", $this)")
            "SetBackgroundColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.backgroundColor = " + tr "v" + ", $this)")
            "SetBorderColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderColor = " + tr "v" + ", $this)")
            "SetBorderWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderWidth = " + tr "v" + ", $this)")
            "SetBorderRadius" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderRadius = " + tr "v" + ", $this)")
            "SetShadowBlur" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowBlur = " + tr "v" + ", $this)")
            "SetShadowColor" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowColor = " + tr "v" + ", $this)")
            "SetShadowOffsetX" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowOffsetX = " + tr "v" + ", $this)")
            "SetShadowOffsetY" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowOffsetY = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "show" =@ (!? T<bool>)
            "text" =@ (!? T<string>)
            "link" =@ (!? T<string>)
            "target" =@ (!? T<string>)
            "textStyle" =@ (!? TextStyleWithRich)
            "subtext" =@ (!? T<string>)
            "sublink" =@ (!? T<string>)
            "subtarget" =@ (!? T<string>)
            "subtextStyle" =@ (!? TextStyleWithRich)
            "textAlign" =@ (!? T<string>)
            "textVerticalAlign" =@ (!? T<string>)
            "triggerEvent" =@ (!? T<bool>)
            "padding" =@ (!? (T<float> + (!| T<float>)))
            "itemGap" =@ (!? T<float>)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
            "left" =@ (!? (T<string> + T<float>))
            "top" =@ (!? (T<string> + T<float>))
            "right" =@ (!? (T<string> + T<float>))
            "bottom" =@ (!? (T<string> + T<float>))
            "backgroundColor" =@ (!? T<string>)
            "borderColor" =@ (!? T<string>)
            "borderWidth" =@ (!? T<float>)
            "borderRadius" =@ (!? (T<float> + (!| T<float>)))
            "shadowBlur" =@ (!? T<float>)
            "shadowColor" =@ (!? T<float>)
            "shadowOffsetX" =@ (!? T<float>)
            "shadowOffsetY" =@ (!? T<float>)
        ]
        |> ignore

    EChartsLoadingOption
        |> WithSourceName "EChartsLoadingOption"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetText" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.text = " + tr "v" + ", $this)")
            "SetColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.color = " + tr "v" + ", $this)")
            "SetTextColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textColor = " + tr "v" + ", $this)")
            "SetMaskColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.maskColor = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetFontSize" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontSize = " + tr "v" + ", $this)")
            "SetShowSpinner" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.showSpinner = " + tr "v" + ", $this)")
            "SetSpinnerRadius" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.spinnerRadius = " + tr "v" + ", $this)")
            "SetLineWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineWidth = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "text" =@ (!? T<string>)
            "color" =@ (!? T<string>)
            "textColor" =@ (!? T<string>)
            "maskColor" =@ (!? T<string>)
            "zlevel" =@ (!? T<float>)
            "fontSize" =@ (!? T<float>)
            "showSpinner" =@ (!? T<bool>)
            "spinnerRadius" =@ (!? T<float>)
            "lineWidth" =@ (!? T<float>)
        ]
        |> ignore

    Line
        |> WithSourceName "Line"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetShow" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.show = " + tr "v" + ", $this)")
            "SetOnZero" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.onZero = " + tr "v" + ", $this)")
            "SetOnZeroAxisIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.onZeroAxisIndex = " + tr "v" + ", $this)")
            "SetSymbol" => (T<string> + (!| T<string>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbol = " + tr "v" + ", $this)")
            "SetSymbolSize" => (!| T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolSize = " + tr "v" + ", $this)")
            "SetSymbolOffset" => (!| T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.symbolOffset = " + tr "v" + ", $this)")
            "SetLineStyle" => LineStyle?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineStyle = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "show" =@ (!? T<bool>)
            "onZero" =@ (!? T<bool>)
            "onZeroAxisIndex" =@ (!? T<float>)
            "symbol" =@ (!? (T<string> + (!| T<string>)))
            "symbolSize" =@ (!? (!| T<float>))
            "symbolOffset" =@ (!? (!| T<float>))
            "lineStyle" =@ (!? LineStyle)
        ]
        |> ignore

    ChartExtremes
        |> WithSourceName "ChartExtremes"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetMin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.min = " + tr "v" + ", $this)")
            "SetMax" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.max = " + tr "v" + ", $this)")
        ]
        |+> Pattern.RequiredFields [
            "min", Helpers.NormaliseType(T<float>)
            "max", Helpers.NormaliseType(T<float>)
        ]
        |> ignore

    CartesianAxis
        |> WithSourceName "CartesianAxis"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetId" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.id = " + tr "v" + ", $this)")
            "SetShow" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.show = " + tr "v" + ", $this)")
            "SetGridIndex" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.gridIndex = " + tr "v" + ", $this)")
            "SetOffset" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.offset = " + tr "v" + ", $this)")
            "SetName" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.name = " + tr "v" + ", $this)")
            "SetNameLocation" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nameLocation = " + tr "v" + ", $this)")
            "SetNameTextStyle" => TextStyleWithRich?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nameTextStyle = " + tr "v" + ", $this)")
            "SetNameGap" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nameGap = " + tr "v" + ", $this)")
            "SetNameRotate" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.nameRotate = " + tr "v" + ", $this)")
            "SetInverse" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.inverse = " + tr "v" + ", $this)")
            "SetBoundaryGap" => (T<bool> + (!| (T<string> + T<float>)))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.boundaryGap = " + tr "v" + ", $this)")
            "SetMin" => (T<float> + T<string> + (ChartExtremes?value ^-> T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.min = " + tr "v" + ", $this)")
            "SetMax" => (T<float> + T<string> + (ChartExtremes?value ^-> T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.max = " + tr "v" + ", $this)")
            "SetScale" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.scale = " + tr "v" + ", $this)")
            "SetSplitNumber" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.splitNumber = " + tr "v" + ", $this)")
            "SetMinInterval" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.minInterval = " + tr "v" + ", $this)")
            "SetInterval" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.interval = " + tr "v" + ", $this)")
            "SetLogBase" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.logBase = " + tr "v" + ", $this)")
            "SetSilent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.silent = " + tr "v" + ", $this)")
            "SetTriggerEvent" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.triggerEvent = " + tr "v" + ", $this)")
            "SetAxisLine" => Line?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.axisLine = " + tr "v" + ", $this)")
            "SetAxisTick" => CartesianAxis_Tick?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.axisTick = " + tr "v" + ", $this)")
            "SetMinorTick" => CartesianAxis_MinorTick?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.minorTick = " + tr "v" + ", $this)")
            "SetAxisLabel" => CartesianAxis_Label?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.axisLabel = " + tr "v" + ", $this)")
            "SetSplitLine" => CartesianAxis_SplitLine?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.splitLine = " + tr "v" + ", $this)")
            "SetMinorSplitLine" => CartesianAxis_MinorSplitLine?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.minorSplitLine = " + tr "v" + ", $this)")
            "SetSplitArea" => CartesianAxis_SplitArea?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.splitArea = " + tr "v" + ", $this)")
            "SetData" => (!| (T<string> + T<float> + CartesianAxis_DataObject))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.data = " + tr "v" + ", $this)")
            "SetAxisPointer" => CartesianAxis_Pointer?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.axisPointer = " + tr "v" + ", $this)")
            "SetZlevel" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.zlevel = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "id" =@ (!? T<string>)
            "show" =@ (!? T<bool>)
            "gridIndex" =@ (!? T<float>)
            "offset" =@ (!? T<float>)
            "name" =@ (!? T<string>)
            "nameLocation" =@ (!? (T<string>))
            "nameTextStyle" =@ (!? TextStyleWithRich)
            "nameGap" =@ (!? T<float>)
            "nameRotate" =@ (!? T<float>)
            "inverse" =@ (!? T<bool>)
            "boundaryGap" =@ (!? (T<bool> + (!| (T<string> + T<float>))))
            "min" =@ (!? (T<float> + T<string> + (ChartExtremes?value ^-> T<float>)))
            "max" =@ (!? (T<float> + T<string> + (ChartExtremes?value ^-> T<float>)))
            "scale" =@ (!? T<bool>)
            "splitNumber" =@ (!? T<float>)
            "minInterval" =@ (!? T<obj>)
            "interval" =@ (!? T<float>)
            "logBase" =@ (!? T<float>)
            "silent" =@ (!? T<bool>)
            "triggerEvent" =@ (!? T<bool>)
            "axisLine" =@ (!? Line)
            "axisTick" =@ (!? CartesianAxis_Tick)
            "minorTick" =@ (!? CartesianAxis_MinorTick)
            "axisLabel" =@ (!? CartesianAxis_Label)
            "splitLine" =@ (!? CartesianAxis_SplitLine)
            "minorSplitLine" =@ (!? CartesianAxis_MinorSplitLine)
            "splitArea" =@ (!? CartesianAxis_SplitArea)
            "data" =@ (!? (!| (T<string> + T<float> + CartesianAxis_DataObject)))
            "axisPointer" =@ (!? CartesianAxis_Pointer)
            "zlevel" =@ (!? T<float>)
            "z" =@ (!? T<float>)
        ]
        |> ignore

    CartesianAxis_Tick
        |> WithSourceName "CartesianAxis_Tick"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetShow" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.show = " + tr "v" + ", $this)")
            "SetAlignWithLabel" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.alignWithLabel = " + tr "v" + ", $this)")
            "SetInterval" => (T<float> + (T<float>?index * T<string>?value ^-> T<bool>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.interval = " + tr "v" + ", $this)")
            "SetCustomValues" => (!| (T<float> + T<string> + T<Date>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.customValues = " + tr "v" + ", $this)")
            "SetInside" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.inside = " + tr "v" + ", $this)")
            "SetLength" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.length = " + tr "v" + ", $this)")
            "SetLineStyle" => LineStyle?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineStyle = " + tr "v" + ", $this)")
        ]
        |+> Pattern.RequiredFields [
            "customValues", Helpers.NormaliseType((!| (T<float> + T<string> + T<Date>)))
        ]
        |+> Instance [
            "show" =@ (!? T<bool>)
            "alignWithLabel" =@ (!? T<bool>)
            "interval" =@ (!? (T<float> + (T<float>?index * T<string>?value ^-> T<bool>)))
            "inside" =@ (!? T<bool>)
            "length" =@ (!? T<float>)
            "lineStyle" =@ (!? LineStyle)
        ]
        |> ignore

    CartesianAxis_MinorTick
        |> WithSourceName "CartesianAxis_MinorTick"
        |+> Static [ ObjectConstructor ((!? T<bool>)?show * (!? T<float>)?splitNumber * (!? T<float>)?length * (!? LineStyle)?lineStyle) ]
        |+> Instance [
            "show" =@ (!? T<bool>)
            "splitNumber" =@ (!? T<float>)
            "length" =@ (!? T<float>)
            "lineStyle" =@ (!? LineStyle)
        ]
        |> ignore

    CartesianAxis_Label
        |> WithSourceName "CartesianAxis_Label"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetShow" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.show = " + tr "v" + ", $this)")
            "SetInterval" => (T<float> + (T<float>?index * T<string>?value ^-> T<bool>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.interval = " + tr "v" + ", $this)")
            "SetCustomValues" => (!| (T<float> + T<string> + T<Date>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.customValues = " + tr "v" + ", $this)")
            "SetInside" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.inside = " + tr "v" + ", $this)")
            "SetRotate" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.rotate = " + tr "v" + ", $this)")
            "SetMargin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.margin = " + tr "v" + ", $this)")
            "SetFormatter" => (T<string> + (T<string>?value * T<float>?index ^-> T<string>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.formatter = " + tr "v" + ", $this)")
            "SetShowMinLabel" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.showMinLabel = " + tr "v" + ", $this)")
            "SetShowMaxLabel" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.showMaxLabel = " + tr "v" + ", $this)")
            "SetRich" => RichStyle?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.rich = " + tr "v" + ", $this)")
            "SetAlign" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.align = " + tr "v" + ", $this)")
            "SetVerticalAlign" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.verticalAlign = " + tr "v" + ", $this)")
            "SetBackgroundColor" => (T<string> + T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.backgroundColor = " + tr "v" + ", $this)")
            "SetBorderColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderColor = " + tr "v" + ", $this)")
            "SetBorderWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderWidth = " + tr "v" + ", $this)")
            "SetBorderRadius" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderRadius = " + tr "v" + ", $this)")
            "SetPadding" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.padding = " + tr "v" + ", $this)")
            "SetShadowColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowColor = " + tr "v" + ", $this)")
            "SetShadowBlur" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowBlur = " + tr "v" + ", $this)")
            "SetShadowOffsetX" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowOffsetX = " + tr "v" + ", $this)")
            "SetShadowOffsetY" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowOffsetY = " + tr "v" + ", $this)")
            "SetColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.color = " + tr "v" + ", $this)")
            "SetFontStyle" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontStyle = " + tr "v" + ", $this)")
            "SetFontWeight" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontWeight = " + tr "v" + ", $this)")
            "SetFontFamily" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontFamily = " + tr "v" + ", $this)")
            "SetFontSize" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontSize = " + tr "v" + ", $this)")
            "SetLineHeight" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineHeight = " + tr "v" + ", $this)")
            "SetWidth" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.width = " + tr "v" + ", $this)")
            "SetHeight" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.height = " + tr "v" + ", $this)")
            "SetTextBorderColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textBorderColor = " + tr "v" + ", $this)")
            "SetTextBorderWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textBorderWidth = " + tr "v" + ", $this)")
            "SetTextShadowColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowColor = " + tr "v" + ", $this)")
            "SetTextShadowBlur" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowBlur = " + tr "v" + ", $this)")
            "SetTextShadowOffsetX" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowOffsetX = " + tr "v" + ", $this)")
            "SetTextShadowOffsetY" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowOffsetY = " + tr "v" + ", $this)")
        ]
        |+> Pattern.RequiredFields [
            "customValues", Helpers.NormaliseType((!| (T<float> + T<string> + T<Date>)))
        ]
        |+> Instance [
            "show" =@ (!? T<bool>)
            "interval" =@ (!? (T<float> + (T<float>?index * T<string>?value ^-> T<bool>)))
            "inside" =@ (!? T<bool>)
            "rotate" =@ (!? T<float>)
            "margin" =@ (!? T<float>)
            "formatter" =@ (!? (T<string> + (T<string>?value * T<float>?index ^-> T<string>)))
            "showMinLabel" =@ (!? T<bool>)
            "showMaxLabel" =@ (!? T<bool>)
            "rich" =@ (!? RichStyle)
            "align" =@ (!? T<string>)
            "verticalAlign" =@ (!? T<string>)
            "backgroundColor" =@ (!? (T<string> + T<obj>))
            "borderColor" =@ (!? T<string>)
            "borderWidth" =@ (!? T<float>)
            "borderRadius" =@ (!? T<float>)
            "padding" =@ (!? (T<float> + (!| T<float>)))
            "shadowColor" =@ (!? T<string>)
            "shadowBlur" =@ (!? T<float>)
            "shadowOffsetX" =@ (!? T<float>)
            "shadowOffsetY" =@ (!? T<float>)
            "color" =@ (!? T<string>)
            "fontStyle" =@ (!? (T<string>))
            "fontWeight" =@ (!? (T<string> + T<float>))
            "fontFamily" =@ (!? T<string>)
            "fontSize" =@ (!? T<float>)
            "lineHeight" =@ (!? T<float>)
            "width" =@ (!? (T<float> + T<string>))
            "height" =@ (!? (T<float> + T<string>))
            "textBorderColor" =@ (!? T<string>)
            "textBorderWidth" =@ (!? T<float>)
            "textShadowColor" =@ (!? T<string>)
            "textShadowBlur" =@ (!? T<float>)
            "textShadowOffsetX" =@ (!? T<float>)
            "textShadowOffsetY" =@ (!? T<float>)
        ]
        |> ignore

    CartesianAxis_SplitLine
        |> WithSourceName "CartesianAxis_SplitLine"
        |+> Static [ ObjectConstructor ((!? T<bool>)?show * (!? (T<float> + (T<float>?index * T<string>?value ^-> T<bool>)))?interval * (!? LineStyle)?lineStyle) ]
        |+> Instance [
            "show" =@ (!? T<bool>)
            "interval" =@ (!? (T<float> + (T<float>?index * T<string>?value ^-> T<bool>)))
            "lineStyle" =@ (!? LineStyle)
        ]
        |> ignore

    CartesianAxis_MinorSplitLine
        |> WithSourceName "CartesianAxis_MinorSplitLine"
        |+> Static [ ObjectConstructor ((!? T<bool>)?show * (!? LineStyle)?lineStyle) ]
        |+> Instance [
            "show" =@ (!? T<bool>)
            "lineStyle" =@ (!? LineStyle)
        ]
        |> ignore

    CartesianAxis_SplitArea
        |> WithSourceName "CartesianAxis_SplitArea"
        |+> Static [ ObjectConstructor ((!? (T<float> + (T<float>?index * T<string>?value ^-> T<bool>)))?interval * (!? T<bool>)?show * (!? T<obj>)?areaStyle) ]
        |+> Instance [
            "interval" =@ (!? (T<float> + (T<float>?index * T<string>?value ^-> T<bool>)))
            "show" =@ (!? T<bool>)
            "areaStyle" =@ (!? T<obj>)
        ]
        |> ignore

    CartesianAxis_DataObject
        |> WithSourceName "CartesianAxis_DataObject"
        |+> Static [ ObjectConstructor ((!? (T<string> + T<float>))?value * (!? TextStyleWithRich)?textStyle) ]
        |+> Instance [
            "value" =@ (!? (T<string> + T<float>))
            "textStyle" =@ (!? TextStyleWithRich)
        ]
        |> ignore

    CartesianAxis_Pointer
        |> WithSourceName "CartesianAxis_Pointer"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetShow" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.show = " + tr "v" + ", $this)")
            "SetType" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.type = " + tr "v" + ", $this)")
            "SetSnap" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.snap = " + tr "v" + ", $this)")
            "SetZ" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.z = " + tr "v" + ", $this)")
            "SetLabel" => CartesianAxis_PointerLabel?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.label = " + tr "v" + ", $this)")
            "SetLineStyle" => LineStyle?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineStyle = " + tr "v" + ", $this)")
            "SetShadowStyle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowStyle = " + tr "v" + ", $this)")
            "SetTriggerTooltip" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.triggerTooltip = " + tr "v" + ", $this)")
            "SetValue" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.value = " + tr "v" + ", $this)")
            "SetStatus" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.status = " + tr "v" + ", $this)")
            "SetHandle" => T<obj>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.handle = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "show" =@ (!? T<bool>)
            "type" =@ (!? (T<string>))
            "snap" =@ (!? T<bool>)
            "z" =@ (!? T<float>)
            "label" =@ (!? CartesianAxis_PointerLabel)
            "lineStyle" =@ (!? LineStyle)
            "shadowStyle" =@ (!? T<obj>)
            "triggerTooltip" =@ (!? T<bool>)
            "value" =@ (!? T<float>)
            "status" =@ (!? T<bool>)
            "handle" =@ (!? T<obj>)
        ]
        |> ignore

    CartesianAxis_PointerLabel
        |> WithSourceName "CartesianAxis_PointerLabel"
        |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]
        |+> Instance [
            "SetShow" => T<bool>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.show = " + tr "v" + ", $this)")
            "SetPrecision" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.precision = " + tr "v" + ", $this)")
            "SetFormatter" => (T<string> + (T<obj>?``params`` ^-> T<string>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.formatter = " + tr "v" + ", $this)")
            "SetMargin" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.margin = " + tr "v" + ", $this)")
            "SetColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.color = " + tr "v" + ", $this)")
            "SetFontStyle" => (T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontStyle = " + tr "v" + ", $this)")
            "SetFontWeight" => (T<string> + T<float>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontWeight = " + tr "v" + ", $this)")
            "SetFontFamily" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontFamily = " + tr "v" + ", $this)")
            "SetFontSize" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.fontSize = " + tr "v" + ", $this)")
            "SetLineHeight" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.lineHeight = " + tr "v" + ", $this)")
            "SetBackgroundColor" => (T<string> + T<obj>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.backgroundColor = " + tr "v" + ", $this)")
            "SetBorderColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderColor = " + tr "v" + ", $this)")
            "SetBorderWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderWidth = " + tr "v" + ", $this)")
            "SetBorderRadius" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.borderRadius = " + tr "v" + ", $this)")
            "SetPadding" => (T<float> + (!| T<float>))?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.padding = " + tr "v" + ", $this)")
            "SetShadowColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowColor = " + tr "v" + ", $this)")
            "SetShadowBlur" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowBlur = " + tr "v" + ", $this)")
            "SetShadowOffsetX" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowOffsetX = " + tr "v" + ", $this)")
            "SetShadowOffsetY" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.shadowOffsetY = " + tr "v" + ", $this)")
            "SetWidth" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.width = " + tr "v" + ", $this)")
            "SetHeight" => (T<float> + T<string>)?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.height = " + tr "v" + ", $this)")
            "SetTextBorderColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textBorderColor = " + tr "v" + ", $this)")
            "SetTextBorderWidth" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textBorderWidth = " + tr "v" + ", $this)")
            "SetTextShadowColor" => T<string>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowColor = " + tr "v" + ", $this)")
            "SetTextShadowBlur" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowBlur = " + tr "v" + ", $this)")
            "SetTextShadowOffsetX" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowOffsetX = " + tr "v" + ", $this)")
            "SetTextShadowOffsetY" => T<float>?v ^-> TSelf
            |> WithInteropInline (fun tr -> "($this.textShadowOffsetY = " + tr "v" + ", $this)")
        ]
        |+> Instance [
            "show" =@ (!? T<bool>)
            "precision" =@ (!? (T<float> + T<string>))
            "formatter" =@ (!? (T<string> + (T<obj>?``params`` ^-> T<string>)))
            "margin" =@ (!? T<float>)
            "color" =@ (!? T<string>)
            "fontStyle" =@ (!? (T<string>))
            "fontWeight" =@ (!? (T<string> + T<float>))
            "fontFamily" =@ (!? T<string>)
            "fontSize" =@ (!? T<float>)
            "lineHeight" =@ (!? T<float>)
            "backgroundColor" =@ (!? (T<string> + T<obj>))
            "borderColor" =@ (!? T<string>)
            "borderWidth" =@ (!? T<float>)
            "borderRadius" =@ (!? T<float>)
            "padding" =@ (!? (T<float> + (!| T<float>)))
            "shadowColor" =@ (!? T<string>)
            "shadowBlur" =@ (!? T<float>)
            "shadowOffsetX" =@ (!? T<float>)
            "shadowOffsetY" =@ (!? T<float>)
            "width" =@ (!? (T<float> + T<string>))
            "height" =@ (!? (T<float> + T<string>))
            "textBorderColor" =@ (!? T<string>)
            "textBorderWidth" =@ (!? T<float>)
            "textShadowColor" =@ (!? T<string>)
            "textShadowBlur" =@ (!? T<float>)
            "textShadowOffsetX" =@ (!? T<float>)
            "textShadowOffsetY" =@ (!? T<float>)
        ]
        |> ignore

    let echarts =
        Class "echarts"
        |+> Static [
            "init" => (T<HTMLElement> + T<HTMLElement>)?dom * (!? (T<obj> + T<string>))?theme * (!? T<obj>)?opts ^-> ECharts
            "connect" => (T<string> + (!| ECharts))?group ^-> T<unit>
            "disConnect" => T<string>?group ^-> T<unit>
            "dispose" => (ECharts + T<HTMLElement> + T<HTMLElement>)?target ^-> T<unit>
            "getInstanceByDom" => (T<HTMLElement> + T<HTMLElement>)?target ^-> ECharts
            "registerMap" => T<string>?mapName * T<obj>?geoJson * (!? T<obj>)?specialAreas ^-> T<unit>
            "registerTheme" => T<string>?themeName * T<obj>?theme ^-> T<unit>
            "getMap" => T<string>?mapName ^-> MapObj
        ]
    let NamespaceEntities: CodeModel.NamespaceEntity list = [
        zrender_LinearGradient
        SeriesBar
        SeriesBar_DataObject
        SeriesBoxplot
        SeriesBoxplot_DataObject
        SeriesCandlestick
        SeriesCandlestick_DataObject
        SeriesCustom
        SeriesCustom_DataObject
        SeriesCustom_RenderItemParams
        SeriesCustom_RenderItemApi
        SeriesCustom_CoordSys
        SeriesCustom_RangeInfo
        SeriesCustom_RenderItemReturnGroup
        SeriesCustom_RenderItemReturnPath
        SeriesCustom_RenderItemReturnImage
        SeriesCustom_RenderItemReturnText
        SeriesCustom_RenderItemReturnRect
        SeriesCustom_RenderItemReturnCircle
        SeriesCustom_RenderItemReturnRing
        SeriesCustom_RenderItemReturnSector
        SeriesCustom_RenderItemReturnArc
        SeriesCustom_RenderItemReturnPolygon
        SeriesCustom_RenderItemReturnPolyline
        SeriesCustom_RenderItemReturnLine
        SeriesCustom_RenderItemReturnBezierCurve
        SeriesEffectScatter
        SeriesEffectScatter_DataObject
        SeriesFunnel
        SeriesFunnel_DataObject
        SeriesGauge
        SeriesGauge_DataObject
        SeriesGraph
        SeriesGraph_CategoryObject
        SeriesGraph_DataObject
        SeriesGraph_LinkObject
        SeriesHeatmap
        SeriesHeatmap_DataObject
        SeriesLine
        SeriesLine_DataObject
        SeriesLines
        SeriesLines_DataObject
        SeriesMap
        SeriesMap_DataObject
        SeriesParallel
        SeriesParallel_DataObject
        SeriesPictorialBar
        SeriesPictorialBar_DataObject
        SeriesPie
        SeriesPie_DataObject
        SeriesRadar
        SeriesRadar_DataObject
        SeriesSankey
        SeriesSankey_DataObject
        SeriesSankey_LinkObject
        SeriesScatter
        SeriesScatter_DataObject
        SeriesSunburst
        SeriesSunburst_DataObject
        SeriesThemeRiver
        SeriesThemeRiver_DataObject
        SeriesTree
        SeriesTree_DataObject
        SeriesTreemap
        SeriesTreemap_DataObject
        AxisPointer
        Calendar
        Calendar_Label
        Calendar_DayLabel
        Calendar_MonthLabel
        Calendar_MonthLabelFormatterParams
        Calendar_YearLabel
        Calendar_YearLabelFormatterParams
        Dataset
        Dataset_DimensionObject
        DataZoom_Inside
        DataZoom_Slider
        Grid
        Legend
        Legend_LegendDataObject
        Legend_PageIcons
        LineStyle
        SingleAxis
        BaseTextStyle
        TextStyle
        RichStyle
        BaseTextStyleWithRich
        TextStyleWithRich
        BaseTooltip
        Tooltip
        Tooltip_Position_Obj
        Tooltip_Format
        Tooltip_AxisPointer
        VisualMap_Continuous
        VisualMap_Piecewise
        VisualMap_RangeObject
        VisualMap_PiecesObject
        XAxis
        YAxis
        MapObj
        Graphic
        ECharts
        EChartsConvertFinder
        ERectangle
        EChartOption
        EChartsResponsiveOption
        EChartsOptionConfig
        EChartsResizeOption
        EChartTitleOption
        EChartsLoadingOption
        Line
        ChartExtremes
        CartesianAxis
        CartesianAxis_Tick
        CartesianAxis_MinorTick
        CartesianAxis_Label
        CartesianAxis_SplitLine
        CartesianAxis_MinorSplitLine
        CartesianAxis_SplitArea
        CartesianAxis_DataObject
        CartesianAxis_Pointer
        CartesianAxis_PointerLabel
        echarts
    ]

    let Assembly =
        Assembly [
            Namespace "WebSharper.ECharts" NamespaceEntities
            Namespace "WebSharper.ECharts.Resources" [
                (Resource "echarts.js" "echarts.min.js").AssemblyWide()
            ]
        ]

[<Sealed>]
type Extension() =
    interface IExtension with
        member ext.Assembly =
            Definition.Assembly

[<assembly: Extension(typeof<Extension>)>]
do ()
